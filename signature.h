#ifndef SIGNATURE_H_
#define SIGNATURE_H_

#include "types.h"

struct SHA1;
struct SHA1_increment
{
	uint32_t a, b, c, d, e;
	SHA1_increment();
	SHA1_increment(uint32_t a, uint32_t b, uint32_t c, uint32_t d, uint32_t e);
	SHA1_increment(SHA1& s);
	void next(uint32_t sum);
};

struct SHA1 {
	uint32_t h0, h1, h2, h3, h4;
	char s[41];

	SHA1() : h0(0x67452301), h1(0xefcdab89), h2(0x98badcfe), h3(0x10325476), h4(0xc3d2e1f0) {}
	SHA1(uint32_t h0, uint32_t h1, uint32_t h2, uint32_t h3, uint32_t h4)
		: h0(h0), h1(h1), h2(h2), h3(h3), h4(h4) {}

	void add(SHA1_increment& incr) {
		h0 += incr.a;
		h1 += incr.b;
		h2 += incr.c;
		h3 += incr.d;
		h4 += incr.e;
	}

	void show() {
		printf("%s", tostring());
	}

	char* tostring() {
		snprintf(s, 41,"%08x%08x%08x%08x%08x", h0, h1, h2, h3, h4);
		return s;
	}
};

SHA1 calculate_sha1(uint8_t* buf, uint32_t len);
uint32_t calculate_crc(uint8_t* buf, uint32_t len);

#endif

