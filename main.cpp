#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "signature.h"
#include "patcher.h"
#include "types.h"

void patching(int rom);
extern game_info_t game_info;

static bool patch_file(const char *dir, int rom_idx)
{
  char *fname;
  const char *file;
  uint32_t crc_correct, size_correct;
  uint32_t crc, size;
  uint32_t crc_out;
  uint8_t *rom;
  SHA1 sha_out;

  file = game_info.data[rom_idx].name;
  crc_correct = game_info.data[rom_idx].crc;
  size_correct = game_info.data[rom_idx].size;

  fname = (char*)malloc(strlen(dir)+1+strlen(file)+5);
  sprintf(fname, "%s/%s", dir, file);
  rom = load_buffer(fname, size);
  if(rom == NULL) {
    perror("Failed to read ROM");
    return false;
  }
  sprintf(fname, "%s/%s.bak", dir, file);
  if(save_buffer(fname, size, rom) == -1) {
    perror("Unable to save backup ROM");
    return false;
  }
  sprintf(fname, "%s/%s", dir, file);
  crc = calculate_crc(rom, size);
  printf("Loaded %s [CRC:%08X|Size:0x%x]\n", file, crc, size);
  if(size != size_correct) {
    printf("ROM size incorrect.\n");
    return false;
  }
  if(crc != crc_correct) {
    printf("CRC incorrect (%08X instead of %08X).\n", crc, crc_correct);
  }
  printf("ROM OK.\n");
  game_patch_set(rom);

  printf("Patching... ");
  fflush(stdout);
  patching(rom_idx);
  printf("done!\n");

  game_patch_set(NULL);

  if(save_buffer(fname, size, rom) == -1) {
    perror("Unable to save ROM");
    return false;
  }
  sha_out = calculate_sha1(rom, size);
  crc_out = calculate_crc(rom, size);
  printf("Signatures for %s:\nCRC(%08x) SHA1(%s) \n", file, crc_out, sha_out.tostring());
  delete game_rom;
  free(fname);
  return true;
}

int main(int argc, char **argv)
{
  if(argc < 2) {
    fprintf(stderr, "Invalid arguments. Usage:\n"
                    "\t%s infolder\n",
                    argv[0]);
    return EXIT_FAILURE;
  }
  const char* romsrc = argv[1];

  for(int i=0; i < game_info.count; i++) {
    patch_file(romsrc, i);
  }

  return EXIT_SUCCESS;
}

