#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <algorithm>
#include <stdarg.h>

#include "signature.h"
#include "types.h"

#if _MSC_VER
#define snprintf _snprintf
#endif

using namespace std;

//CRC shit from W3C
static uint32_t crc_table[256];
static int crc_table_computed = 0;

static void crc_init()
{
	uint32_t n;
	if(crc_table_computed)
		return;
	for(n = 0; n < 256; n++) {
		uint32_t c = n;
		for(int k = 0; k < 8; k++) {
			if(c & 1)
				c = 0xEDB88320 ^ (c >> 1);
			else
				c >>= 1;
		}
		crc_table[n] = c;
	}
	crc_table_computed = 1;
}

uint32_t calculate_crc(uint8_t* rbuf, uint32_t len)
{
	char *buf = (char*)rbuf;
	uint32_t n;
	crc_init();
	uint32_t c = 0xFFFFFFFF;
	for(n = 0; n < len; n++) {
		c = crc_table[(c ^ buf[n]) & 0xFF] ^ (c >> 8);
	}
	return ~c;
}

//SHA-1 shit from RFC3174

//Should define away for big endian system
#if GCC
	#define endian_swap __builtin_bswap32
#elif _MSC_VER
	#define endian_swap _byteswap_ulong
#else
	static uint32_t endian_swap(uint32_t x) {
		return (x << 24) | (x >> 24) | ((x&0xff00)<<8) | ((x&0xff0000)>>8);
	}
#endif

template<uint32_t n>
inline uint32_t rol(uint32_t x)
{
	return (x << n) | (x >> (32 - n));
}

SHA1_increment::SHA1_increment() : a(0), b(0), c(0), d(0), e(0) {}
SHA1_increment::SHA1_increment(uint32_t a, uint32_t b, uint32_t c, uint32_t d, uint32_t e)
	: a(a), b(b), c(c), d(d), e(e) {}
SHA1_increment::SHA1_increment(SHA1& s)
	: a(s.h0), b(s.h1), c(s.h2), d(s.h3), e(s.h4) {}

void SHA1_increment::next(uint32_t sum)
{
	uint32_t temp = rol<5>(a) + e + sum;
	e = d;
	d = c;
	c = rol<30>(b);
	b = a;
	a = temp;
}

typedef uint32_t (*F_func_t)(SHA1_increment& s);
static uint32_t func0 (SHA1_increment& s) { return (s.b & s.c) | ((~s.b) & s.d); }
static uint32_t func1 (SHA1_increment& s) { return s.b ^ s.c ^ s.d; }
static uint32_t func2 (SHA1_increment& s) { return (s.b & s.c) | (s.b & s.d) | (s.c & s.d); }
static F_func_t ftab[] = {func0, func1, func2, func1};
static uint32_t ktab[] = {0x5a827999, 0x6ed9eba1, 0x8f1bbcdc, 0xca62c1d6};

SHA1 calculate_sha1(uint8_t* rbuf, uint32_t len)
{
	char *buf = (char*)rbuf;
	if(!len)
		return SHA1();

	//2 512-bit buffers;
	uint32_t extrachunk[2][16];
	memset(extrachunk[0], 0, 64);
	memset(extrachunk[1], 0, 64);

	int num_extrachunk;
	int main_chunk = len >> 6;
	int extra_len_bits = (len << 3) & 0x1ff;
	if(extra_len_bits > 447)
		num_extrachunk = 2;
	else
		num_extrachunk = 1;

	int remainder = len - (main_chunk << 6);
	if(remainder)
		memcpy(extrachunk[0], buf + (main_chunk << 6), remainder);

	//access byte-wise
	((uint8_t*)(extrachunk[0]))[remainder] = 0x80;
	//little-endian to big-endian (for x86)
	extrachunk[num_extrachunk - 1][15] |= endian_swap(len << 3);
	extrachunk[num_extrachunk - 1][14] |= endian_swap(len >> 29);

	SHA1 s; 
	for(int cur_chunk = 0;;cur_chunk++) {
		const uint32_t* ptr = NULL;

		if(cur_chunk < main_chunk)
			ptr = (const uint32_t*)(buf + (cur_chunk << 6));
		else if((cur_chunk - main_chunk) < num_extrachunk)
			ptr = extrachunk[cur_chunk - main_chunk];
		else
			break;

		uint32_t w[80];
		//Copy top chunk
		memcpy(w, ptr, 64);
		for(int t = 0; t < 16; t++)
			w[t] = endian_swap(w[t]);

		//Extend remaining chunk
		for(int t = 16; t < 80; t++)
			w[t] = rol<1>(w[t-3] ^ w[t-8] ^ w[t-14] ^ w[t-16]);

		SHA1_increment incr(s);
		for(int t = 0; t < 80; t++) {
			int round = t/20;
			uint32_t f = ftab[round](incr);
			incr.next(w[t] + ktab[round] + f);
		}
		s.add(incr);
	}

	return s;
}


