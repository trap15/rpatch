#ifndef TYPES_H_
#define TYPES_H_

#if _MSC_VER
typedef unsigned char	uint8_t;
typedef unsigned short	uint16_t;
typedef unsigned int	uint32_t;
typedef signed char	int8_t;
typedef signed short	int16_t;
typedef signed int	int32_t;
#else
#include <stdint.h>
#endif

#endif
