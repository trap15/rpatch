#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <algorithm>
#include <stdarg.h>

#include "signature.h"
#include "patcher.h"
#include "types.h"

#if _MSC_VER
#define snprintf _snprintf
#endif

using namespace std;

game_info_t game_info = {
  1,
  {
    { "ketsui_v100.u38",
      0x200000, /* ROM size */
      0xDFE62F3B, /* ROM CRC */
    },
  },
};

void patch_v100()
{
  uint32_t patch0_addr = 0x00000100;
  uint8_t patch0_data[] = {
    0x91, 0x29, 0xE9, 
  };
  APPLY_PATCH(0);

  uint32_t patch1_addr = 0x00000105;
  uint8_t patch1_data[] = {
    0xA7, 
  };
  APPLY_PATCH(1);

  uint32_t patch2_addr = 0x00000107;
  uint8_t patch2_data[] = {
    0xF6, 0x74, 0xF8, 
  };
  APPLY_PATCH(2);

  uint32_t patch3_addr = 0x0000010C;
  uint8_t patch3_data[] = {
    0x4B, 
  };
  APPLY_PATCH(3);

  uint32_t patch4_addr = 0x0000010E;
  uint8_t patch4_data[] = {
    0x88, 0x0B, 0xA1, 0x85, 0x94, 0x76, 0x91, 0x99, 0x63, 
  };
  APPLY_PATCH(4);

  uint32_t patch5_addr = 0x00000119;
  uint8_t patch5_data[] = {
    0xD3, 
  };
  APPLY_PATCH(5);

  uint32_t patch6_addr = 0x00000140;
  uint8_t patch6_data[] = {
    0x2F, 0xED, 0xD0, 0x74, 0x0E, 0x88, 0x2C, 0x9F, 0x5C, 
  };
  APPLY_PATCH(6);

  uint32_t patch7_addr = 0x0000014A;
  uint8_t patch7_data[] = {
    0x08, 0xD1, 0x4B, 0x8A, 0xF0, 0x13, 0x0E, 0x2E, 0x2C, 0xC4, 0x76, 
  };
  APPLY_PATCH(7);

  uint32_t patch8_addr = 0x00000156;
  uint8_t patch8_data[] = {
    0x2F, 0x80, 0xF0, 0x40, 0x2E, 0xA8, 0x0C, 0xEA, 0x54, 
  };
  APPLY_PATCH(8);

  uint32_t patch9_addr = 0x00000160;
  uint8_t patch9_data[] = {
    0x6E, 0x26, 0xD0, 0x2E, 0x0E, 0x0D, 0x2C, 0xD7, 0x68, 
  };
  APPLY_PATCH(9);

  uint32_t patch10_addr = 0x0000016A;
  uint8_t patch10_data[] = {
    0xB4, 0x68, 
  };
  APPLY_PATCH(10);

  uint32_t patch11_addr = 0x0000016E;
  uint8_t patch11_data[] = {
    0xC9, 
  };
  APPLY_PATCH(11);

  uint32_t patch12_addr = 0x00000170;
  uint8_t patch12_data[] = {
    0x2E, 0x00, 0x94, 0x1A, 
  };
  APPLY_PATCH(12);

  uint32_t patch13_addr = 0x00000176;
  uint8_t patch13_data[] = {
    0xE9, 
  };
  APPLY_PATCH(13);

  uint32_t patch14_addr = 0x00000178;
  uint8_t patch14_data[] = {
    0x06, 0xC9, 0x71, 0xCF, 0xC8, 
  };
  APPLY_PATCH(14);

  uint32_t patch15_addr = 0x0000017E;
  uint8_t patch15_data[] = {
    0x4C, 0x93, 
  };
  APPLY_PATCH(15);

  uint32_t patch16_addr = 0x00000181;
  uint8_t patch16_data[] = {
    0x4A, 0x6A, 
  };
  APPLY_PATCH(16);

  uint32_t patch17_addr = 0x00000184;
  uint8_t patch17_data[] = {
    0x44, 0xC5, 0x59, 0xCF, 0xC9, 
  };
  APPLY_PATCH(17);

  uint32_t patch18_addr = 0x0000018A;
  uint8_t patch18_data[] = {
    0x1A, 0x37, 0x00, 0x66, 0xEF, 
  };
  APPLY_PATCH(18);

  uint32_t patch19_addr = 0x00000190;
  uint8_t patch19_data[] = {
    0x7E, 0xDF, 0x94, 0xDC, 0x61, 
  };
  APPLY_PATCH(19);

  uint32_t patch20_addr = 0x00000196;
  uint8_t patch20_data[] = {
    0xE1, 
  };
  APPLY_PATCH(20);

  uint32_t patch21_addr = 0x00000198;
  uint8_t patch21_data[] = {
    0x0E, 0xBA, 0x08, 0xAA, 0x9F, 
  };
  APPLY_PATCH(21);

  uint32_t patch22_addr = 0x0000019E;
  uint8_t patch22_data[] = {
    0x48, 0x21, 0x90, 0x4D, 0x6D, 
  };
  APPLY_PATCH(22);

  uint32_t patch23_addr = 0x000001A4;
  uint8_t patch23_data[] = {
    0xE5, 
  };
  APPLY_PATCH(23);

  uint32_t patch24_addr = 0x000001A6;
  uint8_t patch24_data[] = {
    0x2A, 0x44, 0x39, 0x59, 0x75, 0xAE, 0xC5, 
  };
  APPLY_PATCH(24);

  uint32_t patch25_addr = 0x000001AE;
  uint8_t patch25_data[] = {
    0x16, 0x40, 0x2C, 0xDF, 0xC3, 
  };
  APPLY_PATCH(25);

  uint32_t patch26_addr = 0x000001B4;
  uint8_t patch26_data[] = {
    0x74, 0x89, 0x98, 0x6B, 0x4D, 
  };
  APPLY_PATCH(26);

  uint32_t patch27_addr = 0x000001BA;
  uint8_t patch27_data[] = {
    0xCD, 
  };
  APPLY_PATCH(27);

  uint32_t patch28_addr = 0x000001BC;
  uint8_t patch28_data[] = {
    0x0A, 0xD3, 0xB8, 0x0D, 0x69, 
  };
  APPLY_PATCH(28);

  uint32_t patch29_addr = 0x000001C2;
  uint8_t patch29_data[] = {
    0xE9, 
  };
  APPLY_PATCH(29);

  uint32_t patch30_addr = 0x000001C4;
  uint8_t patch30_data[] = {
    0x26, 0xFB, 0x15, 0xFE, 0x71, 0xA2, 0xC8, 
  };
  APPLY_PATCH(30);

  uint32_t patch31_addr = 0x000001CC;
  uint8_t patch31_data[] = {
    0x36, 0xCA, 0x79, 0x88, 0xE9, 
  };
  APPLY_PATCH(31);

  uint32_t patch32_addr = 0x000001D2;
  uint8_t patch32_data[] = {
    0x60, 0x8D, 0x15, 0x13, 
  };
  APPLY_PATCH(32);

  uint32_t patch33_addr = 0x000001E6;
  uint8_t patch33_data[] = {
    0x76, 0xDF, 0x45, 
  };
  APPLY_PATCH(33);

  uint32_t patch34_addr = 0x000001EA;
  uint8_t patch34_data[] = {
    0x52, 0x4D, 0x2E, 0x7A, 0xD0, 
  };
  APPLY_PATCH(34);

  uint32_t patch35_addr = 0x000001F0;
  uint8_t patch35_data[] = {
    0x4E, 
  };
  APPLY_PATCH(35);

  uint32_t patch36_addr = 0x000001F2;
  uint8_t patch36_data[] = {
    0x7A, 0x80, 0x0E, 0x00, 0x44, 
  };
  APPLY_PATCH(36);

  uint32_t patch37_addr = 0x000001F8;
  uint8_t patch37_data[] = {
    0x6E, 
  };
  APPLY_PATCH(37);

  uint32_t patch38_addr = 0x000001FA;
  uint8_t patch38_data[] = {
    0x4E, 0xAE, 0x00, 0x4D, 
  };
  APPLY_PATCH(38);

  uint32_t patch39_addr = 0x000001FF;
  uint8_t patch39_data[] = {
    0x7A, 0x6D, 0x29, 0x29, 0x41, 
  };
  APPLY_PATCH(39);

  uint32_t patch40_addr = 0x00000205;
  uint8_t patch40_data[] = {
    0x57, 0x29, 0x78, 0x10, 0xE0, 0xC9, 0xDC, 0x3C, 0x7A, 0x70, 0x60, 0xE8, 
  };
  APPLY_PATCH(40);

  uint32_t patch41_addr = 0x00000212;
  uint8_t patch41_data[] = {
    0x3B, 0x15, 0x28, 0x3C, 0xC6, 
  };
  APPLY_PATCH(41);

  uint32_t patch42_addr = 0x00000218;
  uint8_t patch42_data[] = {
    0x43, 0x55, 
  };
  APPLY_PATCH(42);

  uint32_t patch43_addr = 0x0000021B;
  uint8_t patch43_data[] = {
    0x42, 
  };
  APPLY_PATCH(43);

  uint32_t patch44_addr = 0x0000021D;
  uint8_t patch44_data[] = {
    0x45, 0xB0, 0x7B, 0x62, 
  };
  APPLY_PATCH(44);

  uint32_t patch45_addr = 0x00000222;
  uint8_t patch45_data[] = {
    0x97, 0x11, 0x2C, 0x2B, 0xB2, 
  };
  APPLY_PATCH(45);

  uint32_t patch46_addr = 0x00000228;
  uint8_t patch46_data[] = {
    0x6D, 0x9B, 
  };
  APPLY_PATCH(46);

  uint32_t patch47_addr = 0x0000022B;
  uint8_t patch47_data[] = {
    0xD9, 0x71, 0xBA, 
  };
  APPLY_PATCH(47);

  uint32_t patch48_addr = 0x00000231;
  uint8_t patch48_data[] = {
    0x0D, 
  };
  APPLY_PATCH(48);

  uint32_t patch49_addr = 0x00000233;
  uint8_t patch49_data[] = {
    0x49, 0xD4, 0x5C, 0x62, 
  };
  APPLY_PATCH(49);

  uint32_t patch50_addr = 0x00000238;
  uint8_t patch50_data[] = {
    0xB7, 0xBA, 0x52, 0x10, 0x71, 0x72, 
  };
  APPLY_PATCH(50);

  uint32_t patch51_addr = 0x00000241;
  uint8_t patch51_data[] = {
    0x0F, 
  };
  APPLY_PATCH(51);

  uint32_t patch52_addr = 0x00000243;
  uint8_t patch52_data[] = {
    0xF6, 0x90, 0xBE, 0x66, 
  };
  APPLY_PATCH(52);

  uint32_t patch53_addr = 0x00000248;
  uint8_t patch53_data[] = {
    0xB3, 0x10, 0x08, 0x48, 0xB5, 
  };
  APPLY_PATCH(53);

  uint32_t patch54_addr = 0x0000024E;
  uint8_t patch54_data[] = {
    0x7B, 0x97, 
  };
  APPLY_PATCH(54);

  uint32_t patch55_addr = 0x00000251;
  uint8_t patch55_data[] = {
    0x22, 0x55, 0xFD, 
  };
  APPLY_PATCH(55);

  uint32_t patch56_addr = 0x00000255;
  uint8_t patch56_data[] = {
    0xF9, 
  };
  APPLY_PATCH(56);

  uint32_t patch57_addr = 0x00000259;
  uint8_t patch57_data[] = {
    0x24, 0xF0, 0x1D, 0x46, 
  };
  APPLY_PATCH(57);

  uint32_t patch58_addr = 0x0000025E;
  uint8_t patch58_data[] = {
    0xB3, 0x81, 0x76, 0x32, 
  };
  APPLY_PATCH(58);

  uint32_t patch59_addr = 0x00000263;
  uint8_t patch59_data[] = {
    0x80, 0x55, 0x27, 
  };
  APPLY_PATCH(59);

  uint32_t patch60_addr = 0x00000267;
  uint8_t patch60_data[] = {
    0x85, 
  };
  APPLY_PATCH(60);

  uint32_t patch61_addr = 0x00000269;
  uint8_t patch61_data[] = {
    0x67, 
  };
  APPLY_PATCH(61);

  uint32_t patch62_addr = 0x0000026B;
  uint8_t patch62_data[] = {
    0xA4, 0xF0, 0x61, 0x46, 
  };
  APPLY_PATCH(62);

  uint32_t patch63_addr = 0x00000270;
  uint8_t patch63_data[] = {
    0x93, 0xA9, 0x76, 0x7D, 0x55, 0x35, 
  };
  APPLY_PATCH(63);

  uint32_t patch64_addr = 0x00000277;
  uint8_t patch64_data[] = {
    0xBB, 
  };
  APPLY_PATCH(64);

  uint32_t patch65_addr = 0x00000279;
  uint8_t patch65_data[] = {
    0xC4, 
  };
  APPLY_PATCH(65);

  uint32_t patch66_addr = 0x0000027B;
  uint8_t patch66_data[] = {
    0x77, 0xB0, 0x16, 0x46, 
  };
  APPLY_PATCH(66);

  uint32_t patch67_addr = 0x00000280;
  uint8_t patch67_data[] = {
    0x93, 0x39, 
  };
  APPLY_PATCH(67);

  uint32_t patch68_addr = 0x00000283;
  uint8_t patch68_data[] = {
    0xC1, 0x5D, 0xF3, 
  };
  APPLY_PATCH(68);

  uint32_t patch69_addr = 0x00000287;
  uint8_t patch69_data[] = {
    0xCF, 
  };
  APPLY_PATCH(69);

  uint32_t patch70_addr = 0x00000289;
  uint8_t patch70_data[] = {
    0x01, 
  };
  APPLY_PATCH(70);

  uint32_t patch71_addr = 0x0000028B;
  uint8_t patch71_data[] = {
    0xFC, 0xF8, 0xDE, 0x4E, 
  };
  APPLY_PATCH(71);

  uint32_t patch72_addr = 0x00000290;
  uint8_t patch72_data[] = {
    0x93, 0x04, 0x76, 0x56, 
  };
  APPLY_PATCH(72);

  uint32_t patch73_addr = 0x00000295;
  uint8_t patch73_data[] = {
    0xD4, 0x5D, 0xBD, 
  };
  APPLY_PATCH(73);

  uint32_t patch74_addr = 0x00000299;
  uint8_t patch74_data[] = {
    0x74, 
  };
  APPLY_PATCH(74);

  uint32_t patch75_addr = 0x0000029B;
  uint8_t patch75_data[] = {
    0x30, 
  };
  APPLY_PATCH(75);

  uint32_t patch76_addr = 0x0000029D;
  uint8_t patch76_data[] = {
    0x34, 0xF8, 0x79, 0x62, 
  };
  APPLY_PATCH(76);

  uint32_t patch77_addr = 0x000002A2;
  uint8_t patch77_data[] = {
    0x97, 0xC7, 0x7A, 0x20, 
  };
  APPLY_PATCH(77);

  uint32_t patch78_addr = 0x000002A7;
  uint8_t patch78_data[] = {
    0x1D, 0x71, 0xF7, 
  };
  APPLY_PATCH(78);

  uint32_t patch79_addr = 0x000002AB;
  uint8_t patch79_data[] = {
    0x63, 
  };
  APPLY_PATCH(79);

  uint32_t patch80_addr = 0x000002AD;
  uint8_t patch80_data[] = {
    0x36, 
  };
  APPLY_PATCH(80);

  uint32_t patch81_addr = 0x000002AF;
  uint8_t patch81_data[] = {
    0xA6, 0xD4, 0x7C, 0x62, 
  };
  APPLY_PATCH(81);

  uint32_t patch82_addr = 0x000002B4;
  uint8_t patch82_data[] = {
    0x9F, 0x37, 0x7A, 0x56, 0x71, 0xF1, 
  };
  APPLY_PATCH(82);

  uint32_t patch83_addr = 0x000002BB;
  uint8_t patch83_data[] = {
    0x79, 
  };
  APPLY_PATCH(83);

  uint32_t patch84_addr = 0x000002BD;
  uint8_t patch84_data[] = {
    0xDA, 
  };
  APPLY_PATCH(84);

  uint32_t patch85_addr = 0x000002BF;
  uint8_t patch85_data[] = {
    0x8D, 0x90, 0x98, 0x66, 
  };
  APPLY_PATCH(85);

  uint32_t patch86_addr = 0x000002C4;
  uint8_t patch86_data[] = {
    0x9B, 0x23, 0x18, 0x42, 0x4B, 
  };
  APPLY_PATCH(86);

  uint32_t patch87_addr = 0x000002CA;
  uint8_t patch87_data[] = {
    0xC8, 
  };
  APPLY_PATCH(87);

  uint32_t patch88_addr = 0x000002CC;
  uint8_t patch88_data[] = {
    0x49, 0xC0, 0x4B, 0x3B, 0x95, 0xA5, 
  };
  APPLY_PATCH(88);

  uint32_t patch89_addr = 0x000002D4;
  uint8_t patch89_data[] = {
    0xE0, 
  };
  APPLY_PATCH(89);

  uint32_t patch90_addr = 0x000002D6;
  uint8_t patch90_data[] = {
    0x69, 0xF0, 0x4F, 0xF2, 0x30, 0x62, 0xC0, 
  };
  APPLY_PATCH(90);

  uint32_t patch91_addr = 0x000002DE;
  uint8_t patch91_data[] = {
    0x49, 0x32, 0x50, 0x72, 0xE8, 
  };
  APPLY_PATCH(91);

  uint32_t patch92_addr = 0x000002E4;
  uint8_t patch92_data[] = {
    0x4F, 0x4A, 0x14, 0x49, 0xF0, 0x01, 
  };
  APPLY_PATCH(92);

  uint32_t patch93_addr = 0x000002EC;
  uint8_t patch93_data[] = {
    0x89, 0x86, 0x38, 0xD8, 0xE8, 
  };
  APPLY_PATCH(93);

  uint32_t patch94_addr = 0x000002F2;
  uint8_t patch94_data[] = {
    0x63, 0x72, 0x20, 0x73, 0x31, 
  };
  APPLY_PATCH(94);

  uint32_t patch95_addr = 0x000002F8;
  uint8_t patch95_data[] = {
    0x08, 0xA8, 0x19, 
  };
  APPLY_PATCH(95);

  uint32_t patch96_addr = 0x000002FC;
  uint8_t patch96_data[] = {
    0x51, 0xF7, 0x02, 0x94, 0x16, 0x6E, 0x6C, 0xB0, 0x2B, 0x13, 0x64, 0xE6, 0x74, 0xEC, 0xC8, 0x94, 
    0x4E, 0x85, 0x0B, 0x61, 0x6A, 0xB3, 0x2B, 0xDA, 0xAB, 0xE4, 0xE9, 
  };
  APPLY_PATCH(96);

  uint32_t patch97_addr = 0x00000318;
  uint8_t patch97_data[] = {
    0x40, 0xDD, 0xB1, 0x42, 0x4E, 
  };
  APPLY_PATCH(97);

  uint32_t patch98_addr = 0x0000031E;
  uint8_t patch98_data[] = {
    0x68, 0x2F, 0x55, 0x3A, 0xED, 
  };
  APPLY_PATCH(98);

  uint32_t patch99_addr = 0x00000324;
  uint8_t patch99_data[] = {
    0x3E, 0x88, 0x2F, 0x3D, 0x93, 
  };
  APPLY_PATCH(99);

  uint32_t patch100_addr = 0x0000032A;
  uint8_t patch100_data[] = {
    0x44, 0x8C, 0xB5, 0xB7, 0x4A, 
  };
  APPLY_PATCH(100);

  uint32_t patch101_addr = 0x00000330;
  uint8_t patch101_data[] = {
    0x8C, 0x7F, 0x60, 0xF5, 0x2F, 0x7B, 0xC3, 
  };
  APPLY_PATCH(101);

  uint32_t patch102_addr = 0x00000338;
  uint8_t patch102_data[] = {
    0x4A, 0xAA, 0xB5, 0x3D, 0x4A, 
  };
  APPLY_PATCH(102);

  uint32_t patch103_addr = 0x0000033E;
  uint8_t patch103_data[] = {
    0x2C, 0xC5, 0xA0, 0x82, 0x66, 0x5D, 0xA0, 0xA8, 0xE9, 
  };
  APPLY_PATCH(103);

  uint32_t patch104_addr = 0x00000348;
  uint8_t patch104_data[] = {
    0x42, 0xCB, 0x4B, 0x37, 0xF1, 0xF8, 0x42, 
  };
  APPLY_PATCH(104);

  uint32_t patch105_addr = 0x00000350;
  uint8_t patch105_data[] = {
    0x14, 0xE2, 0x91, 0xBB, 0x63, 
  };
  APPLY_PATCH(105);

  uint32_t patch106_addr = 0x00000356;
  uint8_t patch106_data[] = {
    0x6A, 0xF3, 0xB4, 0x3D, 
  };
  APPLY_PATCH(106);

  uint32_t patch107_addr = 0x0000035C;
  uint8_t patch107_data[] = {
    0xC9, 
  };
  APPLY_PATCH(107);

  uint32_t patch108_addr = 0x0000035E;
  uint8_t patch108_data[] = {
    0x40, 0x41, 0x91, 0x56, 0x63, 
  };
  APPLY_PATCH(108);

  uint32_t patch109_addr = 0x00000364;
  uint8_t patch109_data[] = {
    0xBE, 0xEF, 0x6E, 0xDE, 0x0C, 0x54, 0xB7, 
  };
  APPLY_PATCH(109);

  uint32_t patch110_addr = 0x0000036C;
  uint8_t patch110_data[] = {
    0x4C, 0x3D, 0x74, 0xB3, 0x97, 
  };
  APPLY_PATCH(110);

  uint32_t patch111_addr = 0x00000372;
  uint8_t patch111_data[] = {
    0x1D, 0x67, 
  };
  APPLY_PATCH(111);

  uint32_t patch112_addr = 0x00000380;
  uint8_t patch112_data[] = {
    0x14, 0x5B, 0x98, 
  };
  APPLY_PATCH(112);

  uint32_t patch113_addr = 0x00000384;
  uint8_t patch113_data[] = {
    0x73, 
  };
  APPLY_PATCH(113);

  uint32_t patch114_addr = 0x00000386;
  uint8_t patch114_data[] = {
    0x1C, 0xE2, 0x58, 
  };
  APPLY_PATCH(114);

  uint32_t patch115_addr = 0x0000038A;
  uint8_t patch115_data[] = {
    0x1B, 
  };
  APPLY_PATCH(115);

  uint32_t patch116_addr = 0x0000038C;
  uint8_t patch116_data[] = {
    0x3C, 0x77, 0xA0, 
  };
  APPLY_PATCH(116);

  uint32_t patch117_addr = 0x00000390;
  uint8_t patch117_data[] = {
    0xFB, 
  };
  APPLY_PATCH(117);

  uint32_t patch118_addr = 0x00000392;
  uint8_t patch118_data[] = {
    0x14, 0xF2, 0x40, 
  };
  APPLY_PATCH(118);

  uint32_t patch119_addr = 0x00000396;
  uint8_t patch119_data[] = {
    0xB3, 
  };
  APPLY_PATCH(119);

  uint32_t patch120_addr = 0x00000398;
  uint8_t patch120_data[] = {
    0x34, 0x83, 0x28, 
  };
  APPLY_PATCH(120);

  uint32_t patch121_addr = 0x0000039C;
  uint8_t patch121_data[] = {
    0x52, 
  };
  APPLY_PATCH(121);

  uint32_t patch122_addr = 0x0000039E;
  uint8_t patch122_data[] = {
    0x3C, 0x59, 0x0C, 
  };
  APPLY_PATCH(122);

  uint32_t patch123_addr = 0x000003A2;
  uint8_t patch123_data[] = {
    0x3E, 
  };
  APPLY_PATCH(123);

  uint32_t patch124_addr = 0x000003A4;
  uint8_t patch124_data[] = {
    0x18, 0xF0, 0xE4, 
  };
  APPLY_PATCH(124);

  uint32_t patch125_addr = 0x000003A8;
  uint8_t patch125_data[] = {
    0xDE, 
  };
  APPLY_PATCH(125);

  uint32_t patch126_addr = 0x000003AA;
  uint8_t patch126_data[] = {
    0x30, 0x83, 0xC4, 
  };
  APPLY_PATCH(126);

  uint32_t patch127_addr = 0x000003AE;
  uint8_t patch127_data[] = {
    0x96, 
  };
  APPLY_PATCH(127);

  uint32_t patch128_addr = 0x000003B0;
  uint8_t patch128_data[] = {
    0x10, 0xCE, 
  };
  APPLY_PATCH(128);

  uint32_t patch129_addr = 0x000003B4;
  uint8_t patch129_data[] = {
    0x59, 
  };
  APPLY_PATCH(129);

  uint32_t patch130_addr = 0x000003B6;
  uint8_t patch130_data[] = {
    0x18, 0x45, 
  };
  APPLY_PATCH(130);

  uint32_t patch131_addr = 0x000003BA;
  uint8_t patch131_data[] = {
    0x31, 
  };
  APPLY_PATCH(131);

  uint32_t patch132_addr = 0x000003BC;
  uint8_t patch132_data[] = {
    0x38, 0xEA, 
  };
  APPLY_PATCH(132);

  uint32_t patch133_addr = 0x000003C0;
  uint8_t patch133_data[] = {
    0xD5, 
  };
  APPLY_PATCH(133);

  uint32_t patch134_addr = 0x000003C2;
  uint8_t patch134_data[] = {
    0x14, 0xD2, 
  };
  APPLY_PATCH(134);

  uint32_t patch135_addr = 0x000003C6;
  uint8_t patch135_data[] = {
    0x9D, 
  };
  APPLY_PATCH(135);

  uint32_t patch136_addr = 0x000003C8;
  uint8_t patch136_data[] = {
    0x3D, 0xDE, 0x34, 0x71, 0xE0, 
  };
  APPLY_PATCH(136);

  uint32_t patch137_addr = 0x000003CE;
  uint8_t patch137_data[] = {
    0x53, 
  };
  APPLY_PATCH(137);

  uint32_t patch138_addr = 0x000003D0;
  uint8_t patch138_data[] = {
    0x14, 0x8E, 0x08, 
  };
  APPLY_PATCH(138);

  uint32_t patch139_addr = 0x000003D4;
  uint8_t patch139_data[] = {
    0x33, 
  };
  APPLY_PATCH(139);

  uint32_t patch140_addr = 0x000003D6;
  uint8_t patch140_data[] = {
    0x1C, 0x1F, 0x88, 
  };
  APPLY_PATCH(140);

  uint32_t patch141_addr = 0x000003DA;
  uint8_t patch141_data[] = {
    0xDB, 
  };
  APPLY_PATCH(141);

  uint32_t patch142_addr = 0x000003DC;
  uint8_t patch142_data[] = {
    0x3C, 0x61, 0x00, 
  };
  APPLY_PATCH(142);

  uint32_t patch143_addr = 0x000003E0;
  uint8_t patch143_data[] = {
    0xBB, 
  };
  APPLY_PATCH(143);

  uint32_t patch144_addr = 0x000003E2;
  uint8_t patch144_data[] = {
    0x14, 0x47, 0x00, 
  };
  APPLY_PATCH(144);

  uint32_t patch145_addr = 0x000003E6;
  uint8_t patch145_data[] = {
    0x72, 
  };
  APPLY_PATCH(145);

  uint32_t patch146_addr = 0x000003E8;
  uint8_t patch146_data[] = {
    0x34, 0x6E, 0x28, 
  };
  APPLY_PATCH(146);

  uint32_t patch147_addr = 0x000003EC;
  uint8_t patch147_data[] = {
    0x12, 
  };
  APPLY_PATCH(147);

  uint32_t patch148_addr = 0x000003EE;
  uint8_t patch148_data[] = {
    0x3C, 0xF4, 0x18, 
  };
  APPLY_PATCH(148);

  uint32_t patch149_addr = 0x000003F2;
  uint8_t patch149_data[] = {
    0xFA, 
  };
  APPLY_PATCH(149);

  uint32_t patch150_addr = 0x000003F4;
  uint8_t patch150_data[] = {
    0x1C, 0x11, 0x10, 
  };
  APPLY_PATCH(150);

  uint32_t patch151_addr = 0x000003F8;
  uint8_t patch151_data[] = {
    0x9A, 
  };
  APPLY_PATCH(151);

  uint32_t patch152_addr = 0x000003FA;
  uint8_t patch152_data[] = {
    0x34, 0xD6, 0xC8, 
  };
  APPLY_PATCH(152);

  uint32_t patch153_addr = 0x000003FE;
  uint8_t patch153_data[] = {
    0x7D, 
  };
  APPLY_PATCH(153);

  uint32_t patch154_addr = 0x00000400;
  uint8_t patch154_data[] = {
    0x15, 0x54, 0x11, 
  };
  APPLY_PATCH(154);

  uint32_t patch155_addr = 0x00000404;
  uint8_t patch155_data[] = {
    0x14, 
  };
  APPLY_PATCH(155);

  uint32_t patch156_addr = 0x00000406;
  uint8_t patch156_data[] = {
    0x15, 0x2D, 0xD9, 
  };
  APPLY_PATCH(156);

  uint32_t patch157_addr = 0x0000040A;
  uint8_t patch157_data[] = {
    0xF4, 
  };
  APPLY_PATCH(157);

  uint32_t patch158_addr = 0x0000040C;
  uint8_t patch158_data[] = {
    0x35, 0x29, 0x39, 
  };
  APPLY_PATCH(158);

  uint32_t patch159_addr = 0x00000410;
  uint8_t patch159_data[] = {
    0x94, 
  };
  APPLY_PATCH(159);

  uint32_t patch160_addr = 0x00000412;
  uint8_t patch160_data[] = {
    0x1C, 0x7F, 0x15, 0x2D, 0x89, 
  };
  APPLY_PATCH(160);

  uint32_t patch161_addr = 0x00000418;
  uint8_t patch161_data[] = {
    0x5A, 
  };
  APPLY_PATCH(161);

  uint32_t patch162_addr = 0x0000041A;
  uint8_t patch162_data[] = {
    0x35, 0x2D, 0x69, 
  };
  APPLY_PATCH(162);

  uint32_t patch163_addr = 0x0000041E;
  uint8_t patch163_data[] = {
    0x1A, 
  };
  APPLY_PATCH(163);

  uint32_t patch164_addr = 0x00000420;
  uint8_t patch164_data[] = {
    0x11, 0x61, 0xAD, 
  };
  APPLY_PATCH(164);

  uint32_t patch165_addr = 0x00000424;
  uint8_t patch165_data[] = {
    0xFE, 
  };
  APPLY_PATCH(165);

  uint32_t patch166_addr = 0x00000426;
  uint8_t patch166_data[] = {
    0x11, 0xB8, 0x0D, 
  };
  APPLY_PATCH(166);

  uint32_t patch167_addr = 0x0000042A;
  uint8_t patch167_data[] = {
    0x9E, 
  };
  APPLY_PATCH(167);

  uint32_t patch168_addr = 0x0000042C;
  uint8_t patch168_data[] = {
    0x31, 0x85, 0x2D, 
  };
  APPLY_PATCH(168);

  uint32_t patch169_addr = 0x00000430;
  uint8_t patch169_data[] = {
    0x7F, 
  };
  APPLY_PATCH(169);

  uint32_t patch170_addr = 0x00000432;
  uint8_t patch170_data[] = {
    0x11, 0x20, 0x0D, 
  };
  APPLY_PATCH(170);

  uint32_t patch171_addr = 0x00000436;
  uint8_t patch171_data[] = {
    0x3F, 
  };
  APPLY_PATCH(171);

  uint32_t patch172_addr = 0x00000438;
  uint8_t patch172_data[] = {
    0x31, 0xFF, 0x3D, 
  };
  APPLY_PATCH(172);

  uint32_t patch173_addr = 0x0000043C;
  uint8_t patch173_data[] = {
    0xDF, 
  };
  APPLY_PATCH(173);

  uint32_t patch174_addr = 0x0000043E;
  uint8_t patch174_data[] = {
    0x31, 0xD2, 0x19, 
  };
  APPLY_PATCH(174);

  uint32_t patch175_addr = 0x00000442;
  uint8_t patch175_data[] = {
    0xBB, 
  };
  APPLY_PATCH(175);

  uint32_t patch176_addr = 0x00000444;
  uint8_t patch176_data[] = {
    0x15, 0xED, 0x91, 
  };
  APPLY_PATCH(176);

  uint32_t patch177_addr = 0x00000448;
  uint8_t patch177_data[] = {
    0x74, 
  };
  APPLY_PATCH(177);

  uint32_t patch178_addr = 0x0000044A;
  uint8_t patch178_data[] = {
    0x35, 0x59, 0x41, 
  };
  APPLY_PATCH(178);

  uint32_t patch179_addr = 0x0000044E;
  uint8_t patch179_data[] = {
    0x34, 
  };
  APPLY_PATCH(179);

  uint32_t patch180_addr = 0x00000450;
  uint8_t patch180_data[] = {
    0x15, 0x10, 0x99, 
  };
  APPLY_PATCH(180);

  uint32_t patch181_addr = 0x00000454;
  uint8_t patch181_data[] = {
    0xD4, 
  };
  APPLY_PATCH(181);

  uint32_t patch182_addr = 0x00000456;
  uint8_t patch182_data[] = {
    0x15, 0x40, 0x59, 
  };
  APPLY_PATCH(182);

  uint32_t patch183_addr = 0x0000045A;
  uint8_t patch183_data[] = {
    0xB4, 
  };
  APPLY_PATCH(183);

  uint32_t patch184_addr = 0x0000045C;
  uint8_t patch184_data[] = {
    0x3C, 0xDF, 0x35, 0xC4, 0xA9, 
  };
  APPLY_PATCH(184);

  uint32_t patch185_addr = 0x00000462;
  uint8_t patch185_data[] = {
    0x7A, 
  };
  APPLY_PATCH(185);

  uint32_t patch186_addr = 0x00000464;
  uint8_t patch186_data[] = {
    0x15, 0x18, 0x29, 
  };
  APPLY_PATCH(186);

  uint32_t patch187_addr = 0x00000468;
  uint8_t patch187_data[] = {
    0x1A, 
  };
  APPLY_PATCH(187);

  uint32_t patch188_addr = 0x0000046A;
  uint8_t patch188_data[] = {
    0x35, 0xCD, 0xC9, 
  };
  APPLY_PATCH(188);

  uint32_t patch189_addr = 0x0000046E;
  uint8_t patch189_data[] = {
    0xDA, 
  };
  APPLY_PATCH(189);

  uint32_t patch190_addr = 0x00000470;
  uint8_t patch190_data[] = {
    0x15, 0xEC, 
  };
  APPLY_PATCH(190);

  uint32_t patch191_addr = 0x00000474;
  uint8_t patch191_data[] = {
    0xBA, 
  };
  APPLY_PATCH(191);

  uint32_t patch192_addr = 0x00000476;
  uint8_t patch192_data[] = {
    0x15, 0x59, 0x29, 
  };
  APPLY_PATCH(192);

  uint32_t patch193_addr = 0x0000047A;
  uint8_t patch193_data[] = {
    0x5B, 
  };
  APPLY_PATCH(193);

  uint32_t patch194_addr = 0x0000047C;
  uint8_t patch194_data[] = {
    0x35, 0x45, 0x29, 
  };
  APPLY_PATCH(194);

  uint32_t patch195_addr = 0x00000480;
  uint8_t patch195_data[] = {
    0x3B, 
  };
  APPLY_PATCH(195);

  uint32_t patch196_addr = 0x00000482;
  uint8_t patch196_data[] = {
    0x15, 0xF3, 0x01, 
  };
  APPLY_PATCH(196);

  uint32_t patch197_addr = 0x00000486;
  uint8_t patch197_data[] = {
    0xF3, 
  };
  APPLY_PATCH(197);

  uint32_t patch198_addr = 0x00000488;
  uint8_t patch198_data[] = {
    0x35, 0x1D, 0x29, 
  };
  APPLY_PATCH(198);

  uint32_t patch199_addr = 0x0000048C;
  uint8_t patch199_data[] = {
    0x93, 
  };
  APPLY_PATCH(199);

  uint32_t patch200_addr = 0x0000048E;
  uint8_t patch200_data[] = {
    0x3D, 0x33, 
  };
  APPLY_PATCH(200);

  uint32_t patch201_addr = 0x00000492;
  uint8_t patch201_data[] = {
    0x79, 
  };
  APPLY_PATCH(201);

  uint32_t patch202_addr = 0x00000494;
  uint8_t patch202_data[] = {
    0x1D, 0xE6, 
  };
  APPLY_PATCH(202);

  uint32_t patch203_addr = 0x00000498;
  uint8_t patch203_data[] = {
    0x19, 
  };
  APPLY_PATCH(203);

  uint32_t patch204_addr = 0x0000049A;
  uint8_t patch204_data[] = {
    0x35, 0xD2, 
  };
  APPLY_PATCH(204);

  uint32_t patch205_addr = 0x0000049E;
  uint8_t patch205_data[] = {
    0xD1, 
  };
  APPLY_PATCH(205);

  uint32_t patch206_addr = 0x000004A0;
  uint8_t patch206_data[] = {
    0x11, 0xBC, 
  };
  APPLY_PATCH(206);

  uint32_t patch207_addr = 0x000004A4;
  uint8_t patch207_data[] = {
    0xB5, 
  };
  APPLY_PATCH(207);

  uint32_t patch208_addr = 0x000004A6;
  uint8_t patch208_data[] = {
    0x19, 0x2F, 0x8D, 
  };
  APPLY_PATCH(208);

  uint32_t patch209_addr = 0x000004AA;
  uint8_t patch209_data[] = {
    0x70, 
  };
  APPLY_PATCH(209);

  uint32_t patch210_addr = 0x000004AC;
  uint8_t patch210_data[] = {
    0x39, 0x2A, 0x05, 
  };
  APPLY_PATCH(210);

  uint32_t patch211_addr = 0x000004B0;
  uint8_t patch211_data[] = {
    0x10, 
  };
  APPLY_PATCH(211);

  uint32_t patch212_addr = 0x000004B2;
  uint8_t patch212_data[] = {
    0x11, 0x3A, 0xE5, 
  };
  APPLY_PATCH(212);

  uint32_t patch213_addr = 0x000004B6;
  uint8_t patch213_data[] = {
    0xD8, 
  };
  APPLY_PATCH(213);

  uint32_t patch214_addr = 0x000004B8;
  uint8_t patch214_data[] = {
    0x31, 0xCE, 
  };
  APPLY_PATCH(214);

  uint32_t patch215_addr = 0x000004BC;
  uint8_t patch215_data[] = {
    0xB8, 
  };
  APPLY_PATCH(215);

  uint32_t patch216_addr = 0x000004BE;
  uint8_t patch216_data[] = {
    0x30, 0xB7, 0x09, 0xA4, 0x19, 0x10, 0x31, 0x66, 0xFA, 
  };
  APPLY_PATCH(216);

  uint32_t patch217_addr = 0x000004C8;
  uint8_t patch217_data[] = {
    0x11, 0xB4, 0xDA, 
  };
  APPLY_PATCH(217);

  uint32_t patch218_addr = 0x000004CC;
  uint8_t patch218_data[] = {
    0x01, 0x3D, 0xDB, 
  };
  APPLY_PATCH(218);

  uint32_t patch219_addr = 0x000004D0;
  uint8_t patch219_data[] = {
    0x28, 0x8B, 0xFB, 
  };
  APPLY_PATCH(219);

  uint32_t patch220_addr = 0x000004D4;
  uint8_t patch220_data[] = {
    0x31, 0x60, 0xBA, 
  };
  APPLY_PATCH(220);

  uint32_t patch221_addr = 0x000004D8;
  uint8_t patch221_data[] = {
    0x11, 0x8F, 0x9A, 
  };
  APPLY_PATCH(221);

  uint32_t patch222_addr = 0x000004DC;
  uint8_t patch222_data[] = {
    0x01, 0x3F, 0x9B, 
  };
  APPLY_PATCH(222);

  uint32_t patch223_addr = 0x000004E0;
  uint8_t patch223_data[] = {
    0x28, 0x5F, 0xBB, 
  };
  APPLY_PATCH(223);

  uint32_t patch224_addr = 0x000004E5;
  uint8_t patch224_data[] = {
    0x1F, 0x31, 0x1A, 0x52, 
  };
  APPLY_PATCH(224);

  uint32_t patch225_addr = 0x000004EA;
  uint8_t patch225_data[] = {
    0x11, 0xCF, 0x52, 
  };
  APPLY_PATCH(225);

  uint32_t patch226_addr = 0x000004EE;
  uint8_t patch226_data[] = {
    0x01, 0xE7, 0x73, 
  };
  APPLY_PATCH(226);

  uint32_t patch227_addr = 0x000004F2;
  uint8_t patch227_data[] = {
    0x28, 0x8F, 0x73, 
  };
  APPLY_PATCH(227);

  uint32_t patch228_addr = 0x000004F6;
  uint8_t patch228_data[] = {
    0x31, 0xEE, 0x12, 
  };
  APPLY_PATCH(228);

  uint32_t patch229_addr = 0x000004FA;
  uint8_t patch229_data[] = {
    0x11, 0x2C, 0x12, 
  };
  APPLY_PATCH(229);

  uint32_t patch230_addr = 0x000004FE;
  uint8_t patch230_data[] = {
    0x01, 0x85, 0x32, 
  };
  APPLY_PATCH(230);

  uint32_t patch231_addr = 0x00000502;
  uint8_t patch231_data[] = {
    0x29, 0xCF, 0x3A, 
  };
  APPLY_PATCH(231);

  uint32_t patch232_addr = 0x00000506;
  uint8_t patch232_data[] = {
    0x1D, 0xC8, 
  };
  APPLY_PATCH(232);

  uint32_t patch233_addr = 0x0000051D;
  uint8_t patch233_data[] = {
    0x03, 
  };
  APPLY_PATCH(233);

  uint32_t patch234_addr = 0x0000051F;
  uint8_t patch234_data[] = {
    0x6B, 
  };
  APPLY_PATCH(234);

  uint32_t patch235_addr = 0x00000522;
  uint8_t patch235_data[] = {
    0xB4, 0x06, 
  };
  APPLY_PATCH(235);

  uint32_t patch236_addr = 0x00000525;
  uint8_t patch236_data[] = {
    0xA2, 0xEC, 0x3B, 
  };
  APPLY_PATCH(236);

  uint32_t patch237_addr = 0x0000052A;
  uint8_t patch237_data[] = {
    0x8C, 0xEF, 
  };
  APPLY_PATCH(237);

  uint32_t patch238_addr = 0x0000052D;
  uint8_t patch238_data[] = {
    0xF8, 
  };
  APPLY_PATCH(238);

  uint32_t patch239_addr = 0x0000052F;
  uint8_t patch239_data[] = {
    0xA8, 
  };
  APPLY_PATCH(239);

  uint32_t patch240_addr = 0x00000532;
  uint8_t patch240_data[] = {
    0xAC, 0x93, 
  };
  APPLY_PATCH(240);

  uint32_t patch241_addr = 0x00000535;
  uint8_t patch241_data[] = {
    0x79, 0xEC, 0xFD, 
  };
  APPLY_PATCH(241);

  uint32_t patch242_addr = 0x0000053A;
  uint8_t patch242_data[] = {
    0x94, 0x7A, 
  };
  APPLY_PATCH(242);

  uint32_t patch243_addr = 0x0000053D;
  uint8_t patch243_data[] = {
    0x7C, 
  };
  APPLY_PATCH(243);

  uint32_t patch244_addr = 0x0000053F;
  uint8_t patch244_data[] = {
    0x9D, 
  };
  APPLY_PATCH(244);

  uint32_t patch245_addr = 0x00000542;
  uint8_t patch245_data[] = {
    0xCC, 0x3D, 
  };
  APPLY_PATCH(245);

  uint32_t patch246_addr = 0x00000545;
  uint8_t patch246_data[] = {
    0x85, 0xE8, 0xB8, 
  };
  APPLY_PATCH(246);

  uint32_t patch247_addr = 0x0000054A;
  uint8_t patch247_data[] = {
    0xC4, 0x40, 
  };
  APPLY_PATCH(247);

  uint32_t patch248_addr = 0x0000054D;
  uint8_t patch248_data[] = {
    0xB8, 
  };
  APPLY_PATCH(248);

  uint32_t patch249_addr = 0x0000054F;
  uint8_t patch249_data[] = {
    0x44, 
  };
  APPLY_PATCH(249);

  uint32_t patch250_addr = 0x00000552;
  uint8_t patch250_data[] = {
    0xE0, 0xFD, 
  };
  APPLY_PATCH(250);

  uint32_t patch251_addr = 0x00000555;
  uint8_t patch251_data[] = {
    0x51, 0xE8, 0x47, 
  };
  APPLY_PATCH(251);

  uint32_t patch252_addr = 0x0000055A;
  uint8_t patch252_data[] = {
    0xF4, 0xAC, 
  };
  APPLY_PATCH(252);

  uint32_t patch253_addr = 0x0000055D;
  uint8_t patch253_data[] = {
    0xD5, 
  };
  APPLY_PATCH(253);

  uint32_t patch254_addr = 0x0000055F;
  uint8_t patch254_data[] = {
    0x81, 
  };
  APPLY_PATCH(254);

  uint32_t patch255_addr = 0x00000562;
  uint8_t patch255_data[] = {
    0xCC, 0x67, 
  };
  APPLY_PATCH(255);

  uint32_t patch256_addr = 0x00000565;
  uint8_t patch256_data[] = {
    0x00, 0xE8, 0xC4, 
  };
  APPLY_PATCH(256);

  uint32_t patch257_addr = 0x0000056A;
  uint8_t patch257_data[] = {
    0x7C, 0x53, 
  };
  APPLY_PATCH(257);

  uint32_t patch258_addr = 0x0000056D;
  uint8_t patch258_data[] = {
    0x50, 
  };
  APPLY_PATCH(258);

  uint32_t patch259_addr = 0x0000056F;
  uint8_t patch259_data[] = {
    0xAE, 
  };
  APPLY_PATCH(259);

  uint32_t patch260_addr = 0x00000572;
  uint8_t patch260_data[] = {
    0xC0, 0x2F, 
  };
  APPLY_PATCH(260);

  uint32_t patch261_addr = 0x00000575;
  uint8_t patch261_data[] = {
    0xD6, 0xE8, 0x8B, 
  };
  APPLY_PATCH(261);

  uint32_t patch262_addr = 0x0000057A;
  uint8_t patch262_data[] = {
    0xD8, 0xD9, 
  };
  APPLY_PATCH(262);

  uint32_t patch263_addr = 0x0000057D;
  uint8_t patch263_data[] = {
    0xAC, 
  };
  APPLY_PATCH(263);

  uint32_t patch264_addr = 0x0000057F;
  uint8_t patch264_data[] = {
    0xA9, 
  };
  APPLY_PATCH(264);

  uint32_t patch265_addr = 0x00000582;
  uint8_t patch265_data[] = {
    0x78, 0x99, 
  };
  APPLY_PATCH(265);

  uint32_t patch266_addr = 0x00000585;
  uint8_t patch266_data[] = {
    0xAF, 0xE0, 0xD3, 
  };
  APPLY_PATCH(266);

  uint32_t patch267_addr = 0x0000058A;
  uint8_t patch267_data[] = {
    0x38, 0x1B, 
  };
  APPLY_PATCH(267);

  uint32_t patch268_addr = 0x0000058D;
  uint8_t patch268_data[] = {
    0x64, 
  };
  APPLY_PATCH(268);

  uint32_t patch269_addr = 0x0000058F;
  uint8_t patch269_data[] = {
    0x8A, 
  };
  APPLY_PATCH(269);

  uint32_t patch270_addr = 0x00000592;
  uint8_t patch270_data[] = {
    0xD8, 0xE8, 
  };
  APPLY_PATCH(270);

  uint32_t patch271_addr = 0x00000595;
  uint8_t patch271_data[] = {
    0x60, 0xE0, 0x99, 
  };
  APPLY_PATCH(271);

  uint32_t patch272_addr = 0x0000059A;
  uint8_t patch272_data[] = {
    0xE0, 0xA0, 0x00, 0x6B, 
  };
  APPLY_PATCH(272);

  uint32_t patch273_addr = 0x0000059F;
  uint8_t patch273_data[] = {
    0x4A, 
  };
  APPLY_PATCH(273);

  uint32_t patch274_addr = 0x000005A2;
  uint8_t patch274_data[] = {
    0xB4, 0x2A, 0x24, 0xE4, 0xE4, 0x70, 
  };
  APPLY_PATCH(274);

  uint32_t patch275_addr = 0x000005AA;
  uint8_t patch275_data[] = {
    0x8C, 0x98, 0x04, 0x9F, 
  };
  APPLY_PATCH(275);

  uint32_t patch276_addr = 0x000005AF;
  uint8_t patch276_data[] = {
    0x77, 
  };
  APPLY_PATCH(276);

  uint32_t patch277_addr = 0x000005B2;
  uint8_t patch277_data[] = {
    0xA0, 0x40, 0x24, 0xE5, 0xE4, 0x4D, 
  };
  APPLY_PATCH(277);

  uint32_t patch278_addr = 0x000005BA;
  uint8_t patch278_data[] = {
    0x94, 0xD7, 0x04, 0xFE, 
  };
  APPLY_PATCH(278);

  uint32_t patch279_addr = 0x000005BF;
  uint8_t patch279_data[] = {
    0x24, 
  };
  APPLY_PATCH(279);

  uint32_t patch280_addr = 0x000005C2;
  uint8_t patch280_data[] = {
    0x18, 0xC7, 0x20, 0xD6, 0xE0, 0xAC, 
  };
  APPLY_PATCH(280);

  uint32_t patch281_addr = 0x000005CA;
  uint8_t patch281_data[] = {
    0x10, 0x64, 0x00, 0xD2, 
  };
  APPLY_PATCH(281);

  uint32_t patch282_addr = 0x000005CF;
  uint8_t patch282_data[] = {
    0xA1, 
  };
  APPLY_PATCH(282);

  uint32_t patch283_addr = 0x000005D2;
  uint8_t patch283_data[] = {
    0xE4, 0x6A, 0x20, 0x54, 0xE0, 0x23, 
  };
  APPLY_PATCH(283);

  uint32_t patch284_addr = 0x000005DA;
  uint8_t patch284_data[] = {
    0x08, 0x66, 0x00, 0x75, 
  };
  APPLY_PATCH(284);

  uint32_t patch285_addr = 0x000005DF;
  uint8_t patch285_data[] = {
    0x21, 
  };
  APPLY_PATCH(285);

  uint32_t patch286_addr = 0x000005E2;
  uint8_t patch286_data[] = {
    0xA4, 0x5C, 0x20, 0xA8, 0xE0, 0xF1, 
  };
  APPLY_PATCH(286);

  uint32_t patch287_addr = 0x000005EA;
  uint8_t patch287_data[] = {
    0xB4, 0x2D, 0xBF, 0x89, 0xBF, 0x16, 
  };
  APPLY_PATCH(287);

  uint32_t patch288_addr = 0x000005F2;
  uint8_t patch288_data[] = {
    0x7C, 0xE0, 
  };
  APPLY_PATCH(288);

  uint32_t patch289_addr = 0x000005F6;
  uint8_t patch289_data[] = {
    0xAA, 0x2D, 
  };
  APPLY_PATCH(289);

  uint32_t patch290_addr = 0x000005FA;
  uint8_t patch290_data[] = {
    0xC8, 0xC8, 
  };
  APPLY_PATCH(290);

  uint32_t patch291_addr = 0x000005FE;
  uint8_t patch291_data[] = {
    0x1E, 0x76, 0x55, 0x79, 
  };
  APPLY_PATCH(291);

  uint32_t patch292_addr = 0x00000604;
  uint8_t patch292_data[] = {
    0x55, 0x61, 0x7A, 
  };
  APPLY_PATCH(292);

  uint32_t patch293_addr = 0x00000608;
  uint8_t patch293_data[] = {
    0x75, 0x04, 
  };
  APPLY_PATCH(293);

  uint32_t patch294_addr = 0x0000060C;
  uint8_t patch294_data[] = {
    0xB0, 0x75, 
  };
  APPLY_PATCH(294);

  uint32_t patch295_addr = 0x00000610;
  uint8_t patch295_data[] = {
    0x59, 0x42, 0xD0, 0x7F, 0x65, 
  };
  APPLY_PATCH(295);

  uint32_t patch296_addr = 0x00000616;
  uint8_t patch296_data[] = {
    0xBB, 0x07, 0x96, 0x7D, 0xB6, 0x4F, 0x3C, 0x7F, 
  };
  APPLY_PATCH(296);

  uint32_t patch297_addr = 0x00000622;
  uint8_t patch297_data[] = {
    0xD9, 0x4F, 
  };
  APPLY_PATCH(297);

  uint32_t patch298_addr = 0x00000626;
  uint8_t patch298_data[] = {
    0xD9, 0xA3, 
  };
  APPLY_PATCH(298);

  uint32_t patch299_addr = 0x0000062A;
  uint8_t patch299_data[] = {
    0xF9, 0xF0, 
  };
  APPLY_PATCH(299);

  uint32_t patch300_addr = 0x0000062E;
  uint8_t patch300_data[] = {
    0xD5, 0x2B, 0x4D, 0x2F, 0x4D, 0x1D, 0x4D, 0x32, 0x4D, 0x0E, 0x1F, 0xA3, 0x0C, 0x62, 0x0A, 0x1E, 
    0x6D, 0x8A, 0x26, 0xBC, 0x2C, 0xC6, 0x44, 0xD0, 0x49, 0xEE, 0x69, 0x68, 0x69, 0x64, 
  };
  APPLY_PATCH(300);

  uint32_t patch301_addr = 0x0000064E;
  uint8_t patch301_data[] = {
    0x1B, 0xB2, 0x28, 0x5F, 0x2E, 0x91, 0x49, 0xBD, 0x26, 0x10, 0x0C, 0x14, 0x3D, 
  };
  APPLY_PATCH(301);

  uint32_t patch302_addr = 0x0000065C;
  uint8_t patch302_data[] = {
    0x28, 0xE3, 0x78, 0xA9, 
  };
  APPLY_PATCH(302);

  uint32_t patch303_addr = 0x00000661;
  uint8_t patch303_data[] = {
    0x27, 0x3A, 0x93, 0x28, 0x51, 0x2F, 0x3C, 
  };
  APPLY_PATCH(303);

  uint32_t patch304_addr = 0x00000669;
  uint8_t patch304_data[] = {
    0xA6, 0x1B, 0x80, 0x0D, 0x60, 0x0A, 0x23, 0x3B, 0xB4, 
  };
  APPLY_PATCH(304);

  uint32_t patch305_addr = 0x00000680;
  uint8_t patch305_data[] = {
    0x50, 0x55, 0xE8, 
  };
  APPLY_PATCH(305);

  uint32_t patch306_addr = 0x00000685;
  uint8_t patch306_data[] = {
    0x31, 0x98, 0x8F, 
  };
  APPLY_PATCH(306);

  uint32_t patch307_addr = 0x0000068A;
  uint8_t patch307_data[] = {
    0x69, 0x8E, 0x31, 0xB0, 
  };
  APPLY_PATCH(307);

  uint32_t patch308_addr = 0x0000068F;
  uint8_t patch308_data[] = {
    0x0E, 0xF9, 0x12, 0x90, 0x38, 0x6B, 
  };
  APPLY_PATCH(308);

  uint32_t patch309_addr = 0x00000696;
  uint8_t patch309_data[] = {
    0x2B, 0x46, 0x62, 0x61, 0x74, 
  };
  APPLY_PATCH(309);

  uint32_t patch310_addr = 0x0000069C;
  uint8_t patch310_data[] = {
    0x43, 0x46, 0x43, 
  };
  APPLY_PATCH(310);

  uint32_t patch311_addr = 0x000006A0;
  uint8_t patch311_data[] = {
    0x2F, 0xB6, 0x50, 
  };
  APPLY_PATCH(311);

  uint32_t patch312_addr = 0x000006A4;
  uint8_t patch312_data[] = {
    0x4E, 0x14, 0x75, 
  };
  APPLY_PATCH(312);

  uint32_t patch313_addr = 0x000006A8;
  uint8_t patch313_data[] = {
    0x4F, 0xD3, 0x4F, 
  };
  APPLY_PATCH(313);

  uint32_t patch314_addr = 0x000006AC;
  uint8_t patch314_data[] = {
    0x07, 0x20, 0x55, 
  };
  APPLY_PATCH(314);

  uint32_t patch315_addr = 0x000006B0;
  uint8_t patch315_data[] = {
    0x46, 0xE0, 0x7D, 
  };
  APPLY_PATCH(315);

  uint32_t patch316_addr = 0x000006B4;
  uint8_t patch316_data[] = {
    0x10, 0x21, 0x1C, 0x3C, 0xCD, 
  };
  APPLY_PATCH(316);

  uint32_t patch317_addr = 0x000006BA;
  uint8_t patch317_data[] = {
    0x4B, 0x97, 0x71, 0x43, 0x7C, 0xC9, 0xE8, 
  };
  APPLY_PATCH(317);

  uint32_t patch318_addr = 0x000006C2;
  uint8_t patch318_data[] = {
    0x6D, 0x80, 0x4D, 0x1C, 0x58, 0x7E, 0xC8, 
  };
  APPLY_PATCH(318);

  uint32_t patch319_addr = 0x000006CB;
  uint8_t patch319_data[] = {
    0x9A, 0x01, 0x78, 0x01, 0x55, 0x79, 
  };
  APPLY_PATCH(319);

  uint32_t patch320_addr = 0x000006D2;
  uint8_t patch320_data[] = {
    0x6B, 0xD7, 
  };
  APPLY_PATCH(320);

  uint32_t patch321_addr = 0x000006D5;
  uint8_t patch321_data[] = {
    0x0D, 0xA1, 0x23, 0xC8, 
  };
  APPLY_PATCH(321);

  uint32_t patch322_addr = 0x000006DB;
  uint8_t patch322_data[] = {
    0xD7, 0xB8, 0x63, 
  };
  APPLY_PATCH(322);

  uint32_t patch323_addr = 0x000006E0;
  uint8_t patch323_data[] = {
    0x99, 0x47, 0x19, 0x32, 
  };
  APPLY_PATCH(323);

  uint32_t patch324_addr = 0x000006E6;
  uint8_t patch324_data[] = {
    0x6F, 0x28, 0xB0, 0x02, 0xC9, 
  };
  APPLY_PATCH(324);

  uint32_t patch325_addr = 0x000006EC;
  uint8_t patch325_data[] = {
    0xC3, 0x78, 0xD1, 0xB4, 0x36, 0xC7, 0x90, 0xDC, 0x6A, 
  };
  APPLY_PATCH(325);

  uint32_t patch326_addr = 0x000006F6;
  uint8_t patch326_data[] = {
    0x03, 0x5C, 
  };
  APPLY_PATCH(326);

  uint32_t patch327_addr = 0x00000700;
  uint8_t patch327_data[] = {
    0x11, 0x6A, 0x77, 
  };
  APPLY_PATCH(327);

  uint32_t patch328_addr = 0x00000704;
  uint8_t patch328_data[] = {
    0xE8, 
  };
  APPLY_PATCH(328);

  uint32_t patch329_addr = 0x00000706;
  uint8_t patch329_data[] = {
    0x92, 0x96, 0x31, 0x88, 0xC9, 
  };
  APPLY_PATCH(329);

  uint32_t patch330_addr = 0x0000070C;
  uint8_t patch330_data[] = {
    0x4E, 0x05, 0x31, 0x27, 0x70, 
  };
  APPLY_PATCH(330);

  uint32_t patch331_addr = 0x00000712;
  uint8_t patch331_data[] = {
    0xE9, 
  };
  APPLY_PATCH(331);

  uint32_t patch332_addr = 0x00000714;
  uint8_t patch332_data[] = {
    0x6E, 0x37, 0x6E, 0x81, 0x31, 0x7F, 0xC9, 
  };
  APPLY_PATCH(332);

  uint32_t patch333_addr = 0x0000071C;
  uint8_t patch333_data[] = {
    0x4E, 0xED, 0x3D, 0x2D, 0x15, 0x46, 0xEC, 
  };
  APPLY_PATCH(333);

  uint32_t patch334_addr = 0x00000724;
  uint8_t patch334_data[] = {
    0x98, 0xBC, 0x62, 0x57, 
  };
  APPLY_PATCH(334);

  uint32_t patch335_addr = 0x00000729;
  uint8_t patch335_data[] = {
    0xED, 0x4F, 0x9B, 0x5C, 0x82, 0x52, 0xD3, 0xD5, 0x7A, 0x67, 
  };
  APPLY_PATCH(335);

  uint32_t patch336_addr = 0x00000734;
  uint8_t patch336_data[] = {
    0x6A, 0x6A, 0x50, 0xD0, 
  };
  APPLY_PATCH(336);

  uint32_t patch337_addr = 0x0000073B;
  uint8_t patch337_data[] = {
    0x7E, 0xB5, 0x33, 0x5C, 
  };
  APPLY_PATCH(337);

  uint32_t patch338_addr = 0x00000740;
  uint8_t patch338_data[] = {
    0x94, 0x71, 0x51, 0x0E, 0xE9, 
  };
  APPLY_PATCH(338);

  uint32_t patch339_addr = 0x00000746;
  uint8_t patch339_data[] = {
    0x6E, 0x4E, 0x02, 0xCF, 0x0A, 0x93, 0x0A, 0x62, 0x8A, 0x8D, 0x78, 0x09, 0x51, 0xC3, 0xE9, 
  };
  APPLY_PATCH(339);

  uint32_t patch340_addr = 0x00000756;
  uint8_t patch340_data[] = {
    0x3A, 0x78, 0xB4, 0xC8, 0x45, 
  };
  APPLY_PATCH(340);

  uint32_t patch341_addr = 0x0000075C;
  uint8_t patch341_data[] = {
    0x03, 0x33, 0x0B, 0xF3, 0x2B, 
  };
  APPLY_PATCH(341);

  uint32_t patch342_addr = 0x00000763;
  uint8_t patch342_data[] = {
    0x62, 0x54, 0x36, 0x6B, 
  };
  APPLY_PATCH(342);

  uint32_t patch343_addr = 0x00000768;
  uint8_t patch343_data[] = {
    0xF1, 0x16, 0x42, 
  };
  APPLY_PATCH(343);

  uint32_t patch344_addr = 0x0000076C;
  uint8_t patch344_data[] = {
    0xAC, 0xEE, 0x74, 0xA9, 
  };
  APPLY_PATCH(344);

  uint32_t patch345_addr = 0x00000774;
  uint8_t patch345_data[] = {
    0x54, 0xFC, 0x40, 
  };
  APPLY_PATCH(345);

  uint32_t patch346_addr = 0x00000778;
  uint8_t patch346_data[] = {
    0x84, 0xF7, 0x74, 0xE9, 0xA8, 0xA0, 0x74, 0xBB, 0x6B, 
  };
  APPLY_PATCH(346);

  uint32_t patch347_addr = 0x00000782;
  uint8_t patch347_data[] = {
    0xD1, 0xDF, 0x6A, 
  };
  APPLY_PATCH(347);

  uint32_t patch348_addr = 0x00000786;
  uint8_t patch348_data[] = {
    0x84, 0x4F, 0xB1, 0x78, 0xC8, 
  };
  APPLY_PATCH(348);

  uint32_t patch349_addr = 0x0000078C;
  uint8_t patch349_data[] = {
    0xE2, 0x94, 0x35, 0xEB, 0x51, 0x8E, 0xE9, 
  };
  APPLY_PATCH(349);

  uint32_t patch350_addr = 0x00000794;
  uint8_t patch350_data[] = {
    0x70, 0x7B, 0x9C, 0x2C, 0x7A, 
  };
  APPLY_PATCH(350);

  uint32_t patch351_addr = 0x0000079A;
  uint8_t patch351_data[] = {
    0x8A, 0xEE, 0x02, 0xB4, 0x79, 0x70, 0xED, 
  };
  APPLY_PATCH(351);

  uint32_t patch352_addr = 0x000007A2;
  uint8_t patch352_data[] = {
    0x1E, 0x08, 0x26, 0x39, 0x26, 0xB6, 0x75, 0x23, 0xCC, 
  };
  APPLY_PATCH(352);

  uint32_t patch353_addr = 0x000007AC;
  uint8_t patch353_data[] = {
    0x82, 0x68, 0x06, 0xB2, 0x55, 0xE7, 0xED, 
  };
  APPLY_PATCH(353);

  uint32_t patch354_addr = 0x000007B4;
  uint8_t patch354_data[] = {
    0x6C, 0x0C, 0x2E, 0xBC, 0x0E, 0x43, 0xB5, 0x9F, 0x48, 
  };
  APPLY_PATCH(354);

  uint32_t patch355_addr = 0x000007BE;
  uint8_t patch355_data[] = {
    0xB4, 0x9D, 0x51, 0xE6, 0xE9, 
  };
  APPLY_PATCH(355);

  uint32_t patch356_addr = 0x000007C4;
  uint8_t patch356_data[] = {
    0x16, 0xFB, 0x6A, 0xC6, 0x31, 0x06, 0xC8, 
  };
  APPLY_PATCH(356);

  uint32_t patch357_addr = 0x000007CC;
  uint8_t patch357_data[] = {
    0x44, 0xCB, 0x35, 0xF0, 0x60, 0xBC, 0x66, 0x42, 0x99, 0x1C, 
  };
  APPLY_PATCH(357);

  uint32_t patch358_addr = 0x000007D8;
  uint8_t patch358_data[] = {
    0xB8, 0x80, 0x38, 0x4E, 
  };
  APPLY_PATCH(358);

  uint32_t patch359_addr = 0x000007DE;
  uint8_t patch359_data[] = {
    0xB9, 0x48, 0xE8, 
  };
  APPLY_PATCH(359);

  uint32_t patch360_addr = 0x000007E2;
  uint8_t patch360_data[] = {
    0xEA, 0xA5, 0xF0, 0xEF, 0x3F, 0xFB, 0x17, 0x53, 0x3D, 0x65, 
  };
  APPLY_PATCH(360);

  uint32_t patch361_addr = 0x000007F2;
  uint8_t patch361_data[] = {
    0x91, 0xA5, 0x66, 
  };
  APPLY_PATCH(361);

  uint32_t patch362_addr = 0x000007F6;
  uint8_t patch362_data[] = {
    0x40, 0x62, 0x4B, 0x10, 0xB1, 0x85, 0x4A, 
  };
  APPLY_PATCH(362);

  uint32_t patch363_addr = 0x000007FE;
  uint8_t patch363_data[] = {
    0x3C, 0xBD, 0x95, 0xCF, 0x77, 
  };
  APPLY_PATCH(363);

  uint32_t patch364_addr = 0x00000804;
  uint8_t patch364_data[] = {
    0xAA, 0x1B, 0xD0, 0xA7, 0xC8, 
  };
  APPLY_PATCH(364);

  uint32_t patch365_addr = 0x0000080A;
  uint8_t patch365_data[] = {
    0x5B, 0x25, 0xF0, 0x38, 
  };
  APPLY_PATCH(365);

  uint32_t patch366_addr = 0x00000810;
  uint8_t patch366_data[] = {
    0x68, 
  };
  APPLY_PATCH(366);

  uint32_t patch367_addr = 0x00000812;
  uint8_t patch367_data[] = {
    0xE8, 
  };
  APPLY_PATCH(367);

  uint32_t patch368_addr = 0x00000814;
  uint8_t patch368_data[] = {
    0x7B, 0x25, 0x63, 0x5E, 0xB5, 0x12, 
  };
  APPLY_PATCH(368);

  uint32_t patch369_addr = 0x0000081C;
  uint8_t patch369_data[] = {
    0x48, 
  };
  APPLY_PATCH(369);

  uint32_t patch370_addr = 0x0000081E;
  uint8_t patch370_data[] = {
    0xC8, 
  };
  APPLY_PATCH(370);

  uint32_t patch371_addr = 0x00000820;
  uint8_t patch371_data[] = {
    0x7F, 0x69, 0x18, 0x07, 
  };
  APPLY_PATCH(371);

  uint32_t patch372_addr = 0x00000830;
  uint8_t patch372_data[] = {
    0x54, 0x3F, 0xAD, 
  };
  APPLY_PATCH(372);

  uint32_t patch373_addr = 0x00000834;
  uint8_t patch373_data[] = {
    0x69, 0x92, 0x25, 0xC3, 0x0D, 0xE0, 0x4C, 
  };
  APPLY_PATCH(373);

  uint32_t patch374_addr = 0x0000083C;
  uint8_t patch374_data[] = {
    0x74, 0x62, 0xCC, 
  };
  APPLY_PATCH(374);

  uint32_t patch375_addr = 0x00000840;
  uint8_t patch375_data[] = {
    0x6B, 0x11, 0x28, 0xC4, 0xA9, 0xC3, 0xE8, 
  };
  APPLY_PATCH(375);

  uint32_t patch376_addr = 0x00000848;
  uint8_t patch376_data[] = {
    0x4B, 0xA8, 0x09, 0x86, 0x88, 0x68, 0xC8, 
  };
  APPLY_PATCH(376);

  uint32_t patch377_addr = 0x00000850;
  uint8_t patch377_data[] = {
    0x6D, 0xED, 0x50, 0xEF, 0xE9, 
  };
  APPLY_PATCH(377);

  uint32_t patch378_addr = 0x00000856;
  uint8_t patch378_data[] = {
    0x1F, 0x4C, 0x3C, 0x1E, 
  };
  APPLY_PATCH(378);

  uint32_t patch379_addr = 0x00000860;
  uint8_t patch379_data[] = {
    0x8E, 0x5A, 0xE9, 0x77, 
  };
  APPLY_PATCH(379);

  uint32_t patch380_addr = 0x00000865;
  uint8_t patch380_data[] = {
    0x77, 
  };
  APPLY_PATCH(380);

  uint32_t patch381_addr = 0x00000867;
  uint8_t patch381_data[] = {
    0x0E, 
  };
  APPLY_PATCH(381);

  uint32_t patch382_addr = 0x00000869;
  uint8_t patch382_data[] = {
    0xEE, 0x56, 0xA8, 0xC9, 0xFF, 0x48, 0xB5, 0x6B, 0x34, 0x6A, 0x9A, 0xA5, 0x46, 0x9F, 0xBB, 0x06, 
    0x9C, 0x06, 0x23, 0x01, 0x00, 0x48, 0xE5, 0x6B, 0x70, 0x6A, 0xFF, 0x41, 0x93, 0x71, 0xEA, 0x06, 
    0x58, 0x96, 0xC4, 0x5A, 0x91, 0x34, 0x60, 0x10, 0x16, 0xE9, 
  };
  APPLY_PATCH(382);

  uint32_t patch383_addr = 0x00000894;
  uint8_t patch383_data[] = {
    0x67, 0xEA, 0x79, 0xF9, 0x70, 0x47, 0xC9, 
  };
  APPLY_PATCH(383);

  uint32_t patch384_addr = 0x0000089C;
  uint8_t patch384_data[] = {
    0x37, 0x51, 0x00, 0x35, 0x3D, 
  };
  APPLY_PATCH(384);

  uint32_t patch385_addr = 0x000008A2;
  uint8_t patch385_data[] = {
    0x2C, 0x93, 0x35, 
  };
  APPLY_PATCH(385);

  uint32_t patch386_addr = 0x000008A6;
  uint8_t patch386_data[] = {
    0x63, 0x54, 0x31, 0xE8, 0x10, 
  };
  APPLY_PATCH(386);

  uint32_t patch387_addr = 0x000008AC;
  uint8_t patch387_data[] = {
    0x5D, 
  };
  APPLY_PATCH(387);

  uint32_t patch388_addr = 0x000008AE;
  uint8_t patch388_data[] = {
    0x30, 0x9C, 0x11, 0x0F, 0x8D, 0x24, 0x7D, 
  };
  APPLY_PATCH(388);

  uint32_t patch389_addr = 0x000008B6;
  uint8_t patch389_data[] = {
    0x10, 0x38, 0x0D, 0xCE, 0xD0, 
  };
  APPLY_PATCH(389);

  uint32_t patch390_addr = 0x000008BC;
  uint8_t patch390_data[] = {
    0x05, 0x38, 0x98, 
  };
  APPLY_PATCH(390);

  uint32_t patch391_addr = 0x000008C1;
  uint8_t patch391_data[] = {
    0xA6, 0x10, 0x2A, 0xE1, 
  };
  APPLY_PATCH(391);

  uint32_t patch392_addr = 0x000008C6;
  uint8_t patch392_data[] = {
    0x67, 0x5F, 0x4B, 0xCE, 0x45, 0x0A, 0x81, 0x13, 0xC0, 
  };
  APPLY_PATCH(392);

  uint32_t patch393_addr = 0x000008D1;
  uint8_t patch393_data[] = {
    0x76, 0x90, 0xFF, 
  };
  APPLY_PATCH(393);

  uint32_t patch394_addr = 0x000008D6;
  uint8_t patch394_data[] = {
    0xB1, 0x17, 
  };
  APPLY_PATCH(394);

  uint32_t patch395_addr = 0x000625E0;
  uint8_t patch395_data[] = {
    0xF4, 0xB6, 0x13, 0x89, 0x3A, 0x62, 0x58, 0x78, 0x90, 0xD9, 0x10, 0x81, 0x97, 0xD3, 0x17, 0x74, 
    0x5D, 0xC6, 0x7A, 0x7D, 0x70, 0x95, 0x6E, 0xB7, 0x45, 0xFD, 0x43, 0x56, 0x88, 0xD3, 
  };
  APPLY_PATCH(395);

  uint32_t patch396_addr = 0x00063620;
  uint8_t patch396_data[] = {
    0xD2, 0x8B, 0xB0, 0xAB, 0x96, 0xF9, 0x77, 0x6B, 0xD7, 0x2D, 0x35, 0x38, 0x15, 0x25, 0x7A, 0xEB, 
    0x78, 0xBA, 0xD9, 0x91, 0x79, 0xD0, 0xBE, 0x8F, 0x9F, 0x5B, 0x1D, 0x90, 0xFC, 0xE5, 0xFE, 0x6B, 
    0xE4, 0x69, 0x67, 
  };
  APPLY_PATCH(396);

  uint32_t patch397_addr = 0x00063644;
  uint8_t patch397_data[] = {
    0x79, 0x50, 0xA4, 0x67, 0xE5, 0xED, 0xA5, 0xDC, 0xC2, 0xCF, 0xC2, 0x52, 0xE3, 0x9D, 0xE0, 
  };
  APPLY_PATCH(397);

  uint32_t patch398_addr = 0x00063654;
  uint8_t patch398_data[] = {
    0x21, 0x60, 0x6F, 0xCD, 0xCF, 0xD8, 0xEA, 0xD7, 
  };
  APPLY_PATCH(398);

  uint32_t patch399_addr = 0x00063660;
  uint8_t patch399_data[] = {
    0x76, 0xE1, 
  };
  APPLY_PATCH(399);

  uint32_t patch400_addr = 0x00063666;
  uint8_t patch400_data[] = {
    0xD7, 0xA8, 
  };
  APPLY_PATCH(400);

  uint32_t patch401_addr = 0x0006366A;
  uint8_t patch401_data[] = {
    0x30, 0x1D, 0x7E, 
  };
  APPLY_PATCH(401);

  uint32_t patch402_addr = 0x0006366E;
  uint8_t patch402_data[] = {
    0xBC, 
  };
  APPLY_PATCH(402);

  uint32_t patch403_addr = 0x00063670;
  uint8_t patch403_data[] = {
    0x9F, 0x4C, 0xDA, 
  };
  APPLY_PATCH(403);

  uint32_t patch404_addr = 0x00063674;
  uint8_t patch404_data[] = {
    0xBA, 0xAE, 0xFB, 
  };
  APPLY_PATCH(404);

  uint32_t patch405_addr = 0x00063678;
  uint8_t patch405_data[] = {
    0xDB, 0x68, 0x19, 
  };
  APPLY_PATCH(405);

  uint32_t patch406_addr = 0x0006367C;
  uint8_t patch406_data[] = {
    0xC6, 0xF5, 0x46, 
  };
  APPLY_PATCH(406);

  uint32_t patch407_addr = 0x00063682;
  uint8_t patch407_data[] = {
    0xA4, 0x4E, 0x2A, 
  };
  APPLY_PATCH(407);

  uint32_t patch408_addr = 0x00063686;
  uint8_t patch408_data[] = {
    0x4B, 
  };
  APPLY_PATCH(408);

  uint32_t patch409_addr = 0x00063688;
  uint8_t patch409_data[] = {
    0xE3, 0x9C, 0x40, 
  };
  APPLY_PATCH(409);

  uint32_t patch410_addr = 0x0006368C;
  uint8_t patch410_data[] = {
    0xA8, 0x0C, 0xC8, 0xBA, 0xAE, 
  };
  APPLY_PATCH(410);

  uint32_t patch411_addr = 0x00063694;
  uint8_t patch411_data[] = {
    0xE4, 
  };
  APPLY_PATCH(411);

  uint32_t patch412_addr = 0x000636A0;
  uint8_t patch412_data[] = {
    0x53, 0x4A, 0xB2, 0x41, 
  };
  APPLY_PATCH(412);

  uint32_t patch413_addr = 0x000636A5;
  uint8_t patch413_data[] = {
    0xD6, 0x3E, 0xE4, 0x74, 0x1B, 0x77, 0x56, 0xD3, 0xE5, 0xB2, 0x17, 0x39, 0xF8, 0xD8, 0x9A, 0x57, 
  };
  APPLY_PATCH(413);

  uint32_t patch414_addr = 0x000636B6;
  uint8_t patch414_data[] = {
    0xF6, 0xCF, 0x38, 0x62, 0xDF, 0x3F, 
  };
  APPLY_PATCH(414);

  uint32_t patch415_addr = 0x000636BE;
  uint8_t patch415_data[] = {
    0x35, 0x48, 0xDB, 0x7F, 0xC6, 0xC9, 0x6C, 
  };
  APPLY_PATCH(415);

  uint32_t patch416_addr = 0x000636C6;
  uint8_t patch416_data[] = {
    0xCC, 0xE7, 0xC5, 0x0C, 0x82, 
  };
  APPLY_PATCH(416);

  uint32_t patch417_addr = 0x000636CC;
  uint8_t patch417_data[] = {
    0x2A, 0x80, 0x2B, 0xF9, 0x20, 0x0E, 0x4E, 0x25, 0x47, 0xE9, 0x64, 0x80, 
  };
  APPLY_PATCH(417);

  uint32_t patch418_addr = 0x000636D9;
  uint8_t patch418_data[] = {
    0x1E, 0xC8, 0xB7, 
  };
  APPLY_PATCH(418);

  uint32_t patch419_addr = 0x000636E2;
  uint8_t patch419_data[] = {
    0xB6, 0xFD, 
  };
  APPLY_PATCH(419);

  uint32_t patch420_addr = 0x000636E8;
  uint8_t patch420_data[] = {
    0x55, 0x95, 0x93, 0x00, 
  };
  APPLY_PATCH(420);

  uint32_t patch421_addr = 0x000636EE;
  uint8_t patch421_data[] = {
    0xF9, 0x34, 0x1E, 0x2E, 0x9D, 0x5C, 0x17, 0xB4, 0x34, 0x32, 0x7A, 0x19, 
  };
  APPLY_PATCH(421);

  uint32_t patch422_addr = 0x000636FC;
  uint8_t patch422_data[] = {
    0x73, 0x28, 
  };
  APPLY_PATCH(422);

  uint32_t patch423_addr = 0x00063700;
  uint8_t patch423_data[] = {
    0xF9, 0xDD, 
  };
  APPLY_PATCH(423);

  uint32_t patch424_addr = 0x00063704;
  uint8_t patch424_data[] = {
    0x07, 0xF6, 0x06, 0x37, 0xC5, 
  };
  APPLY_PATCH(424);

  uint32_t patch425_addr = 0x0006370A;
  uint8_t patch425_data[] = {
    0x04, 0x3B, 0x63, 0x40, 0x62, 0x8E, 0x61, 0x75, 
  };
  APPLY_PATCH(425);

  uint32_t patch426_addr = 0x00063715;
  uint8_t patch426_data[] = {
    0x4B, 
  };
  APPLY_PATCH(426);

  uint32_t patch427_addr = 0x00063722;
  uint8_t patch427_data[] = {
    0xF1, 
  };
  APPLY_PATCH(427);

  uint32_t patch428_addr = 0x00063724;
  uint8_t patch428_data[] = {
    0x30, 
  };
  APPLY_PATCH(428);

  uint32_t patch429_addr = 0x00063726;
  uint8_t patch429_data[] = {
    0x57, 
  };
  APPLY_PATCH(429);

  uint32_t patch430_addr = 0x00063728;
  uint8_t patch430_data[] = {
    0xB6, 0x43, 0x95, 0x33, 0xF4, 0x20, 0xDB, 
  };
  APPLY_PATCH(430);

  uint32_t patch431_addr = 0x00063730;
  uint8_t patch431_data[] = {
    0x1A, 
  };
  APPLY_PATCH(431);

  uint32_t patch432_addr = 0x00063732;
  uint8_t patch432_data[] = {
    0x3A, 0x5F, 0x39, 
  };
  APPLY_PATCH(432);

  uint32_t patch433_addr = 0x00063736;
  uint8_t patch433_data[] = {
    0x58, 0x34, 0x78, 
  };
  APPLY_PATCH(433);

  uint32_t patch434_addr = 0x0006373A;
  uint8_t patch434_data[] = {
    0x5F, 0xBE, 0x5F, 
  };
  APPLY_PATCH(434);

  uint32_t patch435_addr = 0x0006373E;
  uint8_t patch435_data[] = {
    0x5E, 
  };
  APPLY_PATCH(435);

  uint32_t patch436_addr = 0x00063740;
  uint8_t patch436_data[] = {
    0x39, 0x88, 0x38, 0x83, 0x47, 0x3E, 0x46, 0x1B, 0x66, 
  };
  APPLY_PATCH(436);

  uint32_t patch437_addr = 0x0006374A;
  uint8_t patch437_data[] = {
    0x45, 
  };
  APPLY_PATCH(437);

  uint32_t patch438_addr = 0x0006374C;
  uint8_t patch438_data[] = {
    0x26, 0x12, 0xA4, 
  };
  APPLY_PATCH(438);

  uint32_t patch439_addr = 0x00063750;
  uint8_t patch439_data[] = {
    0xA3, 0x84, 0xC2, 
  };
  APPLY_PATCH(439);

  uint32_t patch440_addr = 0x00063762;
  uint8_t patch440_data[] = {
    0x56, 0x97, 0x75, 
  };
  APPLY_PATCH(440);

  uint32_t patch441_addr = 0x00063766;
  uint8_t patch441_data[] = {
    0xB3, 
  };
  APPLY_PATCH(441);

  uint32_t patch442_addr = 0x00063768;
  uint8_t patch442_data[] = {
    0x31, 0x86, 
  };
  APPLY_PATCH(442);

  uint32_t patch443_addr = 0x0006376C;
  uint8_t patch443_data[] = {
    0x51, 0x8C, 0xBF, 0x45, 
  };
  APPLY_PATCH(443);

  uint32_t patch444_addr = 0x00063772;
  uint8_t patch444_data[] = {
    0xBE, 0xE3, 
  };
  APPLY_PATCH(444);

  uint32_t patch445_addr = 0x00063776;
  uint8_t patch445_data[] = {
    0xDC, 
  };
  APPLY_PATCH(445);

  uint32_t patch446_addr = 0x0006377A;
  uint8_t patch446_data[] = {
    0x3C, 0x62, 
  };
  APPLY_PATCH(446);

  uint32_t patch447_addr = 0x0006377E;
  uint8_t patch447_data[] = {
    0x1A, 0x3A, 
  };
  APPLY_PATCH(447);

  uint32_t patch448_addr = 0x00063782;
  uint8_t patch448_data[] = {
    0xD9, 0x24, 0xAF, 
  };
  APPLY_PATCH(448);

  uint32_t patch449_addr = 0x00063788;
  uint8_t patch449_data[] = {
    0x26, 0x98, 0x25, 
  };
  APPLY_PATCH(449);

  uint32_t patch450_addr = 0x0006378C;
  uint8_t patch450_data[] = {
    0x0B, 
  };
  APPLY_PATCH(450);

  uint32_t patch451_addr = 0x0006378E;
  uint8_t patch451_data[] = {
    0x6A, 
  };
  APPLY_PATCH(451);

  uint32_t patch452_addr = 0x00063790;
  uint8_t patch452_data[] = {
    0x61, 
  };
  APPLY_PATCH(452);

  uint32_t patch453_addr = 0x00063792;
  uint8_t patch453_data[] = {
    0x80, 0x73, 0xA7, 
  };
  APPLY_PATCH(453);

  uint32_t patch454_addr = 0x00063796;
  uint8_t patch454_data[] = {
    0xC6, 
  };
  APPLY_PATCH(454);

  uint32_t patch455_addr = 0x000637A6;
  uint8_t patch455_data[] = {
    0xF3, 0xB2, 0x39, 0xD9, 
  };
  APPLY_PATCH(455);

  uint32_t patch456_addr = 0x000637AC;
  uint8_t patch456_data[] = {
    0x70, 
  };
  APPLY_PATCH(456);

  uint32_t patch457_addr = 0x000637B0;
  uint8_t patch457_data[] = {
    0x7F, 
  };
  APPLY_PATCH(457);

  uint32_t patch458_addr = 0x000637B2;
  uint8_t patch458_data[] = {
    0x9D, 0xFB, 0x94, 0x51, 0xAB, 
  };
  APPLY_PATCH(458);

  uint32_t patch459_addr = 0x000637BC;
  uint8_t patch459_data[] = {
    0xC9, 
  };
  APPLY_PATCH(459);

  uint32_t patch460_addr = 0x000637C2;
  uint8_t patch460_data[] = {
    0xA3, 
  };
  APPLY_PATCH(460);

  uint32_t patch461_addr = 0x000637C6;
  uint8_t patch461_data[] = {
    0xCA, 
  };
  APPLY_PATCH(461);

  uint32_t patch462_addr = 0x000637CA;
  uint8_t patch462_data[] = {
    0xC1, 
  };
  APPLY_PATCH(462);

  uint32_t patch463_addr = 0x000637CC;
  uint8_t patch463_data[] = {
    0x29, 0x47, 0x28, 0x22, 
  };
  APPLY_PATCH(463);

  uint32_t patch464_addr = 0x000637E2;
  uint8_t patch464_data[] = {
    0x56, 
  };
  APPLY_PATCH(464);

  uint32_t patch465_addr = 0x000637E4;
  uint8_t patch465_data[] = {
    0xDF, 0x4A, 0xFE, 
  };
  APPLY_PATCH(465);

  uint32_t patch466_addr = 0x000637E8;
  uint8_t patch466_data[] = {
    0x15, 0xAD, 
  };
  APPLY_PATCH(466);

  uint32_t patch467_addr = 0x000637EC;
  uint8_t patch467_data[] = {
    0x7C, 0xA0, 
  };
  APPLY_PATCH(467);

  uint32_t patch468_addr = 0x000637F0;
  uint8_t patch468_data[] = {
    0x92, 
  };
  APPLY_PATCH(468);

  uint32_t patch469_addr = 0x000637F2;
  uint8_t patch469_data[] = {
    0xB1, 
  };
  APPLY_PATCH(469);

  uint32_t patch470_addr = 0x000637F8;
  uint8_t patch470_data[] = {
    0xFE, 
  };
  APPLY_PATCH(470);

  uint32_t patch471_addr = 0x000637FC;
  uint8_t patch471_data[] = {
    0x35, 0xF6, 
  };
  APPLY_PATCH(471);

  uint32_t patch472_addr = 0x00063800;
  uint8_t patch472_data[] = {
    0xBA, 0xF0, 0xD8, 
  };
  APPLY_PATCH(472);

  uint32_t patch473_addr = 0x00063806;
  uint8_t patch473_data[] = {
    0xF9, 
  };
  APPLY_PATCH(473);

  uint32_t patch474_addr = 0x0006380A;
  uint8_t patch474_data[] = {
    0x26, 
  };
  APPLY_PATCH(474);

  uint32_t patch475_addr = 0x0006380C;
  uint8_t patch475_data[] = {
    0x24, 
  };
  APPLY_PATCH(475);

  uint32_t patch476_addr = 0x0006380E;
  uint8_t patch476_data[] = {
    0x05, 
  };
  APPLY_PATCH(476);

  uint32_t patch477_addr = 0x00063810;
  uint8_t patch477_data[] = {
    0x62, 
  };
  APPLY_PATCH(477);

  uint32_t patch478_addr = 0x00063812;
  uint8_t patch478_data[] = {
    0x80, 0xAD, 0xAE, 
  };
  APPLY_PATCH(478);

  uint32_t patch479_addr = 0x00063820;
  uint8_t patch479_data[] = {
    0xB2, 0x87, 
  };
  APPLY_PATCH(479);

  uint32_t patch480_addr = 0x00063824;
  uint8_t patch480_data[] = {
    0x11, 0xC8, 
  };
  APPLY_PATCH(480);

  uint32_t patch481_addr = 0x0006382A;
  uint8_t patch481_data[] = {
    0x14, 0x24, 
  };
  APPLY_PATCH(481);

  uint32_t patch482_addr = 0x0006382E;
  uint8_t patch482_data[] = {
    0x7A, 0xE3, 
  };
  APPLY_PATCH(482);

  uint32_t patch483_addr = 0x00063834;
  uint8_t patch483_data[] = {
    0x9B, 0xD7, 
  };
  APPLY_PATCH(483);

  uint32_t patch484_addr = 0x00063838;
  uint8_t patch484_data[] = {
    0x99, 0x23, 
  };
  APPLY_PATCH(484);

  uint32_t patch485_addr = 0x0006383C;
  uint8_t patch485_data[] = {
    0x9F, 0xED, 
  };
  APPLY_PATCH(485);

  uint32_t patch486_addr = 0x00063840;
  uint8_t patch486_data[] = {
    0x58, 0x48, 
  };
  APPLY_PATCH(486);

  uint32_t patch487_addr = 0x00063846;
  uint8_t patch487_data[] = {
    0x86, 0x7F, 
  };
  APPLY_PATCH(487);

  uint32_t patch488_addr = 0x0006384C;
  uint8_t patch488_data[] = {
    0x84, 0xF6, 
  };
  APPLY_PATCH(488);

  uint32_t patch489_addr = 0x0006384F;
  uint8_t patch489_data[] = {
    0x5E, 
  };
  APPLY_PATCH(489);

  uint32_t patch490_addr = 0x00063851;
  uint8_t patch490_data[] = {
    0xA4, 
  };
  APPLY_PATCH(490);

  uint32_t patch491_addr = 0x00063853;
  uint8_t patch491_data[] = {
    0x7A, 
  };
  APPLY_PATCH(491);

  uint32_t patch492_addr = 0x00063855;
  uint8_t patch492_data[] = {
    0x59, 
  };
  APPLY_PATCH(492);

  uint32_t patch493_addr = 0x00063857;
  uint8_t patch493_data[] = {
    0xC1, 0x6F, 0xC4, 
  };
  APPLY_PATCH(493);

  uint32_t patch494_addr = 0x00063863;
  uint8_t patch494_data[] = {
    0x54, 
  };
  APPLY_PATCH(494);

  uint32_t patch495_addr = 0x0006386C;
  uint8_t patch495_data[] = {
    0x33, 0xF1, 0xB7, 
  };
  APPLY_PATCH(495);

  uint32_t patch496_addr = 0x00063871;
  uint8_t patch496_data[] = {
    0x33, 0x3E, 0x94, 
  };
  APPLY_PATCH(496);

  uint32_t patch497_addr = 0x00063877;
  uint8_t patch497_data[] = {
    0xFD, 
  };
  APPLY_PATCH(497);

  uint32_t patch498_addr = 0x00063879;
  uint8_t patch498_data[] = {
    0x70, 
  };
  APPLY_PATCH(498);

  uint32_t patch499_addr = 0x0006387D;
  uint8_t patch499_data[] = {
    0xE9, 
  };
  APPLY_PATCH(499);

  uint32_t patch500_addr = 0x0006387F;
  uint8_t patch500_data[] = {
    0x5D, 
  };
  APPLY_PATCH(500);

  uint32_t patch501_addr = 0x00063881;
  uint8_t patch501_data[] = {
    0xDC, 0x86, 0x42, 
  };
  APPLY_PATCH(501);

  uint32_t patch502_addr = 0x00063885;
  uint8_t patch502_data[] = {
    0x74, 
  };
  APPLY_PATCH(502);

  uint32_t patch503_addr = 0x0006388A;
  uint8_t patch503_data[] = {
    0x02, 0x21, 
  };
  APPLY_PATCH(503);

  uint32_t patch504_addr = 0x00063890;
  uint8_t patch504_data[] = {
    0xC3, 
  };
  APPLY_PATCH(504);

  uint32_t patch505_addr = 0x00063894;
  uint8_t patch505_data[] = {
    0xE9, 
  };
  APPLY_PATCH(505);

  uint32_t patch506_addr = 0x00063896;
  uint8_t patch506_data[] = {
    0x26, 
  };
  APPLY_PATCH(506);

  uint32_t patch507_addr = 0x0006389A;
  uint8_t patch507_data[] = {
    0x0A, 0x47, 
  };
  APPLY_PATCH(507);

  uint32_t patch508_addr = 0x000638A2;
  uint8_t patch508_data[] = {
    0xB2, 
  };
  APPLY_PATCH(508);

  uint32_t patch509_addr = 0x000638AB;
  uint8_t patch509_data[] = {
    0x77, 
  };
  APPLY_PATCH(509);

  uint32_t patch510_addr = 0x000638AD;
  uint8_t patch510_data[] = {
    0xE9, 
  };
  APPLY_PATCH(510);

  uint32_t patch511_addr = 0x000638AF;
  uint8_t patch511_data[] = {
    0x04, 
  };
  APPLY_PATCH(511);

  uint32_t patch512_addr = 0x000638B1;
  uint8_t patch512_data[] = {
    0xFC, 0xBA, 0xEE, 
  };
  APPLY_PATCH(512);

  uint32_t patch513_addr = 0x000638B5;
  uint8_t patch513_data[] = {
    0xB9, 
  };
  APPLY_PATCH(513);

  uint32_t patch514_addr = 0x000638B7;
  uint8_t patch514_data[] = {
    0xB3, 
  };
  APPLY_PATCH(514);

  uint32_t patch515_addr = 0x000638B9;
  uint8_t patch515_data[] = {
    0x6E, 
  };
  APPLY_PATCH(515);

  uint32_t patch516_addr = 0x000638BB;
  uint8_t patch516_data[] = {
    0x4C, 
  };
  APPLY_PATCH(516);

  uint32_t patch517_addr = 0x000638BD;
  uint8_t patch517_data[] = {
    0x9C, 
  };
  APPLY_PATCH(517);

  uint32_t patch518_addr = 0x000638BF;
  uint8_t patch518_data[] = {
    0x48, 0x19, 0x6B, 0x06, 0xCC, 
  };
  APPLY_PATCH(518);

  uint32_t patch519_addr = 0x000638C5;
  uint8_t patch519_data[] = {
    0xD6, 
  };
  APPLY_PATCH(519);

  uint32_t patch520_addr = 0x000638C9;
  uint8_t patch520_data[] = {
    0x0C, 0x85, 
  };
  APPLY_PATCH(520);

  uint32_t patch521_addr = 0x000638D1;
  uint8_t patch521_data[] = {
    0x0E, 
  };
  APPLY_PATCH(521);

  uint32_t patch522_addr = 0x000638D5;
  uint8_t patch522_data[] = {
    0xE9, 
  };
  APPLY_PATCH(522);

  uint32_t patch523_addr = 0x000638D7;
  uint8_t patch523_data[] = {
    0x9C, 
  };
  APPLY_PATCH(523);

  uint32_t patch524_addr = 0x000638DA;
  uint8_t patch524_data[] = {
    0x8A, 
  };
  APPLY_PATCH(524);

  uint32_t patch525_addr = 0x000638E2;
  uint8_t patch525_data[] = {
    0x74, 0xE1, 0xDB, 0x88, 0xBD, 0xD1, 0xF0, 0x9D, 0x11, 0x08, 0x36, 0x4E, 0x57, 0x28, 0xBD, 0x21, 
    0x3C, 0x53, 0xB2, 0xBB, 0x52, 0x4E, 
  };
  APPLY_PATCH(525);

  uint32_t patch526_addr = 0x000638F9;
  uint8_t patch526_data[] = {
    0x62, 0x7B, 0x8C, 
  };
  APPLY_PATCH(526);

  uint32_t patch527_addr = 0x000638FE;
  uint8_t patch527_data[] = {
    0xB1, 0x2D, 
  };
  APPLY_PATCH(527);

  uint32_t patch528_addr = 0x00063902;
  uint8_t patch528_data[] = {
    0x26, 0x7F, 0x27, 0xEA, 0xE4, 
  };
  APPLY_PATCH(528);

  uint32_t patch529_addr = 0x00063908;
  uint8_t patch529_data[] = {
    0xA3, 0x7E, 0x04, 0x3F, 0x80, 0x45, 
  };
  APPLY_PATCH(529);

  uint32_t patch530_addr = 0x00063910;
  uint8_t patch530_data[] = {
    0x45, 0x7D, 0x63, 0x28, 0x60, 0x76, 0x81, 0x7B, 
  };
  APPLY_PATCH(530);

  uint32_t patch531_addr = 0x0006391D;
  uint8_t patch531_data[] = {
    0x85, 
  };
  APPLY_PATCH(531);

  uint32_t patch532_addr = 0x00063920;
  uint8_t patch532_data[] = {
    0x51, 0xE7, 0xF4, 0xD6, 0x3A, 0x66, 0xF8, 0x8C, 0xB8, 0x5C, 0x3E, 0x50, 0xF9, 0x4F, 0xFE, 0x10, 
    0xFC, 0x85, 0x7C, 0x38, 0xFD, 0xC6, 0x02, 0x5F, 0x42, 0x66, 0x00, 0xD5, 0x23, 0xEE, 0xA3, 0x2D, 
    0xA5, 0xA9, 0xC2, 0x9F, 0x05, 0x2B, 0x82, 0x36, 0x03, 0xB7, 0x83, 0xD3, 0x23, 0x2A, 0x01, 0xC5, 
    0xA1, 0xBD, 0x6E, 0x61, 0x6F, 0xCF, 0x6C, 0xCC, 
  };
  APPLY_PATCH(532);

  uint32_t patch533_addr = 0x0006395C;
  uint8_t patch533_data[] = {
    0xEB, 0x5F, 
  };
  APPLY_PATCH(533);

  uint32_t patch534_addr = 0x00063964;
  uint8_t patch534_data[] = {
    0x95, 
  };
  APPLY_PATCH(534);

  uint32_t patch535_addr = 0x00063966;
  uint8_t patch535_data[] = {
    0xF3, 
  };
  APPLY_PATCH(535);

  uint32_t patch536_addr = 0x00063968;
  uint8_t patch536_data[] = {
    0x70, 
  };
  APPLY_PATCH(536);

  uint32_t patch537_addr = 0x0006396A;
  uint8_t patch537_data[] = {
    0x31, 0x96, 0xB0, 
  };
  APPLY_PATCH(537);

  uint32_t patch538_addr = 0x00063970;
  uint8_t patch538_data[] = {
    0xD1, 
  };
  APPLY_PATCH(538);

  uint32_t patch539_addr = 0x00063972;
  uint8_t patch539_data[] = {
    0xFF, 
  };
  APPLY_PATCH(539);

  uint32_t patch540_addr = 0x00063976;
  uint8_t patch540_data[] = {
    0x7D, 0x11, 0xD4, 0x44, 0x3C, 0x62, 0xA6, 0x0A, 
  };
  APPLY_PATCH(540);

  uint32_t patch541_addr = 0x00063980;
  uint8_t patch541_data[] = {
    0xBD, 0xFF, 0xBB, 0x28, 0x30, 0x10, 0xF1, 
  };
  APPLY_PATCH(541);

  uint32_t patch542_addr = 0x00063988;
  uint8_t patch542_data[] = {
    0x45, 0xAD, 
  };
  APPLY_PATCH(542);

  uint32_t patch543_addr = 0x0006398C;
  uint8_t patch543_data[] = {
    0x4F, 0xC3, 0x2F, 
  };
  APPLY_PATCH(543);

  uint32_t patch544_addr = 0x00063990;
  uint8_t patch544_data[] = {
    0x25, 
  };
  APPLY_PATCH(544);

  uint32_t patch545_addr = 0x00063992;
  uint8_t patch545_data[] = {
    0x45, 
  };
  APPLY_PATCH(545);

  uint32_t patch546_addr = 0x00063994;
  uint8_t patch546_data[] = {
    0x49, 0xFA, 0x6B, 
  };
  APPLY_PATCH(546);

  uint32_t patch547_addr = 0x000639A2;
  uint8_t patch547_data[] = {
    0xF0, 
  };
  APPLY_PATCH(547);

  uint32_t patch548_addr = 0x000639A4;
  uint8_t patch548_data[] = {
    0x39, 
  };
  APPLY_PATCH(548);

  uint32_t patch549_addr = 0x000639A6;
  uint8_t patch549_data[] = {
    0x5F, 
  };
  APPLY_PATCH(549);

  uint32_t patch550_addr = 0x000639AA;
  uint8_t patch550_data[] = {
    0x11, 0x48, 
  };
  APPLY_PATCH(550);

  uint32_t patch551_addr = 0x000639AD;
  uint8_t patch551_data[] = {
    0x5B, 
  };
  APPLY_PATCH(551);

  uint32_t patch552_addr = 0x000639B0;
  uint8_t patch552_data[] = {
    0xBA, 
  };
  APPLY_PATCH(552);

  uint32_t patch553_addr = 0x000639B4;
  uint8_t patch553_data[] = {
    0xFC, 0x2D, 
  };
  APPLY_PATCH(553);

  uint32_t patch554_addr = 0x000639B9;
  uint8_t patch554_data[] = {
    0x2C, 0xF5, 0x10, 0x16, 
  };
  APPLY_PATCH(554);

  uint32_t patch555_addr = 0x000639BE;
  uint8_t patch555_data[] = {
    0xD3, 0x87, 
  };
  APPLY_PATCH(555);

  uint32_t patch556_addr = 0x000639C2;
  uint8_t patch556_data[] = {
    0x5C, 0x72, 0x75, 0x66, 0x53, 0x09, 0x06, 0x29, 0xB8, 0xDD, 0xB1, 0x6A, 0x8E, 0x13, 0x84, 0x3E, 
    0xA5, 0xC4, 0xCB, 0xF8, 
  };
  APPLY_PATCH(556);

  uint32_t patch557_addr = 0x000639D9;
  uint8_t patch557_data[] = {
    0x11, 
  };
  APPLY_PATCH(557);

  uint32_t patch558_addr = 0x000639DC;
  uint8_t patch558_data[] = {
    0xE3, 0xF8, 
  };
  APPLY_PATCH(558);

  uint32_t patch559_addr = 0x000639E2;
  uint8_t patch559_data[] = {
    0x96, 0xA9, 0x9C, 
  };
  APPLY_PATCH(559);

  uint32_t patch560_addr = 0x000639E6;
  uint8_t patch560_data[] = {
    0xFA, 
  };
  APPLY_PATCH(560);

  uint32_t patch561_addr = 0x000639E8;
  uint8_t patch561_data[] = {
    0x30, 0xAD, 
  };
  APPLY_PATCH(561);

  uint32_t patch562_addr = 0x000639EC;
  uint8_t patch562_data[] = {
    0xB8, 0xA4, 
  };
  APPLY_PATCH(562);

  uint32_t patch563_addr = 0x000639F0;
  uint8_t patch563_data[] = {
    0xDE, 0x91, 
  };
  APPLY_PATCH(563);

  uint32_t patch564_addr = 0x000639F4;
  uint8_t patch564_data[] = {
    0x17, 0xCD, 0x76, 0x9B, 
  };
  APPLY_PATCH(564);

  uint32_t patch565_addr = 0x000639FA;
  uint8_t patch565_data[] = {
    0xC6, 0x7A, 
  };
  APPLY_PATCH(565);

  uint32_t patch566_addr = 0x000639FE;
  uint8_t patch566_data[] = {
    0x13, 0xCB, 0x3C, 0xE4, 0xA6, 
  };
  APPLY_PATCH(566);

  uint32_t patch567_addr = 0x00063A04;
  uint8_t patch567_data[] = {
    0x07, 
  };
  APPLY_PATCH(567);

  uint32_t patch568_addr = 0x00063A08;
  uint8_t patch568_data[] = {
    0xBB, 0x90, 0x82, 0x94, 0x05, 
  };
  APPLY_PATCH(568);

  uint32_t patch569_addr = 0x00063A0E;
  uint8_t patch569_data[] = {
    0x86, 0xCE, 0x43, 
  };
  APPLY_PATCH(569);

  uint32_t patch570_addr = 0x00063A12;
  uint8_t patch570_data[] = {
    0xA5, 0xA9, 
  };
  APPLY_PATCH(570);

  uint32_t patch571_addr = 0x00063A16;
  uint8_t patch571_data[] = {
    0xC3, 0xA6, 
  };
  APPLY_PATCH(571);

  uint32_t patch572_addr = 0x00063A1A;
  uint8_t patch572_data[] = {
    0xC1, 
  };
  APPLY_PATCH(572);

  uint32_t patch573_addr = 0x00063A23;
  uint8_t patch573_data[] = {
    0xBA, 
  };
  APPLY_PATCH(573);

  uint32_t patch574_addr = 0x00063A25;
  uint8_t patch574_data[] = {
    0xCC, 
  };
  APPLY_PATCH(574);

  uint32_t patch575_addr = 0x00063A27;
  uint8_t patch575_data[] = {
    0x7B, 
  };
  APPLY_PATCH(575);

  uint32_t patch576_addr = 0x00063A29;
  uint8_t patch576_data[] = {
    0x29, 
  };
  APPLY_PATCH(576);

  uint32_t patch577_addr = 0x00063A2B;
  uint8_t patch577_data[] = {
    0x24, 
  };
  APPLY_PATCH(577);

  uint32_t patch578_addr = 0x00063A2D;
  uint8_t patch578_data[] = {
    0x52, 
  };
  APPLY_PATCH(578);

  uint32_t patch579_addr = 0x00063A2F;
  uint8_t patch579_data[] = {
    0xEB, 0x5F, 
  };
  APPLY_PATCH(579);

  uint32_t patch580_addr = 0x00063A33;
  uint8_t patch580_data[] = {
    0xFC, 0x9D, 0xAF, 
  };
  APPLY_PATCH(580);

  uint32_t patch581_addr = 0x00063A37;
  uint8_t patch581_data[] = {
    0x9A, 
  };
  APPLY_PATCH(581);

  uint32_t patch582_addr = 0x00063A3B;
  uint8_t patch582_data[] = {
    0x81, 
  };
  APPLY_PATCH(582);

  uint32_t patch583_addr = 0x00063A3D;
  uint8_t patch583_data[] = {
    0xF9, 
  };
  APPLY_PATCH(583);

  uint32_t patch584_addr = 0x00063A3F;
  uint8_t patch584_data[] = {
    0x7E, 
  };
  APPLY_PATCH(584);

  uint32_t patch585_addr = 0x00063A41;
  uint8_t patch585_data[] = {
    0x58, 
  };
  APPLY_PATCH(585);

  uint32_t patch586_addr = 0x00063A45;
  uint8_t patch586_data[] = {
    0x55, 
  };
  APPLY_PATCH(586);

  uint32_t patch587_addr = 0x00063A47;
  uint8_t patch587_data[] = {
    0x67, 
  };
  APPLY_PATCH(587);

  uint32_t patch588_addr = 0x00063A49;
  uint8_t patch588_data[] = {
    0xE9, 
  };
  APPLY_PATCH(588);

  uint32_t patch589_addr = 0x00063A4B;
  uint8_t patch589_data[] = {
    0xD8, 
  };
  APPLY_PATCH(589);

  uint32_t patch590_addr = 0x00063A4D;
  uint8_t patch590_data[] = {
    0xFA, 
  };
  APPLY_PATCH(590);

  uint32_t patch591_addr = 0x00063A4F;
  uint8_t patch591_data[] = {
    0x6F, 
  };
  APPLY_PATCH(591);

  uint32_t patch592_addr = 0x00063A53;
  uint8_t patch592_data[] = {
    0x47, 
  };
  APPLY_PATCH(592);

  uint32_t patch593_addr = 0x00063A55;
  uint8_t patch593_data[] = {
    0x68, 
  };
  APPLY_PATCH(593);

  uint32_t patch594_addr = 0x00063A59;
  uint8_t patch594_data[] = {
    0xC0, 
  };
  APPLY_PATCH(594);

  uint32_t patch595_addr = 0x00063A5B;
  uint8_t patch595_data[] = {
    0xC3, 0x8A, 0x19, 
  };
  APPLY_PATCH(595);

  uint32_t patch596_addr = 0x00063A63;
  uint8_t patch596_data[] = {
    0x58, 0xD5, 0xEE, 0xF2, 0x9D, 0x13, 0x46, 
  };
  APPLY_PATCH(596);

  uint32_t patch597_addr = 0x00063A6C;
  uint8_t patch597_data[] = {
    0x70, 0xF5, 
  };
  APPLY_PATCH(597);

  uint32_t patch598_addr = 0x00063A70;
  uint8_t patch598_data[] = {
    0x9E, 0x23, 0xBF, 0x93, 
  };
  APPLY_PATCH(598);

  uint32_t patch599_addr = 0x00063A78;
  uint8_t patch599_data[] = {
    0xFD, 0x0F, 
  };
  APPLY_PATCH(599);

  uint32_t patch600_addr = 0x00063A7C;
  uint8_t patch600_data[] = {
    0x3B, 0x99, 
  };
  APPLY_PATCH(600);

  uint32_t patch601_addr = 0x00063A80;
  uint8_t patch601_data[] = {
    0xB8, 0xDC, 0xD9, 0x57, 
  };
  APPLY_PATCH(601);

  uint32_t patch602_addr = 0x00063A86;
  uint8_t patch602_data[] = {
    0xEE, 0x7B, 
  };
  APPLY_PATCH(602);

  uint32_t patch603_addr = 0x00063A8A;
  uint8_t patch603_data[] = {
    0x27, 0x39, 0x2C, 0x3D, 0x0A, 0x87, 0x63, 0xFD, 0x81, 0xEA, 0xA6, 0x63, 
  };
  APPLY_PATCH(603);

  uint32_t patch604_addr = 0x00063AA2;
  uint8_t patch604_data[] = {
    0xD2, 
  };
  APPLY_PATCH(604);

  uint32_t patch605_addr = 0x00063AA7;
  uint8_t patch605_data[] = {
    0xD9, 
  };
  APPLY_PATCH(605);

  uint32_t patch606_addr = 0x00063AAA;
  uint8_t patch606_data[] = {
    0xD2, 0x46, 
  };
  APPLY_PATCH(606);

  uint32_t patch607_addr = 0x00063AAD;
  uint8_t patch607_data[] = {
    0xE9, 
  };
  APPLY_PATCH(607);

  uint32_t patch608_addr = 0x00063AAF;
  uint8_t patch608_data[] = {
    0x04, 
  };
  APPLY_PATCH(608);

  uint32_t patch609_addr = 0x00063AB3;
  uint8_t patch609_data[] = {
    0xED, 0xFE, 0xD2, 
  };
  APPLY_PATCH(609);

  uint32_t patch610_addr = 0x00063ABA;
  uint8_t patch610_data[] = {
    0x1A, 0x37, 
  };
  APPLY_PATCH(610);

  uint32_t patch611_addr = 0x00063AC1;
  uint8_t patch611_data[] = {
    0x7B, 0x3A, 0xC0, 
  };
  APPLY_PATCH(611);

  uint32_t patch612_addr = 0x00063AC8;
  uint8_t patch612_data[] = {
    0x79, 0x35, 
  };
  APPLY_PATCH(612);

  uint32_t patch613_addr = 0x00063ACD;
  uint8_t patch613_data[] = {
    0xBC, 0x6D, 0xC1, 
  };
  APPLY_PATCH(613);

  uint32_t patch614_addr = 0x00063AD1;
  uint8_t patch614_data[] = {
    0x0E, 
  };
  APPLY_PATCH(614);

  uint32_t patch615_addr = 0x00063AD4;
  uint8_t patch615_data[] = {
    0x8B, 0xED, 0x47, 0x84, 
  };
  APPLY_PATCH(615);

  uint32_t patch616_addr = 0x00063AE2;
  uint8_t patch616_data[] = {
    0x57, 0xE5, 
  };
  APPLY_PATCH(616);

  uint32_t patch617_addr = 0x00063AF2;
  uint8_t patch617_data[] = {
    0xDB, 0x54, 
  };
  APPLY_PATCH(617);

  uint32_t patch618_addr = 0x00063AF5;
  uint8_t patch618_data[] = {
    0xC8, 
  };
  APPLY_PATCH(618);

  uint32_t patch619_addr = 0x00063AFE;
  uint8_t patch619_data[] = {
    0x6F, 0x2D, 
  };
  APPLY_PATCH(619);

  uint32_t patch620_addr = 0x00063B04;
  uint8_t patch620_data[] = {
    0xEC, 0xDF, 0xA5, 
  };
  APPLY_PATCH(620);

  uint32_t patch621_addr = 0x00063B0D;
  uint8_t patch621_data[] = {
    0x79, 0x0A, 0xBB, 
  };
  APPLY_PATCH(621);

  uint32_t patch622_addr = 0x00064260;
  uint8_t patch622_data[] = {
    0xD4, 
  };
  APPLY_PATCH(622);

  uint32_t patch623_addr = 0x00064262;
  uint8_t patch623_data[] = {
    0xF5, 0x50, 0xF2, 0xFA, 0x92, 0x85, 0x71, 0x6F, 0x1F, 0x23, 0x3C, 0xC4, 0x58, 0xAC, 0x19, 0x23, 
    0x85, 0xE4, 
  };
  APPLY_PATCH(623);

  uint32_t patch624_addr = 0x0006427A;
  uint8_t patch624_data[] = {
    0x8E, 
  };
  APPLY_PATCH(624);

  uint32_t patch625_addr = 0x0006427C;
  uint8_t patch625_data[] = {
    0x6A, 
  };
  APPLY_PATCH(625);

  uint32_t patch626_addr = 0x0006427E;
  uint8_t patch626_data[] = {
    0x28, 
  };
  APPLY_PATCH(626);

  uint32_t patch627_addr = 0x00064280;
  uint8_t patch627_data[] = {
    0x56, 
  };
  APPLY_PATCH(627);

  uint32_t patch628_addr = 0x00064282;
  uint8_t patch628_data[] = {
    0x36, 0x09, 0xDF, 0x33, 0x5D, 0x14, 0xF5, 0xC5, 0x92, 0x49, 0x5B, 0x29, 0x59, 0x9B, 0x71, 0xE9, 
    0x1D, 0xC7, 0x90, 0x67, 0xAF, 0x0B, 0x44, 0xE5, 0x63, 0x47, 0x67, 0xC4, 
  };
  APPLY_PATCH(628);

  uint32_t patch629_addr = 0x00065BE3;
  uint8_t patch629_data[] = {
    0xA1, 0x1F, 0x56, 0x3F, 0x24, 0x54, 0x98, 0x54, 0xCC, 0x7D, 0x95, 0x9D, 0x36, 0xB5, 0x80, 0xF3, 
    0x37, 0x1B, 0xC6, 0x9B, 0xE4, 0x30, 0xA2, 0x70, 0x75, 0xF9, 0xF1, 0x59, 0xCB, 0x3E, 0xF8, 0x7E, 
    0xE9, 0x7E, 0xFE, 0xBF, 0x94, 0xFF, 0x95, 0xDF, 0x94, 0xFC, 0x90, 0x1C, 0xF2, 0x3C, 0xD8, 0x5C, 
    0xAD, 0x7C, 0xAC, 0x7D, 0xAA, 0x7D, 0xA9, 0x7D, 0xA8, 0x7A, 0xA9, 
  };
  APPLY_PATCH(629);

  uint32_t patch630_addr = 0x00065C20;
  uint8_t patch630_data[] = {
    0x36, 0x9B, 0x77, 0x96, 0x97, 0xFD, 0xD5, 0x77, 0xF4, 0x31, 0x15, 0x3C, 0x7A, 0x5A, 0xB5, 0xEC, 
    0x1A, 0xBA, 0x3A, 0x88, 0x18, 0xA3, 0x7B, 0x92, 0xB8, 0x4A, 0x79, 0x9D, 0x99, 0xF8, 0xD8, 0x6B, 
    0x5D, 0x55, 0x3A, 0x26, 0x7A, 0x50, 0x7B, 0x6E, 0x9B, 0xE8, 0x9B, 0xE4, 0x98, 0xC7, 0x99, 0x6B, 
    0xA6, 0x95, 0xA7, 0x47, 0xA4, 0x6C, 0xA2, 0xCD, 0xA3, 0xC0, 0xC3, 0xC3, 0xC0, 0x1D, 
  };
  APPLY_PATCH(630);

  uint32_t patch631_addr = 0x00065C63;
  uint8_t patch631_data[] = {
    0x48, 0xF1, 0xDE, 
  };
  APPLY_PATCH(631);

  uint32_t patch632_addr = 0x00065C68;
  uint8_t patch632_data[] = {
    0x3F, 0x56, 0x1E, 0x1A, 0x5F, 0xED, 
  };
  APPLY_PATCH(632);

  uint32_t patch633_addr = 0x00065C70;
  uint8_t patch633_data[] = {
    0x9C, 0x33, 0x9D, 0x9F, 
  };
  APPLY_PATCH(633);

  uint32_t patch634_addr = 0x00065C76;
  uint8_t patch634_data[] = {
    0xDA, 0xFE, 0xFB, 0x7F, 0xF8, 0xBD, 0xDA, 0xED, 0x07, 0x49, 0xB9, 0xD4, 0xA6, 0x5B, 0xAC, 0x68, 
    0xCF, 0x7F, 
  };
  APPLY_PATCH(634);

  uint32_t patch635_addr = 0x00065C89;
  uint8_t patch635_data[] = {
    0xB1, 0xE2, 0x39, 
  };
  APPLY_PATCH(635);

  uint32_t patch636_addr = 0x00065C8D;
  uint8_t patch636_data[] = {
    0x3D, 0x0B, 0x83, 
  };
  APPLY_PATCH(636);

  uint32_t patch637_addr = 0x00065C92;
  uint8_t patch637_data[] = {
    0x40, 
  };
  APPLY_PATCH(637);

  uint32_t patch638_addr = 0x00065C9C;
  uint8_t patch638_data[] = {
    0xA7, 
  };
  APPLY_PATCH(638);

  uint32_t patch639_addr = 0x00065CA0;
  uint8_t patch639_data[] = {
    0x39, 0x1B, 0x59, 0x25, 0x56, 0xBA, 0x76, 0x84, 0x7F, 0x6F, 0x9F, 0x25, 0x97, 0x82, 0x94, 0x67, 
    0x9C, 0x87, 0x9D, 0x92, 0xB5, 0xDA, 0xAA, 0xC3, 0xC2, 0x7E, 0xC3, 0x37, 0xEB, 0x88, 0xE8, 0x54, 
    0x64, 0x7B, 0x65, 0xCD, 0x6D, 0xD2, 
  };
  APPLY_PATCH(639);

  uint32_t patch640_addr = 0x00065CC7;
  uint8_t patch640_data[] = {
    0xE3, 0x85, 0x0C, 
  };
  APPLY_PATCH(640);

  uint32_t patch641_addr = 0x00065CCE;
  uint8_t patch641_data[] = {
    0xAA, 0xFC, 0xA0, 
  };
  APPLY_PATCH(641);

  uint32_t patch642_addr = 0x00065CD2;
  uint8_t patch642_data[] = {
    0xC0, 
  };
  APPLY_PATCH(642);

  uint32_t patch643_addr = 0x00065CD4;
  uint8_t patch643_data[] = {
    0xC9, 
  };
  APPLY_PATCH(643);

  uint32_t patch644_addr = 0x00065CD6;
  uint8_t patch644_data[] = {
    0xE6, 0xB1, 0xEE, 
  };
  APPLY_PATCH(644);

  uint32_t patch645_addr = 0x00065CDA;
  uint8_t patch645_data[] = {
    0x0F, 
  };
  APPLY_PATCH(645);

  uint32_t patch646_addr = 0x000A709E;
  uint8_t patch646_data[] = {
    0xD8, 0x79, 0x6D, 
  };
  APPLY_PATCH(646);

  uint32_t patch647_addr = 0x000A70A2;
  uint8_t patch647_data[] = {
    0x6D, 0x98, 0x10, 0x4E, 
  };
  APPLY_PATCH(647);

  uint32_t patch648_addr = 0x000A70D6;
  uint8_t patch648_data[] = {
    0xE1, 
  };
  APPLY_PATCH(648);

  uint32_t patch649_addr = 0x000A70D9;
  uint8_t patch649_data[] = {
    0x93, 
  };
  APPLY_PATCH(649);

  uint32_t patch650_addr = 0x000A7432;
  uint8_t patch650_data[] = {
    0x55, 0x2C, 
  };
  APPLY_PATCH(650);

  uint32_t patch651_addr = 0x000A83EE;
  uint8_t patch651_data[] = {
    0xFE, 0x9B, 
  };
  APPLY_PATCH(651);

  uint32_t patch652_addr = 0x000A8416;
  uint8_t patch652_data[] = {
    0x92, 0x7C, 0x0B, 
  };
  APPLY_PATCH(652);

  uint32_t patch653_addr = 0x000A841A;
  uint8_t patch653_data[] = {
    0x3B, 0x38, 
  };
  APPLY_PATCH(653);

  uint32_t patch654_addr = 0x000AAADA;
  uint8_t patch654_data[] = {
    0xF2, 0x79, 0x43, 0x22, 0xA5, 0xD3, 
  };
  APPLY_PATCH(654);

  uint32_t patch655_addr = 0x000AD0E2;
  uint8_t patch655_data[] = {
    0x50, 0x5C, 
  };
  APPLY_PATCH(655);

  uint32_t patch656_addr = 0x000AD0E5;
  uint8_t patch656_data[] = {
    0x6F, 0x13, 0x00, 
  };
  APPLY_PATCH(656);

  uint32_t patch657_addr = 0x000B49BA;
  uint8_t patch657_data[] = {
    0x01, 0xE3, 0x25, 
  };
  APPLY_PATCH(657);

  uint32_t patch658_addr = 0x000B4CC6;
  uint8_t patch658_data[] = {
    0x0D, 0x7C, 0x29, 0xA9, 
  };
  APPLY_PATCH(658);

  uint32_t patch659_addr = 0x000B4CFA;
  uint8_t patch659_data[] = {
    0x05, 0x03, 0x21, 0x91, 
  };
  APPLY_PATCH(659);

  uint32_t patch660_addr = 0x000B4D2E;
  uint8_t patch660_data[] = {
    0x01, 0x97, 0x2D, 0x34, 
  };
  APPLY_PATCH(660);

  uint32_t patch661_addr = 0x000B4D62;
  uint8_t patch661_data[] = {
    0x05, 0x52, 0x29, 0x0E, 
  };
  APPLY_PATCH(661);

  uint32_t patch662_addr = 0x000B4D96;
  uint8_t patch662_data[] = {
    0x0D, 0x9A, 0x29, 0x9E, 
  };
  APPLY_PATCH(662);

  uint32_t patch663_addr = 0x000B4DCA;
  uint8_t patch663_data[] = {
    0x05, 0x5E, 0x21, 0xDB, 
  };
  APPLY_PATCH(663);

  uint32_t patch664_addr = 0x000B4DFE;
  uint8_t patch664_data[] = {
    0x0D, 0x40, 0x39, 0x49, 
  };
  APPLY_PATCH(664);

  uint32_t patch665_addr = 0x000B5632;
  uint8_t patch665_data[] = {
    0xD4, 0x73, 0x6D, 
  };
  APPLY_PATCH(665);

  uint32_t patch666_addr = 0x000B5636;
  uint8_t patch666_data[] = {
    0x4D, 0x24, 
  };
  APPLY_PATCH(666);

  uint32_t patch667_addr = 0x000B58DD;
  uint8_t patch667_data[] = {
    0x2E, 
  };
  APPLY_PATCH(667);

  uint32_t patch668_addr = 0x000B58E3;
  uint8_t patch668_data[] = {
    0x1E, 
  };
  APPLY_PATCH(668);

  uint32_t patch669_addr = 0x000B58EB;
  uint8_t patch669_data[] = {
    0xDE, 
  };
  APPLY_PATCH(669);

  uint32_t patch670_addr = 0x000B5D72;
  uint8_t patch670_data[] = {
    0xF9, 
  };
  APPLY_PATCH(670);

  uint32_t patch671_addr = 0x000B5D74;
  uint8_t patch671_data[] = {
    0x11, 0xDA, 
  };
  APPLY_PATCH(671);

  uint32_t patch672_addr = 0x000B5D78;
  uint8_t patch672_data[] = {
    0xF9, 
  };
  APPLY_PATCH(672);

  uint32_t patch673_addr = 0x000B5D7A;
  uint8_t patch673_data[] = {
    0x4F, 0xD9, 
  };
  APPLY_PATCH(673);

  uint32_t patch674_addr = 0x000B5E52;
  uint8_t patch674_data[] = {
    0xE9, 
  };
  APPLY_PATCH(674);

  uint32_t patch675_addr = 0x000B5E54;
  uint8_t patch675_data[] = {
    0xB1, 0xFB, 
  };
  APPLY_PATCH(675);

  uint32_t patch676_addr = 0x000B81B1;
  uint8_t patch676_data[] = {
    0x2B, 
  };
  APPLY_PATCH(676);

  uint32_t patch677_addr = 0x000B81D7;
  uint8_t patch677_data[] = {
    0xFA, 
  };
  APPLY_PATCH(677);

  uint32_t patch678_addr = 0x000B81FD;
  uint8_t patch678_data[] = {
    0xB3, 
  };
  APPLY_PATCH(678);

  uint32_t patch679_addr = 0x000B8223;
  uint8_t patch679_data[] = {
    0xB1, 
  };
  APPLY_PATCH(679);

  uint32_t patch680_addr = 0x000B8249;
  uint8_t patch680_data[] = {
    0xB0, 
  };
  APPLY_PATCH(680);

  uint32_t patch681_addr = 0x000B826F;
  uint8_t patch681_data[] = {
    0x8E, 
  };
  APPLY_PATCH(681);

  uint32_t patch682_addr = 0x000B8295;
  uint8_t patch682_data[] = {
    0x03, 
  };
  APPLY_PATCH(682);

  uint32_t patch683_addr = 0x000B82BB;
  uint8_t patch683_data[] = {
    0x7E, 
  };
  APPLY_PATCH(683);

  uint32_t patch684_addr = 0x000B82E1;
  uint8_t patch684_data[] = {
    0xBA, 
  };
  APPLY_PATCH(684);

  uint32_t patch685_addr = 0x000B8307;
  uint8_t patch685_data[] = {
    0x7E, 
  };
  APPLY_PATCH(685);

  uint32_t patch686_addr = 0x000B832D;
  uint8_t patch686_data[] = {
    0x0E, 
  };
  APPLY_PATCH(686);

  uint32_t patch687_addr = 0x000B8353;
  uint8_t patch687_data[] = {
    0x0D, 
  };
  APPLY_PATCH(687);

  uint32_t patch688_addr = 0x000B8379;
  uint8_t patch688_data[] = {
    0x15, 
  };
  APPLY_PATCH(688);

  uint32_t patch689_addr = 0x000B839F;
  uint8_t patch689_data[] = {
    0xBC, 
  };
  APPLY_PATCH(689);

  uint32_t patch690_addr = 0x000B83C5;
  uint8_t patch690_data[] = {
    0x27, 
  };
  APPLY_PATCH(690);

  uint32_t patch691_addr = 0x000B83EB;
  uint8_t patch691_data[] = {
    0xD3, 
  };
  APPLY_PATCH(691);

  uint32_t patch692_addr = 0x000B8411;
  uint8_t patch692_data[] = {
    0xBC, 
  };
  APPLY_PATCH(692);

  uint32_t patch693_addr = 0x000B8437;
  uint8_t patch693_data[] = {
    0xD7, 
  };
  APPLY_PATCH(693);

  uint32_t patch694_addr = 0x000B845D;
  uint8_t patch694_data[] = {
    0x65, 
  };
  APPLY_PATCH(694);

  uint32_t patch695_addr = 0x000B8483;
  uint8_t patch695_data[] = {
    0x1A, 
  };
  APPLY_PATCH(695);

  uint32_t patch696_addr = 0x000B84A9;
  uint8_t patch696_data[] = {
    0x21, 
  };
  APPLY_PATCH(696);

  uint32_t patch697_addr = 0x000B84CF;
  uint8_t patch697_data[] = {
    0xAD, 
  };
  APPLY_PATCH(697);

  uint32_t patch698_addr = 0x000B84F5;
  uint8_t patch698_data[] = {
    0x85, 
  };
  APPLY_PATCH(698);

  uint32_t patch699_addr = 0x000B851B;
  uint8_t patch699_data[] = {
    0xF7, 
  };
  APPLY_PATCH(699);

  uint32_t patch700_addr = 0x000B8541;
  uint8_t patch700_data[] = {
    0xC5, 
  };
  APPLY_PATCH(700);

  uint32_t patch701_addr = 0x000B8567;
  uint8_t patch701_data[] = {
    0x12, 
  };
  APPLY_PATCH(701);

  uint32_t patch702_addr = 0x000B858D;
  uint8_t patch702_data[] = {
    0x9E, 
  };
  APPLY_PATCH(702);

  uint32_t patch703_addr = 0x000B85B3;
  uint8_t patch703_data[] = {
    0xB2, 
  };
  APPLY_PATCH(703);

  uint32_t patch704_addr = 0x000B85D9;
  uint8_t patch704_data[] = {
    0x71, 
  };
  APPLY_PATCH(704);

  uint32_t patch705_addr = 0x000B85FF;
  uint8_t patch705_data[] = {
    0x86, 
  };
  APPLY_PATCH(705);

  uint32_t patch706_addr = 0x000B8625;
  uint8_t patch706_data[] = {
    0xD3, 
  };
  APPLY_PATCH(706);

  uint32_t patch707_addr = 0x000B864B;
  uint8_t patch707_data[] = {
    0xB0, 
  };
  APPLY_PATCH(707);

  uint32_t patch708_addr = 0x000B8671;
  uint8_t patch708_data[] = {
    0x05, 
  };
  APPLY_PATCH(708);

  uint32_t patch709_addr = 0x000B8697;
  uint8_t patch709_data[] = {
    0x6B, 
  };
  APPLY_PATCH(709);

  uint32_t patch710_addr = 0x000B86BD;
  uint8_t patch710_data[] = {
    0xD7, 
  };
  APPLY_PATCH(710);

  uint32_t patch711_addr = 0x000B86E3;
  uint8_t patch711_data[] = {
    0xE0, 
  };
  APPLY_PATCH(711);

  uint32_t patch712_addr = 0x000B8709;
  uint8_t patch712_data[] = {
    0x28, 
  };
  APPLY_PATCH(712);

  uint32_t patch713_addr = 0x000B872F;
  uint8_t patch713_data[] = {
    0x57, 
  };
  APPLY_PATCH(713);

  uint32_t patch714_addr = 0x000B8755;
  uint8_t patch714_data[] = {
    0xAD, 
  };
  APPLY_PATCH(714);

  uint32_t patch715_addr = 0x000B877B;
  uint8_t patch715_data[] = {
    0x2D, 
  };
  APPLY_PATCH(715);

  uint32_t patch716_addr = 0x000B87A1;
  uint8_t patch716_data[] = {
    0x8C, 
  };
  APPLY_PATCH(716);

  uint32_t patch717_addr = 0x000B87C7;
  uint8_t patch717_data[] = {
    0x42, 
  };
  APPLY_PATCH(717);

  uint32_t patch718_addr = 0x000B87ED;
  uint8_t patch718_data[] = {
    0x84, 
  };
  APPLY_PATCH(718);

  uint32_t patch719_addr = 0x000B8813;
  uint8_t patch719_data[] = {
    0xC3, 
  };
  APPLY_PATCH(719);

  uint32_t patch720_addr = 0x000B8839;
  uint8_t patch720_data[] = {
    0x10, 
  };
  APPLY_PATCH(720);

  uint32_t patch721_addr = 0x000B885F;
  uint8_t patch721_data[] = {
    0x2B, 
  };
  APPLY_PATCH(721);

  uint32_t patch722_addr = 0x000B8885;
  uint8_t patch722_data[] = {
    0x23, 
  };
  APPLY_PATCH(722);

  uint32_t patch723_addr = 0x000B88AB;
  uint8_t patch723_data[] = {
    0x6E, 
  };
  APPLY_PATCH(723);

  uint32_t patch724_addr = 0x000B88D1;
  uint8_t patch724_data[] = {
    0x64, 
  };
  APPLY_PATCH(724);

  uint32_t patch725_addr = 0x000B88F7;
  uint8_t patch725_data[] = {
    0x01, 
  };
  APPLY_PATCH(725);

  uint32_t patch726_addr = 0x000B891D;
  uint8_t patch726_data[] = {
    0xFF, 
  };
  APPLY_PATCH(726);

  uint32_t patch727_addr = 0x000B8943;
  uint8_t patch727_data[] = {
    0xC8, 
  };
  APPLY_PATCH(727);

  uint32_t patch728_addr = 0x000B8969;
  uint8_t patch728_data[] = {
    0xAA, 
  };
  APPLY_PATCH(728);

  uint32_t patch729_addr = 0x000B898F;
  uint8_t patch729_data[] = {
    0x57, 
  };
  APPLY_PATCH(729);

  uint32_t patch730_addr = 0x000B89B5;
  uint8_t patch730_data[] = {
    0x1E, 
  };
  APPLY_PATCH(730);

  uint32_t patch731_addr = 0x000B89DB;
  uint8_t patch731_data[] = {
    0x9C, 
  };
  APPLY_PATCH(731);

  uint32_t patch732_addr = 0x000B8A01;
  uint8_t patch732_data[] = {
    0xBB, 
  };
  APPLY_PATCH(732);

  uint32_t patch733_addr = 0x000B8A27;
  uint8_t patch733_data[] = {
    0x57, 
  };
  APPLY_PATCH(733);

  uint32_t patch734_addr = 0x000B8A4D;
  uint8_t patch734_data[] = {
    0xA9, 
  };
  APPLY_PATCH(734);

  uint32_t patch735_addr = 0x000B8A73;
  uint8_t patch735_data[] = {
    0xAF, 
  };
  APPLY_PATCH(735);

  uint32_t patch736_addr = 0x000B8A99;
  uint8_t patch736_data[] = {
    0x87, 
  };
  APPLY_PATCH(736);

  uint32_t patch737_addr = 0x000B8ABF;
  uint8_t patch737_data[] = {
    0x0B, 
  };
  APPLY_PATCH(737);

  uint32_t patch738_addr = 0x000B8AE5;
  uint8_t patch738_data[] = {
    0x9D, 
  };
  APPLY_PATCH(738);

  uint32_t patch739_addr = 0x000B8B0B;
  uint8_t patch739_data[] = {
    0x60, 
  };
  APPLY_PATCH(739);

  uint32_t patch740_addr = 0x000B8B31;
  uint8_t patch740_data[] = {
    0xC6, 
  };
  APPLY_PATCH(740);

  uint32_t patch741_addr = 0x000B8B57;
  uint8_t patch741_data[] = {
    0xAE, 
  };
  APPLY_PATCH(741);

  uint32_t patch742_addr = 0x000B8B7D;
  uint8_t patch742_data[] = {
    0x50, 
  };
  APPLY_PATCH(742);

  uint32_t patch743_addr = 0x000B8BA3;
  uint8_t patch743_data[] = {
    0xDE, 
  };
  APPLY_PATCH(743);

  uint32_t patch744_addr = 0x000B995B;
  uint8_t patch744_data[] = {
    0x53, 
  };
  APPLY_PATCH(744);

  uint32_t patch745_addr = 0x000B9981;
  uint8_t patch745_data[] = {
    0xBE, 
  };
  APPLY_PATCH(745);

  uint32_t patch746_addr = 0x000B99A7;
  uint8_t patch746_data[] = {
    0x98, 
  };
  APPLY_PATCH(746);

  uint32_t patch747_addr = 0x000B99CD;
  uint8_t patch747_data[] = {
    0x23, 
  };
  APPLY_PATCH(747);

  uint32_t patch748_addr = 0x000B99F3;
  uint8_t patch748_data[] = {
    0x1C, 
  };
  APPLY_PATCH(748);

  uint32_t patch749_addr = 0x000B9A19;
  uint8_t patch749_data[] = {
    0xC9, 
  };
  APPLY_PATCH(749);

  uint32_t patch750_addr = 0x000B9A3F;
  uint8_t patch750_data[] = {
    0x37, 
  };
  APPLY_PATCH(750);

  uint32_t patch751_addr = 0x000B9A65;
  uint8_t patch751_data[] = {
    0xFD, 
  };
  APPLY_PATCH(751);

  uint32_t patch752_addr = 0x000B9A8B;
  uint8_t patch752_data[] = {
    0x70, 
  };
  APPLY_PATCH(752);

  uint32_t patch753_addr = 0x000B9AB1;
  uint8_t patch753_data[] = {
    0xCA, 
  };
  APPLY_PATCH(753);

  uint32_t patch754_addr = 0x000B9AD7;
  uint8_t patch754_data[] = {
    0xE8, 
  };
  APPLY_PATCH(754);

  uint32_t patch755_addr = 0x000B9AFD;
  uint8_t patch755_data[] = {
    0x69, 
  };
  APPLY_PATCH(755);

  uint32_t patch756_addr = 0x000B9B23;
  uint8_t patch756_data[] = {
    0xF8, 
  };
  APPLY_PATCH(756);

  uint32_t patch757_addr = 0x000B9B49;
  uint8_t patch757_data[] = {
    0xD3, 
  };
  APPLY_PATCH(757);

  uint32_t patch758_addr = 0x000B9B6F;
  uint8_t patch758_data[] = {
    0x73, 
  };
  APPLY_PATCH(758);

  uint32_t patch759_addr = 0x000B9B95;
  uint8_t patch759_data[] = {
    0x96, 
  };
  APPLY_PATCH(759);

  uint32_t patch760_addr = 0x000B9BBB;
  uint8_t patch760_data[] = {
    0x29, 
  };
  APPLY_PATCH(760);

  uint32_t patch761_addr = 0x000B9BE1;
  uint8_t patch761_data[] = {
    0xA1, 
  };
  APPLY_PATCH(761);

  uint32_t patch762_addr = 0x000B9C07;
  uint8_t patch762_data[] = {
    0xC4, 
  };
  APPLY_PATCH(762);

  uint32_t patch763_addr = 0x000B9C2D;
  uint8_t patch763_data[] = {
    0x6C, 
  };
  APPLY_PATCH(763);

  uint32_t patch764_addr = 0x000B9C53;
  uint8_t patch764_data[] = {
    0x2B, 
  };
  APPLY_PATCH(764);

  uint32_t patch765_addr = 0x000B9C79;
  uint8_t patch765_data[] = {
    0x31, 
  };
  APPLY_PATCH(765);

  uint32_t patch766_addr = 0x000B9C9F;
  uint8_t patch766_data[] = {
    0xC3, 
  };
  APPLY_PATCH(766);

  uint32_t patch767_addr = 0x000B9CC5;
  uint8_t patch767_data[] = {
    0x8F, 
  };
  APPLY_PATCH(767);

  uint32_t patch768_addr = 0x000B9CEB;
  uint8_t patch768_data[] = {
    0x26, 
  };
  APPLY_PATCH(768);

  uint32_t patch769_addr = 0x000B9D11;
  uint8_t patch769_data[] = {
    0x20, 
  };
  APPLY_PATCH(769);

  uint32_t patch770_addr = 0x000B9D37;
  uint8_t patch770_data[] = {
    0x06, 
  };
  APPLY_PATCH(770);

  uint32_t patch771_addr = 0x000B9D5D;
  uint8_t patch771_data[] = {
    0x2F, 
  };
  APPLY_PATCH(771);

  uint32_t patch772_addr = 0x000B9D83;
  uint8_t patch772_data[] = {
    0x65, 
  };
  APPLY_PATCH(772);

  uint32_t patch773_addr = 0x000B9DA9;
  uint8_t patch773_data[] = {
    0xE3, 
  };
  APPLY_PATCH(773);

  uint32_t patch774_addr = 0x000B9DCF;
  uint8_t patch774_data[] = {
    0x4A, 
  };
  APPLY_PATCH(774);

  uint32_t patch775_addr = 0x000B9DF5;
  uint8_t patch775_data[] = {
    0xF8, 
  };
  APPLY_PATCH(775);

  uint32_t patch776_addr = 0x000B9E1B;
  uint8_t patch776_data[] = {
    0xC4, 
  };
  APPLY_PATCH(776);

  uint32_t patch777_addr = 0x000B9E41;
  uint8_t patch777_data[] = {
    0x05, 
  };
  APPLY_PATCH(777);

  uint32_t patch778_addr = 0x000B9E67;
  uint8_t patch778_data[] = {
    0x88, 
  };
  APPLY_PATCH(778);

  uint32_t patch779_addr = 0x000B9E8D;
  uint8_t patch779_data[] = {
    0x62, 
  };
  APPLY_PATCH(779);

  uint32_t patch780_addr = 0x000B9EB3;
  uint8_t patch780_data[] = {
    0xD5, 
  };
  APPLY_PATCH(780);

  uint32_t patch781_addr = 0x000B9ED9;
  uint8_t patch781_data[] = {
    0x60, 
  };
  APPLY_PATCH(781);

  uint32_t patch782_addr = 0x000B9EFF;
  uint8_t patch782_data[] = {
    0x6A, 
  };
  APPLY_PATCH(782);

  uint32_t patch783_addr = 0x000B9F25;
  uint8_t patch783_data[] = {
    0x5E, 
  };
  APPLY_PATCH(783);

  uint32_t patch784_addr = 0x000B9F4B;
  uint8_t patch784_data[] = {
    0xB5, 
  };
  APPLY_PATCH(784);

  uint32_t patch785_addr = 0x000B9F71;
  uint8_t patch785_data[] = {
    0xD6, 
  };
  APPLY_PATCH(785);

  uint32_t patch786_addr = 0x000B9F97;
  uint8_t patch786_data[] = {
    0x5A, 
  };
  APPLY_PATCH(786);

  uint32_t patch787_addr = 0x000B9FBD;
  uint8_t patch787_data[] = {
    0x05, 
  };
  APPLY_PATCH(787);

  uint32_t patch788_addr = 0x000B9FE3;
  uint8_t patch788_data[] = {
    0xA8, 
  };
  APPLY_PATCH(788);

  uint32_t patch789_addr = 0x000BA009;
  uint8_t patch789_data[] = {
    0xC2, 
  };
  APPLY_PATCH(789);

  uint32_t patch790_addr = 0x000BA02F;
  uint8_t patch790_data[] = {
    0xDF, 
  };
  APPLY_PATCH(790);

  uint32_t patch791_addr = 0x000BA055;
  uint8_t patch791_data[] = {
    0x0A, 
  };
  APPLY_PATCH(791);

  uint32_t patch792_addr = 0x000BA07B;
  uint8_t patch792_data[] = {
    0xF1, 
  };
  APPLY_PATCH(792);

  uint32_t patch793_addr = 0x000BA0A1;
  uint8_t patch793_data[] = {
    0x53, 
  };
  APPLY_PATCH(793);

  uint32_t patch794_addr = 0x000BA0C7;
  uint8_t patch794_data[] = {
    0xBC, 
  };
  APPLY_PATCH(794);

  uint32_t patch795_addr = 0x000BA0ED;
  uint8_t patch795_data[] = {
    0x75, 
  };
  APPLY_PATCH(795);

  uint32_t patch796_addr = 0x000BA113;
  uint8_t patch796_data[] = {
    0x7B, 
  };
  APPLY_PATCH(796);

  uint32_t patch797_addr = 0x000BA139;
  uint8_t patch797_data[] = {
    0x3D, 
  };
  APPLY_PATCH(797);

  uint32_t patch798_addr = 0x000BA15F;
  uint8_t patch798_data[] = {
    0x53, 
  };
  APPLY_PATCH(798);

  uint32_t patch799_addr = 0x000BA185;
  uint8_t patch799_data[] = {
    0x53, 
  };
  APPLY_PATCH(799);

  uint32_t patch800_addr = 0x000BA1AB;
  uint8_t patch800_data[] = {
    0x6C, 
  };
  APPLY_PATCH(800);

  uint32_t patch801_addr = 0x000BA1D1;
  uint8_t patch801_data[] = {
    0x61, 
  };
  APPLY_PATCH(801);

  uint32_t patch802_addr = 0x000BA1F7;
  uint8_t patch802_data[] = {
    0xDC, 
  };
  APPLY_PATCH(802);

  uint32_t patch803_addr = 0x000BA21D;
  uint8_t patch803_data[] = {
    0xC3, 
  };
  APPLY_PATCH(803);

  uint32_t patch804_addr = 0x000BA243;
  uint8_t patch804_data[] = {
    0x70, 
  };
  APPLY_PATCH(804);

  uint32_t patch805_addr = 0x000BA269;
  uint8_t patch805_data[] = {
    0x6A, 
  };
  APPLY_PATCH(805);

  uint32_t patch806_addr = 0x000BA28F;
  uint8_t patch806_data[] = {
    0xDC, 
  };
  APPLY_PATCH(806);

  uint32_t patch807_addr = 0x000BA2B5;
  uint8_t patch807_data[] = {
    0x9D, 
  };
  APPLY_PATCH(807);

  uint32_t patch808_addr = 0x000BA2DB;
  uint8_t patch808_data[] = {
    0xC5, 
  };
  APPLY_PATCH(808);

  uint32_t patch809_addr = 0x000BA301;
  uint8_t patch809_data[] = {
    0x9A, 
  };
  APPLY_PATCH(809);

  uint32_t patch810_addr = 0x000BA327;
  uint8_t patch810_data[] = {
    0xC3, 
  };
  APPLY_PATCH(810);

  uint32_t patch811_addr = 0x000BA34D;
  uint8_t patch811_data[] = {
    0x44, 
  };
  APPLY_PATCH(811);

  uint32_t patch812_addr = 0x000BAA68;
  uint8_t patch812_data[] = {
    0x59, 0xE8, 0x38, 0x9E, 
  };
  APPLY_PATCH(812);

  uint32_t patch813_addr = 0x000BAB18;
  uint8_t patch813_data[] = {
    0x5B, 0x4D, 0x3A, 0x4D, 
  };
  APPLY_PATCH(813);

  uint32_t patch814_addr = 0x000BAF74;
  uint8_t patch814_data[] = {
    0x1A, 0x96, 0x1A, 0xE2, 
  };
  APPLY_PATCH(814);

  uint32_t patch815_addr = 0x000BAF8A;
  uint8_t patch815_data[] = {
    0xF0, 
  };
  APPLY_PATCH(815);

  uint32_t patch816_addr = 0x000BAF8C;
  uint8_t patch816_data[] = {
    0x41, 
  };
  APPLY_PATCH(816);

  uint32_t patch817_addr = 0x000BAF8E;
  uint8_t patch817_data[] = {
    0x40, 
  };
  APPLY_PATCH(817);

  uint32_t patch818_addr = 0x000BAF90;
  uint8_t patch818_data[] = {
    0xEA, 0xBA, 0x79, 0xFA, 0x65, 0x01, 0xDA, 
  };
  APPLY_PATCH(818);

  uint32_t patch819_addr = 0x000BAF9C;
  uint8_t patch819_data[] = {
    0x32, 0x2C, 0x32, 0x0A, 
  };
  APPLY_PATCH(819);

  uint32_t patch820_addr = 0x000BAFA6;
  uint8_t patch820_data[] = {
    0x75, 
  };
  APPLY_PATCH(820);

  uint32_t patch821_addr = 0x000BAFB0;
  uint8_t patch821_data[] = {
    0x7F, 
  };
  APPLY_PATCH(821);

  uint32_t patch822_addr = 0x000BB0D6;
  uint8_t patch822_data[] = {
    0x4A, 0x5E, 0xDB, 
  };
  APPLY_PATCH(822);

  uint32_t patch823_addr = 0x000BB0DA;
  uint8_t patch823_data[] = {
    0x1B, 0x33, 0xA2, 0x6C, 0xA2, 0x9C, 0x88, 0x0C, 0x88, 0x5C, 0x80, 0x21, 
  };
  APPLY_PATCH(823);

  uint32_t patch824_addr = 0x000BB176;
  uint8_t patch824_data[] = {
    0x02, 0xE2, 0xDB, 
  };
  APPLY_PATCH(824);

  uint32_t patch825_addr = 0x000BB17A;
  uint8_t patch825_data[] = {
    0x63, 0xD7, 
  };
  APPLY_PATCH(825);

  uint32_t patch826_addr = 0x000BB3A0;
  uint8_t patch826_data[] = {
    0x6D, 
  };
  APPLY_PATCH(826);

  uint32_t patch827_addr = 0x000BB3A2;
  uint8_t patch827_data[] = {
    0xED, 0x2A, 
  };
  APPLY_PATCH(827);

  uint32_t patch828_addr = 0x000BB3BE;
  uint8_t patch828_data[] = {
    0x47, 
  };
  APPLY_PATCH(828);

  uint32_t patch829_addr = 0x000BB3C0;
  uint8_t patch829_data[] = {
    0x69, 0xC4, 
  };
  APPLY_PATCH(829);

  uint32_t patch830_addr = 0x000BB3FC;
  uint8_t patch830_data[] = {
    0xC3, 
  };
  APPLY_PATCH(830);

  uint32_t patch831_addr = 0x000BB3FE;
  uint8_t patch831_data[] = {
    0xC3, 
  };
  APPLY_PATCH(831);

  uint32_t patch832_addr = 0x000BB400;
  uint8_t patch832_data[] = {
    0x79, 
  };
  APPLY_PATCH(832);

  uint32_t patch833_addr = 0x000BB402;
  uint8_t patch833_data[] = {
    0x79, 
  };
  APPLY_PATCH(833);

  uint32_t patch834_addr = 0x000BB404;
  uint8_t patch834_data[] = {
    0x79, 
  };
  APPLY_PATCH(834);

  uint32_t patch835_addr = 0x000BB406;
  uint8_t patch835_data[] = {
    0x79, 
  };
  APPLY_PATCH(835);

  uint32_t patch836_addr = 0x000BB408;
  uint8_t patch836_data[] = {
    0x59, 
  };
  APPLY_PATCH(836);

  uint32_t patch837_addr = 0x000BB40A;
  uint8_t patch837_data[] = {
    0x59, 
  };
  APPLY_PATCH(837);

  uint32_t patch838_addr = 0x000BB40C;
  uint8_t patch838_data[] = {
    0x59, 
  };
  APPLY_PATCH(838);

  uint32_t patch839_addr = 0x000BB40E;
  uint8_t patch839_data[] = {
    0x59, 
  };
  APPLY_PATCH(839);

  uint32_t patch840_addr = 0x000BB410;
  uint8_t patch840_data[] = {
    0x7B, 
  };
  APPLY_PATCH(840);

  uint32_t patch841_addr = 0x000BB412;
  uint8_t patch841_data[] = {
    0x7B, 
  };
  APPLY_PATCH(841);

  uint32_t patch842_addr = 0x000BB414;
  uint8_t patch842_data[] = {
    0x7B, 
  };
  APPLY_PATCH(842);

  uint32_t patch843_addr = 0x000BB416;
  uint8_t patch843_data[] = {
    0x7B, 
  };
  APPLY_PATCH(843);

  uint32_t patch844_addr = 0x000BB418;
  uint8_t patch844_data[] = {
    0x5B, 
  };
  APPLY_PATCH(844);

  uint32_t patch845_addr = 0x000BB41A;
  uint8_t patch845_data[] = {
    0x5B, 
  };
  APPLY_PATCH(845);

  uint32_t patch846_addr = 0x000BB422;
  uint8_t patch846_data[] = {
    0x7D, 
  };
  APPLY_PATCH(846);

  uint32_t patch847_addr = 0x000BB424;
  uint8_t patch847_data[] = {
    0x7D, 
  };
  APPLY_PATCH(847);

  uint32_t patch848_addr = 0x000BB426;
  uint8_t patch848_data[] = {
    0x7D, 
  };
  APPLY_PATCH(848);

  uint32_t patch849_addr = 0x000BB428;
  uint8_t patch849_data[] = {
    0x5D, 
  };
  APPLY_PATCH(849);

  uint32_t patch850_addr = 0x000BB42A;
  uint8_t patch850_data[] = {
    0x5D, 
  };
  APPLY_PATCH(850);

  uint32_t patch851_addr = 0x000BB42C;
  uint8_t patch851_data[] = {
    0x5D, 
  };
  APPLY_PATCH(851);

  uint32_t patch852_addr = 0x000BB42E;
  uint8_t patch852_data[] = {
    0x5D, 
  };
  APPLY_PATCH(852);

  uint32_t patch853_addr = 0x000BB430;
  uint8_t patch853_data[] = {
    0x7F, 
  };
  APPLY_PATCH(853);

  uint32_t patch854_addr = 0x000BB432;
  uint8_t patch854_data[] = {
    0x7F, 
  };
  APPLY_PATCH(854);

  uint32_t patch855_addr = 0x000BB434;
  uint8_t patch855_data[] = {
    0x7F, 
  };
  APPLY_PATCH(855);

  uint32_t patch856_addr = 0x000BB436;
  uint8_t patch856_data[] = {
    0x7F, 
  };
  APPLY_PATCH(856);

  uint32_t patch857_addr = 0x000BB438;
  uint8_t patch857_data[] = {
    0x5F, 
  };
  APPLY_PATCH(857);

  uint32_t patch858_addr = 0x000BB43A;
  uint8_t patch858_data[] = {
    0x5F, 
  };
  APPLY_PATCH(858);

  uint32_t patch859_addr = 0x000BB43C;
  uint8_t patch859_data[] = {
    0x5F, 
  };
  APPLY_PATCH(859);

  uint32_t patch860_addr = 0x000BB43E;
  uint8_t patch860_data[] = {
    0x5F, 
  };
  APPLY_PATCH(860);

  uint32_t patch861_addr = 0x000BB440;
  uint8_t patch861_data[] = {
    0xF9, 
  };
  APPLY_PATCH(861);

  uint32_t patch862_addr = 0x000BB48E;
  uint8_t patch862_data[] = {
    0x50, 0x5C, 0x0A, 0x12, 
  };
  APPLY_PATCH(862);

  uint32_t patch863_addr = 0x000BB508;
  uint8_t patch863_data[] = {
    0xA0, 0x94, 0x52, 
  };
  APPLY_PATCH(863);

  uint32_t patch864_addr = 0x000BB50C;
  uint8_t patch864_data[] = {
    0x01, 0x50, 
  };
  APPLY_PATCH(864);

  uint32_t patch865_addr = 0x000BB5A6;
  uint8_t patch865_data[] = {
    0x8C, 0x2E, 0x56, 
  };
  APPLY_PATCH(865);

  uint32_t patch866_addr = 0x000BB5AA;
  uint8_t patch866_data[] = {
    0xE5, 0x2B, 
  };
  APPLY_PATCH(866);

  uint32_t patch867_addr = 0x000BB6FA;
  uint8_t patch867_data[] = {
    0xC5, 0x1E, 0xC2, 0xE1, 0xAF, 0x94, 0x6B, 0x68, 0x49, 
  };
  APPLY_PATCH(867);

  uint32_t patch868_addr = 0x000BB704;
  uint8_t patch868_data[] = {
    0x6B, 0x24, 0x6A, 0xF6, 0x67, 0xCA, 0x5B, 
  };
  APPLY_PATCH(868);

  uint32_t patch869_addr = 0x000BB710;
  uint8_t patch869_data[] = {
    0x61, 
  };
  APPLY_PATCH(869);

  uint32_t patch870_addr = 0x000BB712;
  uint8_t patch870_data[] = {
    0x6F, 0xED, 0x45, 0x86, 0x71, 0xE7, 0x65, 0x65, 0x51, 
  };
  APPLY_PATCH(870);

  uint32_t patch871_addr = 0x000BB71C;
  uint8_t patch871_data[] = {
    0x65, 0x1D, 0x58, 
  };
  APPLY_PATCH(871);

  uint32_t patch872_addr = 0x000BB720;
  uint8_t patch872_data[] = {
    0x43, 0xBC, 0x76, 0x00, 0x67, 0xCB, 0x69, 0x55, 0x63, 0xCC, 0x56, 0xE9, 
  };
  APPLY_PATCH(872);

  uint32_t patch873_addr = 0x000BB72E;
  uint8_t patch873_data[] = {
    0x56, 
  };
  APPLY_PATCH(873);

  uint32_t patch874_addr = 0x000BB730;
  uint8_t patch874_data[] = {
    0x81, 0x79, 0x2F, 
  };
  APPLY_PATCH(874);

  uint32_t patch875_addr = 0x000BB734;
  uint8_t patch875_data[] = {
    0xA7, 0x26, 0xBD, 0x0D, 0x10, 0xE3, 0x3A, 0x32, 
  };
  APPLY_PATCH(875);

  uint32_t patch876_addr = 0x000BC1E0;
  uint8_t patch876_data[] = {
    0x79, 
  };
  APPLY_PATCH(876);

  uint32_t patch877_addr = 0x000BC1EC;
  uint8_t patch877_data[] = {
    0x40, 0x38, 0x40, 0xA7, 0x4A, 0x15, 0x4A, 0xAA, 0x42, 0x42, 0x42, 0x60, 0x4A, 0x2E, 0x4A, 0x85, 
    0x42, 0x05, 0x42, 0x3C, 0x58, 0x07, 0x58, 0x09, 0x58, 0x1D, 0x90, 0x7E, 
  };
  APPLY_PATCH(877);

  uint32_t patch878_addr = 0x000BC20A;
  uint8_t patch878_data[] = {
    0xB9, 0x38, 
  };
  APPLY_PATCH(878);

  uint32_t patch879_addr = 0x000BC232;
  uint8_t patch879_data[] = {
    0x9F, 0x5D, 
  };
  APPLY_PATCH(879);

  uint32_t patch880_addr = 0x000BC3A0;
  uint8_t patch880_data[] = {
    0x6D, 
  };
  APPLY_PATCH(880);

  uint32_t patch881_addr = 0x000BC3AC;
  uint8_t patch881_data[] = {
    0x54, 0xD8, 0x54, 0x2A, 0x5E, 0x9D, 0x5E, 0x08, 0x56, 0xA2, 0x56, 0x16, 0x5E, 0xDB, 0x5E, 0x9F, 
    0x56, 0xB9, 0x56, 0x70, 0x58, 0x8C, 0x58, 0x81, 0x50, 0x91, 0x98, 0xFE, 
  };
  APPLY_PATCH(881);

  uint32_t patch882_addr = 0x000BC3CA;
  uint8_t patch882_data[] = {
    0xB9, 0x64, 
  };
  APPLY_PATCH(882);

  uint32_t patch883_addr = 0x000BC3F2;
  uint8_t patch883_data[] = {
    0x9B, 0x84, 
  };
  APPLY_PATCH(883);

  uint32_t patch884_addr = 0x000BC660;
  uint8_t patch884_data[] = {
    0x3B, 0x0B, 
  };
  APPLY_PATCH(884);

  uint32_t patch885_addr = 0x000BC66E;
  uint8_t patch885_data[] = {
    0x3D, 0x68, 
  };
  APPLY_PATCH(885);

  uint32_t patch886_addr = 0x000BC682;
  uint8_t patch886_data[] = {
    0x21, 
  };
  APPLY_PATCH(886);

  uint32_t patch887_addr = 0x000BC684;
  uint8_t patch887_data[] = {
    0x29, 
  };
  APPLY_PATCH(887);

  uint32_t patch888_addr = 0x000BC686;
  uint8_t patch888_data[] = {
    0x28, 
  };
  APPLY_PATCH(888);

  uint32_t patch889_addr = 0x000BC688;
  uint8_t patch889_data[] = {
    0x20, 
  };
  APPLY_PATCH(889);

  uint32_t patch890_addr = 0x000BC68A;
  uint8_t patch890_data[] = {
    0x23, 
  };
  APPLY_PATCH(890);

  uint32_t patch891_addr = 0x000BC68C;
  uint8_t patch891_data[] = {
    0x29, 
  };
  APPLY_PATCH(891);

  uint32_t patch892_addr = 0x000BC68E;
  uint8_t patch892_data[] = {
    0x28, 
  };
  APPLY_PATCH(892);

  uint32_t patch893_addr = 0x000BC690;
  uint8_t patch893_data[] = {
    0x22, 
  };
  APPLY_PATCH(893);

  uint32_t patch894_addr = 0x000BC692;
  uint8_t patch894_data[] = {
    0x21, 
  };
  APPLY_PATCH(894);

  uint32_t patch895_addr = 0x000BC694;
  uint8_t patch895_data[] = {
    0x29, 
  };
  APPLY_PATCH(895);

  uint32_t patch896_addr = 0x000BC696;
  uint8_t patch896_data[] = {
    0x2B, 
  };
  APPLY_PATCH(896);

  uint32_t patch897_addr = 0x000BC698;
  uint8_t patch897_data[] = {
    0x23, 
  };
  APPLY_PATCH(897);

  uint32_t patch898_addr = 0x000BC69A;
  uint8_t patch898_data[] = {
    0x22, 
  };
  APPLY_PATCH(898);

  uint32_t patch899_addr = 0x000BC69C;
  uint8_t patch899_data[] = {
    0x2A, 
  };
  APPLY_PATCH(899);

  uint32_t patch900_addr = 0x000BC69E;
  uint8_t patch900_data[] = {
    0x29, 
  };
  APPLY_PATCH(900);

  uint32_t patch901_addr = 0x000BC6A0;
  uint8_t patch901_data[] = {
    0x25, 
  };
  APPLY_PATCH(901);

  uint32_t patch902_addr = 0x000BC6A2;
  uint8_t patch902_data[] = {
    0x24, 
  };
  APPLY_PATCH(902);

  uint32_t patch903_addr = 0x000BC6A4;
  uint8_t patch903_data[] = {
    0x2C, 
  };
  APPLY_PATCH(903);

  uint32_t patch904_addr = 0x000BC6A6;
  uint8_t patch904_data[] = {
    0x2F, 
  };
  APPLY_PATCH(904);

  uint32_t patch905_addr = 0x000BC6A8;
  uint8_t patch905_data[] = {
    0x27, 
  };
  APPLY_PATCH(905);

  uint32_t patch906_addr = 0x000BCDDA;
  uint8_t patch906_data[] = {
    0x1B, 0x8E, 
  };
  APPLY_PATCH(906);

  uint32_t patch907_addr = 0x000BCDE8;
  uint8_t patch907_data[] = {
    0x39, 0x73, 0x39, 0x2B, 
  };
  APPLY_PATCH(907);

  uint32_t patch908_addr = 0x000BCDFA;
  uint8_t patch908_data[] = {
    0x1B, 0x2B, 0x73, 0x8B, 
  };
  APPLY_PATCH(908);

  uint32_t patch909_addr = 0x000BCE08;
  uint8_t patch909_data[] = {
    0x29, 0x30, 0x29, 0x30, 0x21, 0xCC, 0x39, 0xA2, 
  };
  APPLY_PATCH(909);

  uint32_t patch910_addr = 0x000BD9D7;
  uint8_t patch910_data[] = {
    0x21, 0xFB, 
  };
  APPLY_PATCH(910);

  uint32_t patch911_addr = 0x000BD9DA;
  uint8_t patch911_data[] = {
    0xFA, 
  };
  APPLY_PATCH(911);

  uint32_t patch912_addr = 0x000BD9DC;
  uint8_t patch912_data[] = {
    0x72, 0x7C, 0xE1, 0x10, 0x8C, 0x1B, 
  };
  APPLY_PATCH(912);

  uint32_t patch913_addr = 0x000BE580;
  uint8_t patch913_data[] = {
    0xC0, 0x08, 0x79, 
  };
  APPLY_PATCH(913);

  uint32_t patch914_addr = 0x000BE584;
  uint8_t patch914_data[] = {
    0xB7, 0xA3, 
  };
  APPLY_PATCH(914);

  uint32_t patch915_addr = 0x000BFC72;
  uint8_t patch915_data[] = {
    0x8A, 0x13, 0x8A, 0x59, 0x8A, 0x0A, 0x8A, 0x8B, 0x8A, 0x4D, 0x8A, 0x16, 0x8A, 0xBA, 0x08, 
  };
  APPLY_PATCH(915);

  uint32_t patch916_addr = 0x000BFC82;
  uint8_t patch916_data[] = {
    0x08, 0xA0, 0x00, 0x9F, 0x00, 0x80, 0x08, 0x4E, 0x08, 0xC6, 0x00, 0xDE, 0x00, 0x60, 0x0A, 0x12, 
    0x0A, 0x38, 0x02, 0xB5, 
  };
  APPLY_PATCH(916);

  uint32_t patch917_addr = 0x000BFCA2;
  uint8_t patch917_data[] = {
    0x0C, 0xD1, 0x04, 0x4E, 0x04, 0x7C, 
  };
  APPLY_PATCH(917);

  uint32_t patch918_addr = 0x000BFCBC;
  uint8_t patch918_data[] = {
    0x06, 0x6B, 0x06, 0xB7, 0x88, 0x98, 0x88, 0x2E, 
  };
  APPLY_PATCH(918);

  uint32_t patch919_addr = 0x000C0C00;
  uint8_t patch919_data[] = {
    0x5D, 0x69, 
  };
  APPLY_PATCH(919);

  uint32_t patch920_addr = 0x000C0C08;
  uint8_t patch920_data[] = {
    0x19, 0x00, 0x1A, 0x64, 
  };
  APPLY_PATCH(920);

  uint32_t patch921_addr = 0x000C0C20;
  uint8_t patch921_data[] = {
    0x5D, 0x4D, 
  };
  APPLY_PATCH(921);

  uint32_t patch922_addr = 0x000C0C28;
  uint8_t patch922_data[] = {
    0x1D, 0xCF, 0x1E, 0xA2, 
  };
  APPLY_PATCH(922);

  uint32_t patch923_addr = 0x000C0C40;
  uint8_t patch923_data[] = {
    0x5C, 0xD1, 
  };
  APPLY_PATCH(923);

  uint32_t patch924_addr = 0x000C0C48;
  uint8_t patch924_data[] = {
    0x19, 0x78, 0x1A, 0x10, 
  };
  APPLY_PATCH(924);

  uint32_t patch925_addr = 0x000C0C60;
  uint8_t patch925_data[] = {
    0x5B, 0x23, 
  };
  APPLY_PATCH(925);

  uint32_t patch926_addr = 0x000C0C68;
  uint8_t patch926_data[] = {
    0x19, 0xA8, 0x1A, 0x84, 
  };
  APPLY_PATCH(926);

  uint32_t patch927_addr = 0x000C0C80;
  uint8_t patch927_data[] = {
    0x59, 0x53, 
  };
  APPLY_PATCH(927);

  uint32_t patch928_addr = 0x000C0CA0;
  uint8_t patch928_data[] = {
    0x5D, 0x97, 
  };
  APPLY_PATCH(928);

  uint32_t patch929_addr = 0x000C1742;
  uint8_t patch929_data[] = {
    0xD0, 0x0A, 0x68, 0x8B, 0xBB, 0x8A, 0x81, 0xC7, 
  };
  APPLY_PATCH(929);

  uint32_t patch930_addr = 0x000C4028;
  uint8_t patch930_data[] = {
    0x3A, 
  };
  APPLY_PATCH(930);

  uint32_t patch931_addr = 0x000C402A;
  uint8_t patch931_data[] = {
    0x4D, 0xB3, 0x24, 0xCF, 0x21, 0x61, 0x2A, 0x2F, 0x3B, 0x74, 0x4D, 0x57, 0x22, 0x7A, 0x4D, 0xB7, 
    0x4D, 0x71, 0x3F, 0x16, 0x28, 0x8A, 0x26, 0xB5, 
  };
  APPLY_PATCH(931);

  uint32_t patch932_addr = 0x000C4043;
  uint8_t patch932_data[] = {
    0xCF, 
  };
  APPLY_PATCH(932);

  uint32_t patch933_addr = 0x000C404C;
  uint8_t patch933_data[] = {
    0x20, 0x0C, 0x21, 0xA7, 0x3C, 0x42, 0x49, 0x8B, 0x2B, 0xB7, 0x2C, 0x17, 0x3D, 0x13, 0x26, 0x1A, 
    0x3A, 0xDF, 
  };
  APPLY_PATCH(933);

  uint32_t patch934_addr = 0x000C405F;
  uint8_t patch934_data[] = {
    0xF8, 
  };
  APPLY_PATCH(934);

  uint32_t patch935_addr = 0x000C406A;
  uint8_t patch935_data[] = {
    0x3D, 0x99, 0x20, 0x0F, 
  };
  APPLY_PATCH(935);

  uint32_t patch936_addr = 0x000C406F;
  uint8_t patch936_data[] = {
    0x25, 0x3C, 0xBE, 0x49, 0x0F, 0x28, 
  };
  APPLY_PATCH(936);

  uint32_t patch937_addr = 0x000C4076;
  uint8_t patch937_data[] = {
    0x3B, 0x07, 0x2D, 0x80, 0x24, 0x23, 0x3A, 0x11, 0x20, 0xA7, 0x27, 0x2E, 
  };
  APPLY_PATCH(937);

  uint32_t patch938_addr = 0x000C408A;
  uint8_t patch938_data[] = {
    0x3D, 0xA8, 0x41, 0xDF, 0x20, 0x63, 0x2C, 0x17, 0x3E, 0x56, 0x33, 
  };
  APPLY_PATCH(938);

  uint32_t patch939_addr = 0x000C4096;
  uint8_t patch939_data[] = {
    0x25, 0xD3, 0x2B, 0x55, 0x3D, 
  };
  APPLY_PATCH(939);

  uint32_t patch940_addr = 0x000C409C;
  uint8_t patch940_data[] = {
    0x24, 
  };
  APPLY_PATCH(940);

  uint32_t patch941_addr = 0x000C409E;
  uint8_t patch941_data[] = {
    0x4F, 0x65, 
  };
  APPLY_PATCH(941);

  uint32_t patch942_addr = 0x000C40AA;
  uint8_t patch942_data[] = {
    0x39, 0xBC, 0x2A, 0x7F, 0x36, 0x87, 0x23, 0x73, 0x3E, 0x63, 0x2A, 0x4F, 0x45, 0x30, 0x3E, 0x86, 
    0x3F, 0xA6, 0x24, 0x60, 0x3C, 0xBD, 0x49, 0xF6, 
  };
  APPLY_PATCH(942);

  uint32_t patch943_addr = 0x000C40CA;
  uint8_t patch943_data[] = {
    0x2F, 0x5A, 0x33, 0x6F, 0x27, 0x79, 0x3D, 0xC3, 0x3B, 0xE4, 0x41, 0x38, 0x33, 0x53, 0x3A, 0xC7, 
    0x2D, 0x76, 0x32, 0x67, 0x4F, 0xFC, 
  };
  APPLY_PATCH(943);

  uint32_t patch944_addr = 0x000C40E1;
  uint8_t patch944_data[] = {
    0x6C, 
  };
  APPLY_PATCH(944);

  uint32_t patch945_addr = 0x000C40EA;
  uint8_t patch945_data[] = {
    0x49, 0xF2, 0x41, 0xA7, 0x41, 0xDA, 0x49, 0xCB, 
  };
  APPLY_PATCH(945);

  uint32_t patch946_addr = 0x000C40F3;
  uint8_t patch946_data[] = {
    0xB2, 0x41, 0x51, 0x41, 0xD3, 0x49, 0x84, 0x49, 0x11, 0x41, 0xB1, 0x41, 0xB8, 
  };
  APPLY_PATCH(946);

  uint32_t patch947_addr = 0x000C4101;
  uint8_t patch947_data[] = {
    0x48, 
  };
  APPLY_PATCH(947);

  uint32_t patch948_addr = 0x000C410A;
  uint8_t patch948_data[] = {
    0x3D, 0xB2, 0x2C, 0xAD, 0x2C, 0x79, 0x20, 0xF4, 
  };
  APPLY_PATCH(948);

  uint32_t patch949_addr = 0x000C4114;
  uint8_t patch949_data[] = {
    0x26, 0x99, 0x3E, 0xC7, 0x3B, 0x72, 0x2D, 0x4F, 0x20, 0x5A, 0x2C, 0x27, 0x4D, 0x2C, 
  };
  APPLY_PATCH(949);

  uint32_t patch950_addr = 0x000C412A;
  uint8_t patch950_data[] = {
    0x4D, 0xC9, 0x4D, 0xD6, 0x4D, 0x85, 0x28, 0x60, 0x3F, 0xC7, 0x3F, 0x38, 0x3E, 0xBB, 0x43, 0x82, 
    0x43, 0x52, 0x4D, 0x52, 
  };
  APPLY_PATCH(950);

  uint32_t patch951_addr = 0x000C414A;
  uint8_t patch951_data[] = {
    0x26, 
  };
  APPLY_PATCH(951);

  uint32_t patch952_addr = 0x000C414D;
  uint8_t patch952_data[] = {
    0xE4, 0x3E, 0x19, 0x2D, 0x64, 0x49, 0xB0, 0x24, 0x16, 0x2C, 0x0C, 0x20, 0x5C, 0x25, 0xEA, 0x3A, 
    0x92, 
  };
  APPLY_PATCH(952);

  uint32_t patch953_addr = 0x000C415F;
  uint8_t patch953_data[] = {
    0xEC, 0x3A, 0x51, 
  };
  APPLY_PATCH(953);

  uint32_t patch954_addr = 0x000C416A;
  uint8_t patch954_data[] = {
    0x49, 0x7B, 
  };
  APPLY_PATCH(954);

  uint32_t patch955_addr = 0x000C416D;
  uint8_t patch955_data[] = {
    0x7E, 0x21, 0xDF, 0x49, 0x61, 
  };
  APPLY_PATCH(955);

  uint32_t patch956_addr = 0x000C4173;
  uint8_t patch956_data[] = {
    0x7B, 0x25, 0x99, 0x2D, 0x8C, 0x27, 0xAC, 0x2C, 0x98, 0x48, 0xF0, 0x49, 0xA3, 
  };
  APPLY_PATCH(956);

  uint32_t patch957_addr = 0x000C418A;
  uint8_t patch957_data[] = {
    0x49, 0x33, 0x41, 0x4A, 
  };
  APPLY_PATCH(957);

  uint32_t patch958_addr = 0x000C41CC;
  uint8_t patch958_data[] = {
    0x53, 0xFB, 0x50, 
  };
  APPLY_PATCH(958);

  uint32_t patch959_addr = 0x000C41D0;
  uint8_t patch959_data[] = {
    0x46, 0xA1, 0x5D, 0x5D, 
  };
  APPLY_PATCH(959);

  uint32_t patch960_addr = 0x000C41D5;
  uint8_t patch960_data[] = {
    0x72, 
  };
  APPLY_PATCH(960);

  uint32_t patch961_addr = 0x000C41D7;
  uint8_t patch961_data[] = {
    0x35, 0x3B, 0xC4, 0x28, 0x3C, 0x26, 0x32, 0x41, 0x40, 0x2C, 0x03, 
  };
  APPLY_PATCH(961);

  uint32_t patch962_addr = 0x000C41E3;
  uint8_t patch962_data[] = {
    0x08, 
  };
  APPLY_PATCH(962);

  uint32_t patch963_addr = 0x000C43D2;
  uint8_t patch963_data[] = {
    0x49, 0x4D, 0x31, 0x7D, 0x24, 0x50, 0x3A, 0xD6, 0x58, 
  };
  APPLY_PATCH(963);

  uint32_t patch964_addr = 0x000C43DD;
  uint8_t patch964_data[] = {
    0x2C, 0x35, 0x56, 0x3B, 0x14, 0x49, 
  };
  APPLY_PATCH(964);

  uint32_t patch965_addr = 0x000C43E4;
  uint8_t patch965_data[] = {
    0x41, 0x81, 
  };
  APPLY_PATCH(965);

  uint32_t patch966_addr = 0x000C4618;
  uint8_t patch966_data[] = {
    0x79, 
  };
  APPLY_PATCH(966);

  uint32_t patch967_addr = 0x000C4648;
  uint8_t patch967_data[] = {
    0x79, 
  };
  APPLY_PATCH(967);

  uint32_t patch968_addr = 0x000C4678;
  uint8_t patch968_data[] = {
    0x79, 
  };
  APPLY_PATCH(968);

  uint32_t patch969_addr = 0x000C46B6;
  uint8_t patch969_data[] = {
    0x75, 
  };
  APPLY_PATCH(969);

  uint32_t patch970_addr = 0x000C46FE;
  uint8_t patch970_data[] = {
    0x71, 
  };
  APPLY_PATCH(970);

  uint32_t patch971_addr = 0x000C4750;
  uint8_t patch971_data[] = {
    0x79, 
  };
  APPLY_PATCH(971);

  uint32_t patch972_addr = 0x000C4780;
  uint8_t patch972_data[] = {
    0x79, 
  };
  APPLY_PATCH(972);

  uint32_t patch973_addr = 0x000C54B0;
  uint8_t patch973_data[] = {
    0x94, 0x7C, 0x6D, 0x27, 0x65, 0x69, 
  };
  APPLY_PATCH(973);

  uint32_t patch974_addr = 0x000C55D6;
  uint8_t patch974_data[] = {
    0x18, 
  };
  APPLY_PATCH(974);

  uint32_t patch975_addr = 0x000C55D8;
  uint8_t patch975_data[] = {
    0xE9, 0x85, 0xF5, 0x6B, 0x18, 0x32, 0xED, 0x05, 0xEF, 0x03, 
  };
  APPLY_PATCH(975);

  uint32_t patch976_addr = 0x000C59AE;
  uint8_t patch976_data[] = {
    0x51, 0x44, 
  };
  APPLY_PATCH(976);

  uint32_t patch977_addr = 0x000C59B6;
  uint8_t patch977_data[] = {
    0x35, 0x68, 0x3E, 0xC1, 
  };
  APPLY_PATCH(977);

  uint32_t patch978_addr = 0x000C59CE;
  uint8_t patch978_data[] = {
    0xD1, 0x8F, 
  };
  APPLY_PATCH(978);

  uint32_t patch979_addr = 0x000C59D6;
  uint8_t patch979_data[] = {
    0xB1, 0x32, 0xBA, 0xD1, 
  };
  APPLY_PATCH(979);

  uint32_t patch980_addr = 0x000C59EE;
  uint8_t patch980_data[] = {
    0xD4, 0xC9, 
  };
  APPLY_PATCH(980);

  uint32_t patch981_addr = 0x000C59F6;
  uint8_t patch981_data[] = {
    0xB1, 0x1E, 0xBA, 0x34, 
  };
  APPLY_PATCH(981);

  uint32_t patch982_addr = 0x000C5A0E;
  uint8_t patch982_data[] = {
    0x5B, 0x63, 
  };
  APPLY_PATCH(982);

  uint32_t patch983_addr = 0x000C5A16;
  uint8_t patch983_data[] = {
    0x39, 0x02, 0x3A, 0x65, 
  };
  APPLY_PATCH(983);

  uint32_t patch984_addr = 0x000C5A2A;
  uint8_t patch984_data[] = {
    0x5C, 0xD6, 0x39, 0xCB, 0x5F, 
  };
  APPLY_PATCH(984);

  uint32_t patch985_addr = 0x000C5A36;
  uint8_t patch985_data[] = {
    0x3D, 0x13, 0x3E, 0xB6, 
  };
  APPLY_PATCH(985);

  uint32_t patch986_addr = 0x000C5A4A;
  uint8_t patch986_data[] = {
    0xDB, 0x64, 0xAD, 0x15, 0xDF, 0xD3, 
  };
  APPLY_PATCH(986);

  uint32_t patch987_addr = 0x000C5A56;
  uint8_t patch987_data[] = {
    0xB9, 0x6D, 0xBA, 0x04, 
  };
  APPLY_PATCH(987);

  uint32_t patch988_addr = 0x000C5B06;
  uint8_t patch988_data[] = {
    0x62, 
  };
  APPLY_PATCH(988);

  uint32_t patch989_addr = 0x000C5B2E;
  uint8_t patch989_data[] = {
    0x60, 
  };
  APPLY_PATCH(989);

  uint32_t patch990_addr = 0x000C5B62;
  uint8_t patch990_data[] = {
    0xE3, 
  };
  APPLY_PATCH(990);

  uint32_t patch991_addr = 0x000C85D2;
  uint8_t patch991_data[] = {
    0x4B, 
  };
  APPLY_PATCH(991);

  uint32_t patch992_addr = 0x000C85D4;
  uint8_t patch992_data[] = {
    0x43, 0x7D, 0x43, 0x22, 0x6B, 
  };
  APPLY_PATCH(992);

  uint32_t patch993_addr = 0x000C85DA;
  uint8_t patch993_data[] = {
    0x6B, 0x4E, 0x63, 0x5C, 
  };
  APPLY_PATCH(993);

  uint32_t patch994_addr = 0x000C85F4;
  uint8_t patch994_data[] = {
    0x43, 0x2C, 0x43, 0x0E, 0x6B, 
  };
  APPLY_PATCH(994);

  uint32_t patch995_addr = 0x000C85FA;
  uint8_t patch995_data[] = {
    0x6B, 0xEB, 
  };
  APPLY_PATCH(995);

  uint32_t patch996_addr = 0x000C85FD;
  uint8_t patch996_data[] = {
    0x6B, 
  };
  APPLY_PATCH(996);

  uint32_t patch997_addr = 0x000C8612;
  uint8_t patch997_data[] = {
    0x4B, 
  };
  APPLY_PATCH(997);

  uint32_t patch998_addr = 0x000C8614;
  uint8_t patch998_data[] = {
    0x4B, 0x10, 0x4B, 0x12, 
  };
  APPLY_PATCH(998);

  uint32_t patch999_addr = 0x000C8619;
  uint8_t patch999_data[] = {
    0x11, 0x6B, 0x10, 0x6B, 0x11, 
  };
  APPLY_PATCH(999);

  uint32_t patch1000_addr = 0x000C8632;
  uint8_t patch1000_data[] = {
    0x4F, 
  };
  APPLY_PATCH(1000);

  uint32_t patch1001_addr = 0x000C8634;
  uint8_t patch1001_data[] = {
    0x4F, 0x32, 0x4F, 0x03, 
  };
  APPLY_PATCH(1001);

  uint32_t patch1002_addr = 0x000C8639;
  uint8_t patch1002_data[] = {
    0xC2, 
  };
  APPLY_PATCH(1002);

  uint32_t patch1003_addr = 0x000C8644;
  uint8_t patch1003_data[] = {
    0x49, 
  };
  APPLY_PATCH(1003);

  uint32_t patch1004_addr = 0x000C8646;
  uint8_t patch1004_data[] = {
    0x49, 0xEE, 0x69, 0x68, 
  };
  APPLY_PATCH(1004);

  uint32_t patch1005_addr = 0x000C8652;
  uint8_t patch1005_data[] = {
    0x4B, 
  };
  APPLY_PATCH(1005);

  uint32_t patch1006_addr = 0x000C8654;
  uint8_t patch1006_data[] = {
    0x4B, 0xD8, 0x4B, 0x7D, 
  };
  APPLY_PATCH(1006);

  uint32_t patch1007_addr = 0x000C8659;
  uint8_t patch1007_data[] = {
    0x70, 
  };
  APPLY_PATCH(1007);

  uint32_t patch1008_addr = 0x000C8664;
  uint8_t patch1008_data[] = {
    0x49, 
  };
  APPLY_PATCH(1008);

  uint32_t patch1009_addr = 0x000C8666;
  uint8_t patch1009_data[] = {
    0x49, 0x5A, 
  };
  APPLY_PATCH(1009);

  uint32_t patch1010_addr = 0x000C8669;
  uint8_t patch1010_data[] = {
    0xB8, 0x69, 0xF0, 0x69, 0x0F, 
  };
  APPLY_PATCH(1010);

  uint32_t patch1011_addr = 0x000C866F;
  uint8_t patch1011_data[] = {
    0x56, 
  };
  APPLY_PATCH(1011);

  uint32_t patch1012_addr = 0x000C8672;
  uint8_t patch1012_data[] = {
    0x4B, 
  };
  APPLY_PATCH(1012);

  uint32_t patch1013_addr = 0x000C8674;
  uint8_t patch1013_data[] = {
    0x4B, 0x37, 0x4B, 0x64, 
  };
  APPLY_PATCH(1013);

  uint32_t patch1014_addr = 0x000C8679;
  uint8_t patch1014_data[] = {
    0xE5, 
  };
  APPLY_PATCH(1014);

  uint32_t patch1015_addr = 0x000C8682;
  uint8_t patch1015_data[] = {
    0x49, 
  };
  APPLY_PATCH(1015);

  uint32_t patch1016_addr = 0x000C8684;
  uint8_t patch1016_data[] = {
    0x41, 
  };
  APPLY_PATCH(1016);

  uint32_t patch1017_addr = 0x000C8686;
  uint8_t patch1017_data[] = {
    0x41, 0xEE, 
  };
  APPLY_PATCH(1017);

  uint32_t patch1018_addr = 0x000C8689;
  uint8_t patch1018_data[] = {
    0x20, 0x69, 0xA8, 0x61, 0xB0, 
  };
  APPLY_PATCH(1018);

  uint32_t patch1019_addr = 0x000C8990;
  uint8_t patch1019_data[] = {
    0x1A, 
  };
  APPLY_PATCH(1019);

  uint32_t patch1020_addr = 0x000C8992;
  uint8_t patch1020_data[] = {
    0x1A, 0xA1, 0x12, 0x20, 
  };
  APPLY_PATCH(1020);

  uint32_t patch1021_addr = 0x000C899A;
  uint8_t patch1021_data[] = {
    0x3A, 
  };
  APPLY_PATCH(1021);

  uint32_t patch1022_addr = 0x000C899C;
  uint8_t patch1022_data[] = {
    0x32, 0x2C, 0x32, 0x0A, 
  };
  APPLY_PATCH(1022);

  uint32_t patch1023_addr = 0x000CA2B0;
  uint8_t patch1023_data[] = {
    0x1E, 0x7C, 0x1E, 0x69, 0x16, 0x21, 0x16, 0x38, 0x3E, 0x9D, 
  };
  APPLY_PATCH(1023);

  uint32_t patch1024_addr = 0x000CA2D6;
  uint8_t patch1024_data[] = {
    0x40, 
  };
  APPLY_PATCH(1024);

  uint32_t patch1025_addr = 0x000CA2D8;
  uint8_t patch1025_data[] = {
    0x72, 0x9A, 0x49, 
  };
  APPLY_PATCH(1025);

  uint32_t patch1026_addr = 0x000CA2DC;
  uint8_t patch1026_data[] = {
    0xC2, 0x22, 0xE5, 0xF6, 0x6B, 0x24, 0x6B, 0x72, 0x6E, 0x1F, 0x69, 0x67, 0x38, 0x01, 0x38, 0x9C, 
    0x30, 0xC9, 0x30, 0xB4, 
  };
  APPLY_PATCH(1026);

  uint32_t patch1027_addr = 0x000CA2F7;
  uint8_t patch1027_data[] = {
    0x91, 
  };
  APPLY_PATCH(1027);

  uint32_t patch1028_addr = 0x000CA34A;
  uint8_t patch1028_data[] = {
    0x4B, 0x47, 0x4C, 0xB5, 
  };
  APPLY_PATCH(1028);

  uint32_t patch1029_addr = 0x000CA34F;
  uint8_t patch1029_data[] = {
    0x55, 0x7F, 0x3D, 0x71, 0xE3, 
  };
  APPLY_PATCH(1029);

  uint32_t patch1030_addr = 0x000CA355;
  uint8_t patch1030_data[] = {
    0x41, 0x43, 0x7C, 0x7F, 0x22, 
  };
  APPLY_PATCH(1030);

  uint32_t patch1031_addr = 0x000CA35B;
  uint8_t patch1031_data[] = {
    0x97, 0x1B, 0x9B, 0x23, 0xF9, 
  };
  APPLY_PATCH(1031);

  uint32_t patch1032_addr = 0x000CA361;
  uint8_t patch1032_data[] = {
    0x60, 
  };
  APPLY_PATCH(1032);

  uint32_t patch1033_addr = 0x000CA3C0;
  uint8_t patch1033_data[] = {
    0xD0, 0x8C, 0x69, 
  };
  APPLY_PATCH(1033);

  uint32_t patch1034_addr = 0x000CA3C4;
  uint8_t patch1034_data[] = {
    0xA1, 0xD8, 0x10, 0xFE, 
  };
  APPLY_PATCH(1034);

  uint32_t patch1035_addr = 0x000CA3E6;
  uint8_t patch1035_data[] = {
    0x98, 0x99, 0x49, 
  };
  APPLY_PATCH(1035);

  uint32_t patch1036_addr = 0x000CA3EA;
  uint8_t patch1036_data[] = {
    0xD9, 0x2C, 0x30, 0x38, 0x30, 0xA7, 
  };
  APPLY_PATCH(1036);

  uint32_t patch1037_addr = 0x000CA416;
  uint8_t patch1037_data[] = {
    0x2A, 0x36, 0xB4, 
  };
  APPLY_PATCH(1037);

  uint32_t patch1038_addr = 0x000CA41A;
  uint8_t patch1038_data[] = {
    0x41, 0x53, 0x32, 0xA2, 0xCA, 
  };
  APPLY_PATCH(1038);

  uint32_t patch1039_addr = 0x000CA420;
  uint8_t patch1039_data[] = {
    0x65, 0x9C, 0x51, 0x7B, 0x6D, 
  };
  APPLY_PATCH(1039);

  uint32_t patch1040_addr = 0x000CA426;
  uint8_t patch1040_data[] = {
    0x2C, 0xA3, 0xB2, 0xFF, 0x3C, 0xB8, 
  };
  APPLY_PATCH(1040);

  uint32_t patch1041_addr = 0x000CA434;
  uint8_t patch1041_data[] = {
    0x90, 
  };
  APPLY_PATCH(1041);

  uint32_t patch1042_addr = 0x000CA440;
  uint8_t patch1042_data[] = {
    0x96, 
  };
  APPLY_PATCH(1042);

  uint32_t patch1043_addr = 0x000CA449;
  uint8_t patch1043_data[] = {
    0x78, 
  };
  APPLY_PATCH(1043);

  uint32_t patch1044_addr = 0x000CA44C;
  uint8_t patch1044_data[] = {
    0x1B, 
  };
  APPLY_PATCH(1044);

  uint32_t patch1045_addr = 0x000CA455;
  uint8_t patch1045_data[] = {
    0xCA, 
  };
  APPLY_PATCH(1045);

  uint32_t patch1046_addr = 0x000CA457;
  uint8_t patch1046_data[] = {
    0x61, 0xB2, 0x11, 
  };
  APPLY_PATCH(1046);

  uint32_t patch1047_addr = 0x000CA45C;
  uint8_t patch1047_data[] = {
    0x1B, 
  };
  APPLY_PATCH(1047);

  uint32_t patch1048_addr = 0x000CA45E;
  uint8_t patch1048_data[] = {
    0xF3, 0x97, 0x0F, 0x11, 0x2D, 0x9E, 0x7B, 0x05, 0x2E, 0xA6, 0xF1, 0xD6, 0x2F, 0xD3, 0x0D, 0x1E, 
    0x59, 
  };
  APPLY_PATCH(1048);

  uint32_t patch1049_addr = 0x000CA470;
  uint8_t patch1049_data[] = {
    0x23, 0x13, 0x6B, 0x69, 0x6B, 0x2B, 0xD3, 0x0A, 0x2D, 0xC6, 0x0F, 0x32, 0x51, 0x58, 0x0C, 0x28, 
    0xD1, 0x2F, 0x0F, 0xED, 0x25, 0xE0, 0x79, 0xCE, 0x09, 0xD2, 0x48, 0xB4, 0xF9, 0xDE, 0x27, 0x2D, 
    0x2F, 0x6D, 0x61, 0x76, 0x24, 0x27, 0xDB, 0xD1, 0x2D, 0x76, 0x0F, 0xFE, 0x4B, 0x40, 0x0B, 0xD5, 
    0x6C, 0x97, 0x2D, 0x4D, 0x64, 0x3C, 0xDD, 0x7C, 0x2B, 0xD6, 0x09, 0xAD, 0x43, 0x37, 0x02, 0x0E, 
    0xD7, 0x7C, 0x09, 0x24, 0x23, 0x5E, 0x63, 0x76, 0x0F, 0x45, 0x4C, 0xBA, 0xFF, 0x6B, 0x21, 0xFA, 
    0x2D, 0xE7, 0x67, 0x60, 0x26, 0xA7, 0xD9, 0x00, 0x2F, 0xAA, 0x0D, 0x4B, 0x4D, 0x20, 0x43, 0x6F, 
    0x2B, 0x42, 0x69, 0x8D, 0xDB, 0x33, 0x05, 0x13, 0x0F, 0xA3, 0x5D, 0x37, 0xBB, 0x6C, 0x03, 0xD3, 
  };
  APPLY_PATCH(1049);

  uint32_t patch1050_addr = 0x000DD998;
  uint8_t patch1050_data[] = {
    0x61, 0xCE, 
  };
  APPLY_PATCH(1050);

  uint32_t patch1051_addr = 0x000EA73C;
  uint8_t patch1051_data[] = {
    0x44, 
  };
  APPLY_PATCH(1051);

  uint32_t patch1052_addr = 0x000EB2D2;
  uint8_t patch1052_data[] = {
    0xE0, 
  };
  APPLY_PATCH(1052);

  uint32_t patch1053_addr = 0x000F5760;
  uint8_t patch1053_data[] = {
    0x50, 
  };
  APPLY_PATCH(1053);

  uint32_t patch1054_addr = 0x000F5766;
  uint8_t patch1054_data[] = {
    0x50, 
  };
  APPLY_PATCH(1054);

  uint32_t patch1055_addr = 0x000F5768;
  uint8_t patch1055_data[] = {
    0xE6, 0x58, 0xA9, 0x19, 0x68, 0x5E, 0xFB, 0x9E, 0xE3, 0x41, 0x15, 0x0A, 0xE6, 
  };
  APPLY_PATCH(1055);

  uint32_t patch1056_addr = 0x000F5776;
  uint8_t patch1056_data[] = {
    0xD6, 0xEE, 0x68, 0xED, 0xFB, 0xCA, 0xD0, 0x92, 0x68, 0x83, 0x79, 0x53, 0x50, 0x99, 0x63, 0xA1, 
    0xE0, 0xFF, 0xCF, 0x11, 0x6F, 0x74, 0x58, 0x4A, 0xE0, 0xA5, 0x7B, 0xAF, 0xD0, 0xA1, 0x6A, 0x6E, 
    0xB7, 0x42, 0x90, 0xD0, 0x79, 0xA6, 0x43, 0x4F, 
  };
  APPLY_PATCH(1056);

  uint32_t patch1057_addr = 0x000F5887;
  uint8_t patch1057_data[] = {
    0xEE, 
  };
  APPLY_PATCH(1057);

  uint32_t patch1058_addr = 0x000F588D;
  uint8_t patch1058_data[] = {
    0xB2, 0xF8, 
  };
  APPLY_PATCH(1058);

  uint32_t patch1059_addr = 0x000F5890;
  uint8_t patch1059_data[] = {
    0xF8, 
  };
  APPLY_PATCH(1059);

  uint32_t patch1060_addr = 0x000F5892;
  uint8_t patch1060_data[] = {
    0xF9, 
  };
  APPLY_PATCH(1060);

  uint32_t patch1061_addr = 0x000F5894;
  uint8_t patch1061_data[] = {
    0x71, 
  };
  APPLY_PATCH(1061);

  uint32_t patch1062_addr = 0x000F5896;
  uint8_t patch1062_data[] = {
    0x70, 0x9F, 0x7D, 0x19, 0x78, 0xBF, 0x00, 0x0E, 
  };
  APPLY_PATCH(1062);

  uint32_t patch1063_addr = 0x000F589F;
  uint8_t patch1063_data[] = {
    0x14, 
  };
  APPLY_PATCH(1063);

  uint32_t patch1064_addr = 0x000F58DC;
  uint8_t patch1064_data[] = {
    0x08, 0x6C, 0xF1, 
  };
  APPLY_PATCH(1064);

  uint32_t patch1065_addr = 0x000F58E0;
  uint8_t patch1065_data[] = {
    0xF7, 0x40, 
  };
  APPLY_PATCH(1065);

  uint32_t patch1066_addr = 0x000F58F8;
  uint8_t patch1066_data[] = {
    0x1B, 0xC4, 
  };
  APPLY_PATCH(1066);

  uint32_t patch1067_addr = 0x000F59C6;
  uint8_t patch1067_data[] = {
    0x63, 
  };
  APPLY_PATCH(1067);

  uint32_t patch1068_addr = 0x000F59C8;
  uint8_t patch1068_data[] = {
    0x6B, 
  };
  APPLY_PATCH(1068);

  uint32_t patch1069_addr = 0x000F59CA;
  uint8_t patch1069_data[] = {
    0xF6, 0x6C, 0xB1, 0x99, 0xF7, 0xDB, 0x45, 0xB7, 0xF6, 0x6D, 0xCE, 0x1F, 0xA2, 0x50, 0xE9, 0xB5, 
    0xC0, 0x66, 0xF3, 0x7C, 0x70, 0x05, 0x5F, 0x71, 0xFB, 0x3D, 0xE3, 0x81, 0x60, 
  };
  APPLY_PATCH(1069);

  uint32_t patch1070_addr = 0x000F59E8;
  uint8_t patch1070_data[] = {
    0xF9, 0x03, 0x79, 0x17, 0xB1, 0x4B, 0xF3, 0xE9, 0x80, 0x08, 0x78, 
  };
  APPLY_PATCH(1070);

  uint32_t patch1071_addr = 0x000F59F4;
  uint8_t patch1071_data[] = {
    0x75, 0x7D, 0x08, 0x60, 0xE9, 0x60, 0xDB, 0xE6, 
  };
  APPLY_PATCH(1071);

  uint32_t patch1072_addr = 0x000F5C5A;
  uint8_t patch1072_data[] = {
    0x6B, 
  };
  APPLY_PATCH(1072);

  uint32_t patch1073_addr = 0x000F5C5C;
  uint8_t patch1073_data[] = {
    0x6B, 
  };
  APPLY_PATCH(1073);

  uint32_t patch1074_addr = 0x000F5C5E;
  uint8_t patch1074_data[] = {
    0xF6, 0xD9, 0xB9, 0x50, 0xFF, 0xCA, 0x45, 0x21, 0xF6, 0x7A, 0xC6, 0xDA, 0xAA, 0x82, 0xE9, 0x1F, 
    0xC0, 0x7E, 0xFB, 0xF1, 0x78, 0x5D, 0x5F, 0x33, 0xFB, 0x23, 0xEB, 0xE5, 0x68, 
  };
  APPLY_PATCH(1074);

  uint32_t patch1075_addr = 0x000F5C7C;
  uint8_t patch1075_data[] = {
    0xF9, 0x28, 0x79, 0xC8, 0x39, 0x5C, 0x7B, 0xEE, 0x08, 0x82, 0xF0, 
  };
  APPLY_PATCH(1075);

  uint32_t patch1076_addr = 0x000F5C88;
  uint8_t patch1076_data[] = {
    0xFD, 0x71, 0x80, 0xC6, 0x61, 0x90, 0x53, 0x03, 
  };
  APPLY_PATCH(1076);

  uint32_t patch1077_addr = 0x000F5EF0;
  uint8_t patch1077_data[] = {
    0x7B, 
  };
  APPLY_PATCH(1077);

  uint32_t patch1078_addr = 0x000F5EF2;
  uint8_t patch1078_data[] = {
    0x7B, 
  };
  APPLY_PATCH(1078);

  uint32_t patch1079_addr = 0x000F5EF4;
  uint8_t patch1079_data[] = {
    0xEE, 0x71, 0xA1, 0xB1, 0xEF, 0xC1, 0x55, 0x15, 0xEE, 0x91, 0xDE, 0xDA, 0x3A, 0x3A, 0x79, 0xE2, 
    0x50, 0x4F, 0x6B, 0x86, 0xE8, 0xDA, 0xCF, 0xB6, 0x6B, 0x82, 0x7B, 0x0B, 0xF8, 
  };
  APPLY_PATCH(1079);

  uint32_t patch1080_addr = 0x000F5F12;
  uint8_t patch1080_data[] = {
    0x69, 0xF9, 0xE9, 0xEB, 0x29, 0xDA, 0x6B, 0x3D, 0x10, 0x50, 0xE8, 
  };
  APPLY_PATCH(1080);

  uint32_t patch1081_addr = 0x000F5F1E;
  uint8_t patch1081_data[] = {
    0xED, 0x12, 0x94, 0x42, 0x7D, 0x00, 0x4F, 0x81, 
  };
  APPLY_PATCH(1081);

  uint32_t patch1082_addr = 0x000F6186;
  uint8_t patch1082_data[] = {
    0xE3, 
  };
  APPLY_PATCH(1082);

  uint32_t patch1083_addr = 0x000F6188;
  uint8_t patch1083_data[] = {
    0xEB, 
  };
  APPLY_PATCH(1083);

  uint32_t patch1084_addr = 0x000F618A;
  uint8_t patch1084_data[] = {
    0x76, 0x13, 0x31, 0x28, 0x77, 0xC0, 0xC5, 0x9E, 0x76, 0xEF, 0x4E, 0x2C, 0x22, 0xFA, 0x69, 0xAE, 
    0x40, 0xAE, 0x73, 0x62, 0xF0, 0x44, 0xDB, 0x5A, 0x7F, 0x4B, 0x67, 0xCD, 0xE4, 
  };
  APPLY_PATCH(1084);

  uint32_t patch1085_addr = 0x000F61A8;
  uint8_t patch1085_data[] = {
    0x7D, 0x67, 0xFD, 0xA2, 0x35, 0xAB, 0x77, 0x64, 0x04, 0x80, 0xFC, 
  };
  APPLY_PATCH(1085);

  uint32_t patch1086_addr = 0x000F61B4;
  uint8_t patch1086_data[] = {
    0xF1, 0x9D, 0x8C, 0x16, 0x6D, 0x95, 0x5F, 0xFC, 
  };
  APPLY_PATCH(1086);

  uint32_t patch1087_addr = 0x000F641C;
  uint8_t patch1087_data[] = {
    0xEB, 
  };
  APPLY_PATCH(1087);

  uint32_t patch1088_addr = 0x000F641E;
  uint8_t patch1088_data[] = {
    0xEB, 
  };
  APPLY_PATCH(1088);

  uint32_t patch1089_addr = 0x000F6420;
  uint8_t patch1089_data[] = {
    0x72, 0x7C, 0x3D, 0x0B, 0x7B, 0x42, 0xC1, 0x81, 0x72, 0xFF, 0x42, 0xB4, 0x2E, 0xCA, 0x6D, 0x1D, 
    0x44, 0x07, 0x7F, 0x3D, 0xFC, 0x12, 0xDB, 0x07, 0x7F, 0x85, 0x6F, 0x10, 0xEC, 
  };
  APPLY_PATCH(1089);

  uint32_t patch1090_addr = 0x000F643E;
  uint8_t patch1090_data[] = {
    0x7D, 0xBF, 0xF9, 0xCD, 0x39, 0xBF, 0x7B, 0xF0, 0x00, 0x9D, 0xF8, 
  };
  APPLY_PATCH(1090);

  uint32_t patch1091_addr = 0x000F644A;
  uint8_t patch1091_data[] = {
    0xFD, 0x35, 0x80, 0x15, 0x69, 0xF3, 0x5B, 0x20, 
  };
  APPLY_PATCH(1091);

  uint32_t patch1092_addr = 0x000F6B40;
  uint8_t patch1092_data[] = {
    0x18, 0x7F, 0x18, 0x74, 0x18, 0xC5, 0x18, 0xE0, 0x18, 0x65, 
  };
  APPLY_PATCH(1092);

  uint32_t patch1093_addr = 0x000FD212;
  uint8_t patch1093_data[] = {
    0x1A, 0x7F, 0x1A, 0x7E, 0x1A, 0x7C, 0x1A, 0x7F, 0x1A, 0x7E, 
  };
  APPLY_PATCH(1093);

  uint32_t patch1094_addr = 0x000FD32E;
  uint8_t patch1094_data[] = {
    0x35, 
  };
  APPLY_PATCH(1094);

  uint32_t patch1095_addr = 0x000FD330;
  uint8_t patch1095_data[] = {
    0xD6, 0x38, 0x6F, 0x95, 
  };
  APPLY_PATCH(1095);

  uint32_t patch1096_addr = 0x000FD341;
  uint8_t patch1096_data[] = {
    0x17, 
  };
  APPLY_PATCH(1096);

  uint32_t patch1097_addr = 0x000FD366;
  uint8_t patch1097_data[] = {
    0x50, 0xA8, 0xE9, 
  };
  APPLY_PATCH(1097);

  uint32_t patch1098_addr = 0x000FD36A;
  uint8_t patch1098_data[] = {
    0xE9, 0x53, 0xE9, 0x3E, 0x6B, 0x8B, 
  };
  APPLY_PATCH(1098);

  uint32_t patch1099_addr = 0x000FD3B2;
  uint8_t patch1099_data[] = {
    0x5F, 
  };
  APPLY_PATCH(1099);

  uint32_t patch1100_addr = 0x000FD3BC;
  uint8_t patch1100_data[] = {
    0x47, 
  };
  APPLY_PATCH(1100);

  uint32_t patch1101_addr = 0x000FD3C6;
  uint8_t patch1101_data[] = {
    0xF1, 
  };
  APPLY_PATCH(1101);

  uint32_t patch1102_addr = 0x000FD492;
  uint8_t patch1102_data[] = {
    0x4B, 
  };
  APPLY_PATCH(1102);

  uint32_t patch1103_addr = 0x000FD49C;
  uint8_t patch1103_data[] = {
    0x53, 
  };
  APPLY_PATCH(1103);

  uint32_t patch1104_addr = 0x000FD4A6;
  uint8_t patch1104_data[] = {
    0x65, 
  };
  APPLY_PATCH(1104);

  uint32_t patch1105_addr = 0x000FD526;
  uint8_t patch1105_data[] = {
    0x0C, 0x7F, 0x0C, 0xD3, 0x0C, 0xA7, 0x0C, 0xB8, 0x0C, 0xEB, 
  };
  APPLY_PATCH(1105);

  uint32_t patch1106_addr = 0x000FD642;
  uint8_t patch1106_data[] = {
    0xB1, 
  };
  APPLY_PATCH(1106);

  uint32_t patch1107_addr = 0x000FD644;
  uint8_t patch1107_data[] = {
    0x50, 0xFC, 0xE9, 0xCE, 
  };
  APPLY_PATCH(1107);

  uint32_t patch1108_addr = 0x000FD655;
  uint8_t patch1108_data[] = {
    0xDE, 
  };
  APPLY_PATCH(1108);

  uint32_t patch1109_addr = 0x000FD67A;
  uint8_t patch1109_data[] = {
    0x52, 0x4D, 0xEB, 
  };
  APPLY_PATCH(1109);

  uint32_t patch1110_addr = 0x000FD67E;
  uint8_t patch1110_data[] = {
    0xEB, 0xFC, 0x69, 0x01, 0xEB, 0xEE, 
  };
  APPLY_PATCH(1110);

  uint32_t patch1111_addr = 0x000FD6C6;
  uint8_t patch1111_data[] = {
    0xD1, 
  };
  APPLY_PATCH(1111);

  uint32_t patch1112_addr = 0x000FD6D0;
  uint8_t patch1112_data[] = {
    0xCB, 
  };
  APPLY_PATCH(1112);

  uint32_t patch1113_addr = 0x000FD6DA;
  uint8_t patch1113_data[] = {
    0xFB, 
  };
  APPLY_PATCH(1113);

  uint32_t patch1114_addr = 0x000FD7A6;
  uint8_t patch1114_data[] = {
    0x55, 
  };
  APPLY_PATCH(1114);

  uint32_t patch1115_addr = 0x000FD7B0;
  uint8_t patch1115_data[] = {
    0x4F, 
  };
  APPLY_PATCH(1115);

  uint32_t patch1116_addr = 0x000FD7BA;
  uint8_t patch1116_data[] = {
    0x7F, 
  };
  APPLY_PATCH(1116);

  uint32_t patch1117_addr = 0x000FD83C;
  uint8_t patch1117_data[] = {
    0x0E, 0x1E, 0x0E, 0x81, 0x88, 0xBF, 0x88, 0xCC, 0x88, 0xBE, 
  };
  APPLY_PATCH(1117);

  uint32_t patch1118_addr = 0x000FD958;
  uint8_t patch1118_data[] = {
    0xA3, 
  };
  APPLY_PATCH(1118);

  uint32_t patch1119_addr = 0x000FD95A;
  uint8_t patch1119_data[] = {
    0x42, 0xA7, 0xFB, 0xDB, 
  };
  APPLY_PATCH(1119);

  uint32_t patch1120_addr = 0x000FD96B;
  uint8_t patch1120_data[] = {
    0x7D, 
  };
  APPLY_PATCH(1120);

  uint32_t patch1121_addr = 0x000FD990;
  uint8_t patch1121_data[] = {
    0xC2, 0xF4, 0x7B, 
  };
  APPLY_PATCH(1121);

  uint32_t patch1122_addr = 0x000FD994;
  uint8_t patch1122_data[] = {
    0x73, 0x66, 0x73, 0xC8, 0xF9, 0x9E, 
  };
  APPLY_PATCH(1122);

  uint32_t patch1123_addr = 0x000FDB4C;
  uint8_t patch1123_data[] = {
    0x98, 0xF8, 0x98, 0x13, 0x9A, 0x63, 0x9A, 0xBB, 0x9A, 0x11, 
  };
  APPLY_PATCH(1123);

  uint32_t patch1124_addr = 0x000FDC68;
  uint8_t patch1124_data[] = {
    0xA1, 
  };
  APPLY_PATCH(1124);

  uint32_t patch1125_addr = 0x000FDC6A;
  uint8_t patch1125_data[] = {
    0x40, 0xDC, 0xF9, 0x2F, 
  };
  APPLY_PATCH(1125);

  uint32_t patch1126_addr = 0x000FDC7B;
  uint8_t patch1126_data[] = {
    0x25, 
  };
  APPLY_PATCH(1126);

  uint32_t patch1127_addr = 0x000FDCA0;
  uint8_t patch1127_data[] = {
    0xC4, 0xEF, 0x7D, 
  };
  APPLY_PATCH(1127);

  uint32_t patch1128_addr = 0x000FDCA4;
  uint8_t patch1128_data[] = {
    0x75, 0x08, 0x75, 0x52, 0xFF, 0xD5, 
  };
  APPLY_PATCH(1128);

  uint32_t patch1129_addr = 0x000FDF21;
  uint8_t patch1129_data[] = {
    0x2A, 
  };
  APPLY_PATCH(1129);

  uint32_t patch1130_addr = 0x000FDF36;
  uint8_t patch1130_data[] = {
    0x1E, 0xBC, 0x1E, 0x81, 0x1E, 0x32, 
  };
  APPLY_PATCH(1130);

  uint32_t patch1131_addr = 0x000FDF3D;
  uint8_t patch1131_data[] = {
    0x12, 
  };
  APPLY_PATCH(1131);

  uint32_t patch1132_addr = 0x000FDFC8;
  uint8_t patch1132_data[] = {
    0x98, 0xDE, 0x98, 0x22, 0x90, 0x95, 
  };
  APPLY_PATCH(1132);

  uint32_t patch1133_addr = 0x000FDFCF;
  uint8_t patch1133_data[] = {
    0xDE, 
  };
  APPLY_PATCH(1133);

  uint32_t patch1134_addr = 0x000FE080;
  uint8_t patch1134_data[] = {
    0x59, 
  };
  APPLY_PATCH(1134);

  uint32_t patch1135_addr = 0x000FE084;
  uint8_t patch1135_data[] = {
    0x79, 
  };
  APPLY_PATCH(1135);

  uint32_t patch1136_addr = 0x000FE088;
  uint8_t patch1136_data[] = {
    0x49, 
  };
  APPLY_PATCH(1136);

  uint32_t patch1137_addr = 0x000FE08C;
  uint8_t patch1137_data[] = {
    0x51, 
  };
  APPLY_PATCH(1137);

  uint32_t patch1138_addr = 0x000FE090;
  uint8_t patch1138_data[] = {
    0x53, 
  };
  APPLY_PATCH(1138);

  uint32_t patch1139_addr = 0x000FE094;
  uint8_t patch1139_data[] = {
    0x7F, 
  };
  APPLY_PATCH(1139);

  uint32_t patch1140_addr = 0x000FE098;
  uint8_t patch1140_data[] = {
    0x37, 
  };
  APPLY_PATCH(1140);

  uint32_t patch1141_addr = 0x000FE0A0;
  uint8_t patch1141_data[] = {
    0x41, 
  };
  APPLY_PATCH(1141);

  uint32_t patch1142_addr = 0x000FE0A4;
  uint8_t patch1142_data[] = {
    0x45, 
  };
  APPLY_PATCH(1142);

  uint32_t patch1143_addr = 0x000FE0A8;
  uint8_t patch1143_data[] = {
    0x4D, 
  };
  APPLY_PATCH(1143);

  uint32_t patch1144_addr = 0x000FE0AC;
  uint8_t patch1144_data[] = {
    0x45, 
  };
  APPLY_PATCH(1144);

  uint32_t patch1145_addr = 0x000FE0B0;
  uint8_t patch1145_data[] = {
    0x6F, 
  };
  APPLY_PATCH(1145);

  uint32_t patch1146_addr = 0x000FE0B4;
  uint8_t patch1146_data[] = {
    0x33, 
  };
  APPLY_PATCH(1146);

  uint32_t patch1147_addr = 0x000FE0B8;
  uint8_t patch1147_data[] = {
    0x43, 
  };
  APPLY_PATCH(1147);

  uint32_t patch1148_addr = 0x000FE0BE;
  uint8_t patch1148_data[] = {
    0x77, 
  };
  APPLY_PATCH(1148);

  uint32_t patch1149_addr = 0x000FE0C0;
  uint8_t patch1149_data[] = {
    0x7B, 
  };
  APPLY_PATCH(1149);

  uint32_t patch1150_addr = 0x000FE0D8;
  uint8_t patch1150_data[] = {
    0x7F, 
  };
  APPLY_PATCH(1150);

  uint32_t patch1151_addr = 0x000FE0DA;
  uint8_t patch1151_data[] = {
    0x78, 
  };
  APPLY_PATCH(1151);

  uint32_t patch1152_addr = 0x000FE0DC;
  uint8_t patch1152_data[] = {
    0x70, 
  };
  APPLY_PATCH(1152);

  uint32_t patch1153_addr = 0x000FE0DE;
  uint8_t patch1153_data[] = {
    0x71, 
  };
  APPLY_PATCH(1153);

  uint32_t patch1154_addr = 0x000FE0E0;
  uint8_t patch1154_data[] = {
    0x7B, 
  };
  APPLY_PATCH(1154);

  uint32_t patch1155_addr = 0x000FE11A;
  uint8_t patch1155_data[] = {
    0x19, 0x01, 
  };
  APPLY_PATCH(1155);

  uint32_t patch1156_addr = 0x000FE11D;
  uint8_t patch1156_data[] = {
    0x57, 0x81, 0x61, 
  };
  APPLY_PATCH(1156);

  uint32_t patch1157_addr = 0x000FE121;
  uint8_t patch1157_data[] = {
    0xFC, 0x4C, 0x01, 0x7D, 0x81, 
  };
  APPLY_PATCH(1157);

  uint32_t patch1158_addr = 0x000FE12A;
  uint8_t patch1158_data[] = {
    0x88, 0xEC, 0x7D, 0x17, 0x5A, 0xA2, 0x7F, 0x3A, 0xEC, 0x96, 0x7F, 0xF0, 
  };
  APPLY_PATCH(1158);

  uint32_t patch1159_addr = 0x000FE40C;
  uint8_t patch1159_data[] = {
    0x05, 
  };
  APPLY_PATCH(1159);

  uint32_t patch1160_addr = 0x000FE40E;
  uint8_t patch1160_data[] = {
    0x79, 
  };
  APPLY_PATCH(1160);

  uint32_t patch1161_addr = 0x000FE410;
  uint8_t patch1161_data[] = {
    0x6B, 0x44, 0x0A, 0x7F, 0x07, 
  };
  APPLY_PATCH(1161);

  uint32_t patch1162_addr = 0x000FE416;
  uint8_t patch1162_data[] = {
    0x7B, 
  };
  APPLY_PATCH(1162);

  uint32_t patch1163_addr = 0x000FE418;
  uint8_t patch1163_data[] = {
    0x6A, 0x31, 0x0A, 0x7E, 
  };
  APPLY_PATCH(1163);

  uint32_t patch1164_addr = 0x000FF016;
  uint8_t patch1164_data[] = {
    0x0A, 0x7C, 0x0A, 0x7F, 0x0A, 0x7E, 
  };
  APPLY_PATCH(1164);

  uint32_t patch1165_addr = 0x000FF01D;
  uint8_t patch1165_data[] = {
    0x51, 
  };
  APPLY_PATCH(1165);

  uint32_t patch1166_addr = 0x000FF148;
  uint8_t patch1166_data[] = {
    0x88, 0x65, 0x88, 0x09, 0x88, 0xF8, 
  };
  APPLY_PATCH(1166);

  uint32_t patch1167_addr = 0x000FF14F;
  uint8_t patch1167_data[] = {
    0x3D, 
  };
  APPLY_PATCH(1167);

  uint32_t patch1168_addr = 0x000FF522;
  uint8_t patch1168_data[] = {
    0x0C, 0x4E, 0x0C, 0xE2, 0x0C, 0x7F, 0x0C, 0xD3, 0x0C, 0xA7, 
  };
  APPLY_PATCH(1168);

  uint32_t patch1169_addr = 0x000FFFAE;
  uint8_t patch1169_data[] = {
    0x14, 0x2A, 0x1E, 0x9D, 0x1E, 0x08, 0x16, 0xA2, 
  };
  APPLY_PATCH(1169);

  uint32_t patch1170_addr = 0x000FFFDC;
  uint8_t patch1170_data[] = {
    0x92, 0x32, 0x92, 0x4B, 0x98, 0x1B, 0x98, 0x14, 
  };
  APPLY_PATCH(1170);

  uint32_t patch1171_addr = 0x00100136;
  uint8_t patch1171_data[] = {
    0x19, 0xBC, 0x39, 0x81, 0x39, 0x32, 0x39, 0x3C, 
  };
  APPLY_PATCH(1171);

  uint32_t patch1172_addr = 0x0010017A;
  uint8_t patch1172_data[] = {
    0x39, 0x91, 0x39, 0xEC, 0x39, 0xCD, 
  };
  APPLY_PATCH(1172);

  uint32_t patch1173_addr = 0x00100181;
  uint8_t patch1173_data[] = {
    0x26, 
  };
  APPLY_PATCH(1173);

  uint32_t patch1174_addr = 0x00100418;
  uint8_t patch1174_data[] = {
    0x38, 0x7F, 0x38, 0x7E, 0x38, 0x7F, 0x38, 0x7B, 
  };
  APPLY_PATCH(1174);

  uint32_t patch1175_addr = 0x00101D08;
  uint8_t patch1175_data[] = {
    0x38, 
  };
  APPLY_PATCH(1175);

  uint32_t patch1176_addr = 0x00101D0A;
  uint8_t patch1176_data[] = {
    0x38, 0xDC, 0x38, 0xAB, 
  };
  APPLY_PATCH(1176);

  uint32_t patch1177_addr = 0x00101DCE;
  uint8_t patch1177_data[] = {
    0xB0, 0xF0, 0x98, 0xDD, 0x98, 0x23, 
  };
  APPLY_PATCH(1177);

  uint32_t patch1178_addr = 0x00101DD5;
  uint8_t patch1178_data[] = {
    0x3D, 
  };
  APPLY_PATCH(1178);

  uint32_t patch1179_addr = 0x00102F88;
  uint8_t patch1179_data[] = {
    0xEB, 
  };
  APPLY_PATCH(1179);

  uint32_t patch1180_addr = 0x00103286;
  uint8_t patch1180_data[] = {
    0x98, 0x80, 0x59, 0x00, 0x15, 0xBA, 
  };
  APPLY_PATCH(1180);

  uint32_t patch1181_addr = 0x001032F0;
  uint8_t patch1181_data[] = {
    0x16, 
  };
  APPLY_PATCH(1181);

  uint32_t patch1182_addr = 0x00103340;
  uint8_t patch1182_data[] = {
    0x17, 
  };
  APPLY_PATCH(1182);

  uint32_t patch1183_addr = 0x00103354;
  uint8_t patch1183_data[] = {
    0x17, 
  };
  APPLY_PATCH(1183);

  uint32_t patch1184_addr = 0x00103368;
  uint8_t patch1184_data[] = {
    0x37, 
  };
  APPLY_PATCH(1184);

  uint32_t patch1185_addr = 0x0010337C;
  uint8_t patch1185_data[] = {
    0x37, 
  };
  APPLY_PATCH(1185);

  uint32_t patch1186_addr = 0x00103390;
  uint8_t patch1186_data[] = {
    0x97, 
  };
  APPLY_PATCH(1186);

  uint32_t patch1187_addr = 0x001048A7;
  uint8_t patch1187_data[] = {
    0x12, 
  };
  APPLY_PATCH(1187);

  uint32_t patch1188_addr = 0x00104BA6;
  uint8_t patch1188_data[] = {
    0x61, 
  };
  APPLY_PATCH(1188);

  uint32_t patch1189_addr = 0x00104BA8;
  uint8_t patch1189_data[] = {
    0x09, 0x1F, 
  };
  APPLY_PATCH(1189);

  uint32_t patch1190_addr = 0x00104BAB;
  uint8_t patch1190_data[] = {
    0xBE, 
  };
  APPLY_PATCH(1190);

  uint32_t patch1191_addr = 0x00104BB0;
  uint8_t patch1191_data[] = {
    0xC9, 
  };
  APPLY_PATCH(1191);

  uint32_t patch1192_addr = 0x00104BB4;
  uint8_t patch1192_data[] = {
    0x66, 
  };
  APPLY_PATCH(1192);

  uint32_t patch1193_addr = 0x00104BB6;
  uint8_t patch1193_data[] = {
    0xF9, 
  };
  APPLY_PATCH(1193);

  uint32_t patch1194_addr = 0x00104BBA;
  uint8_t patch1194_data[] = {
    0xE9, 
  };
  APPLY_PATCH(1194);

  uint32_t patch1195_addr = 0x001056FA;
  uint8_t patch1195_data[] = {
    0x95, 0x33, 0x1F, 0x6E, 0x94, 0xD6, 
  };
  APPLY_PATCH(1195);

  uint32_t patch1196_addr = 0x0010634A;
  uint8_t patch1196_data[] = {
    0x19, 0x09, 0x19, 0xF8, 0x19, 0x13, 
  };
  APPLY_PATCH(1196);

  uint32_t patch1197_addr = 0x00106351;
  uint8_t patch1197_data[] = {
    0x4D, 
  };
  APPLY_PATCH(1197);

  uint32_t patch1198_addr = 0x00106369;
  uint8_t patch1198_data[] = {
    0x78, 
  };
  APPLY_PATCH(1198);

  uint32_t patch1199_addr = 0x00106A00;
  uint8_t patch1199_data[] = {
    0x18, 0x07, 0x18, 0x09, 0x18, 0x1D, 0x18, 0x7E, 0x18, 0x7E, 
  };
  APPLY_PATCH(1199);

  uint32_t patch1200_addr = 0x001093F0;
  uint8_t patch1200_data[] = {
    0x9B, 0x15, 0x9B, 0xAA, 
  };
  APPLY_PATCH(1200);

  uint32_t patch1201_addr = 0x0010B1C7;
  uint8_t patch1201_data[] = {
    0xD0, 
  };
  APPLY_PATCH(1201);

  uint32_t patch1202_addr = 0x0010B25A;
  uint8_t patch1202_data[] = {
    0xCB, 
  };
  APPLY_PATCH(1202);

  uint32_t patch1203_addr = 0x0010B25C;
  uint8_t patch1203_data[] = {
    0xA9, 0x97, 
  };
  APPLY_PATCH(1203);

  uint32_t patch1204_addr = 0x0010B264;
  uint8_t patch1204_data[] = {
    0xE9, 
  };
  APPLY_PATCH(1204);

  uint32_t patch1205_addr = 0x0010B266;
  uint8_t patch1205_data[] = {
    0x83, 0x7C, 
  };
  APPLY_PATCH(1205);

  uint32_t patch1206_addr = 0x0010B358;
  uint8_t patch1206_data[] = {
    0x4A, 0x0E, 
  };
  APPLY_PATCH(1206);

  uint32_t patch1207_addr = 0x0010B35B;
  uint8_t patch1207_data[] = {
    0xA7, 0xCB, 
  };
  APPLY_PATCH(1207);

  uint32_t patch1208_addr = 0x0010B35E;
  uint8_t patch1208_data[] = {
    0xCA, 
  };
  APPLY_PATCH(1208);

  uint32_t patch1209_addr = 0x0010B360;
  uint8_t patch1209_data[] = {
    0xA6, 0x1E, 
  };
  APPLY_PATCH(1209);

  uint32_t patch1210_addr = 0x0010B365;
  uint8_t patch1210_data[] = {
    0x00, 0xE9, 
  };
  APPLY_PATCH(1210);

  uint32_t patch1211_addr = 0x0010B368;
  uint8_t patch1211_data[] = {
    0xC8, 
  };
  APPLY_PATCH(1211);

  uint32_t patch1212_addr = 0x0010B36A;
  uint8_t patch1212_data[] = {
    0x93, 0x5D, 0x08, 0x5F, 
  };
  APPLY_PATCH(1212);

  uint32_t patch1213_addr = 0x0010B374;
  uint8_t patch1213_data[] = {
    0x12, 0x6C, 0xDA, 0xAC, 
  };
  APPLY_PATCH(1213);

  uint32_t patch1214_addr = 0x0010B37C;
  uint8_t patch1214_data[] = {
    0xDA, 0xA2, 0xC2, 0x36, 0x58, 0x46, 0x68, 0x91, 0x60, 0xA1, 0x70, 0xFF, 0x50, 0x80, 0x48, 0x11, 
    0x40, 0x6A, 0x40, 0xA5, 0x95, 0x45, 0x95, 0x10, 
  };
  APPLY_PATCH(1214);

  uint32_t patch1215_addr = 0x001270B8;
  uint8_t patch1215_data[] = {
    0x18, 0x9D, 0x18, 0xC8, 0x10, 0x6B, 0x10, 0xB7, 0x98, 0x98, 
  };
  APPLY_PATCH(1215);

  uint32_t patch1216_addr = 0x00127144;
  uint8_t patch1216_data[] = {
    0x99, 0xC5, 0x99, 0xE0, 0x99, 0x65, 0x99, 0x09, 0x99, 0xF8, 
  };
  APPLY_PATCH(1216);

  uint32_t patch1217_addr = 0x00127156;
  uint8_t patch1217_data[] = {
    0x99, 0x12, 0x99, 0x40, 0x99, 0xE5, 0x99, 0x95, 0x99, 0xEF, 
  };
  APPLY_PATCH(1217);

  uint32_t patch1218_addr = 0x00127294;
  uint8_t patch1218_data[] = {
    0x10, 0xB5, 0x10, 0xD1, 0x18, 0x3B, 
  };
  APPLY_PATCH(1218);

  uint32_t patch1219_addr = 0x0012729B;
  uint8_t patch1219_data[] = {
    0xAF, 
  };
  APPLY_PATCH(1219);

  uint32_t patch1220_addr = 0x0012814C;
  uint8_t patch1220_data[] = {
    0x79, 0xF8, 0x79, 0x13, 0x5B, 0x63, 0x5B, 0xBB, 0x5B, 0x11, 
  };
  APPLY_PATCH(1220);
}

void patching(int rom)
{
  switch(rom) {
    case 0:
      patch_v100();
      break;
    default:
      fprintf(stderr, "Bad ROM ID\n");
      break;
  }
}

