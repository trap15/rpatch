COMOBJECTS	 = signature.o patcher.o
OBJECTS		 = main.o ket.o $(COMOBJECTS)
OUTPUTS		 = ketpatch
DEFS		 = 
OPTIMIZE	 = -O0 -g
LIBS		 = 
LIBDIRS		 = 
INCLUDES	 = -I./
CFLAGS		 = -Wall -pedantic $(INCLUDES) $(OPTIMIZE)
CXXFLAGS	 = $(CFLAGS)
LDFLAGS		 = $(LIBDIRS) $(LIBS) -g
CC		 = gcc
CXX		 = g++
RM		 = rm

all: $(OBJECTS) $(OUTPUTS)
%.o: %.m
	$(CC) $(CFLAGS) $(DEFS) -c -o $@ $<
%.o: %.c
	$(CC) $(CFLAGS) $(DEFS) -c -o $@ $<
%.o: %.cpp
	$(CXX) $(CXXFLAGS) $(DEFS) -c -o $@ $<
ketpatch: $(OBJECTS)
	$(CXX) $(LDFLAGS) -o $@ main.o ket.o $(COMOBJECTS)
clean:
	$(RM) -f $(OUTPUTS) $(OBJECTS)
release: clean

