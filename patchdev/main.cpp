#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

int main(int argc, char **argv)
{
  if(argc < 3) {
    fprintf(stderr, "Invalid arguments. Usage:\n"
                    "\t%s orig.bin new.bin\n",
                    argv[0]);
    return EXIT_FAILURE;
  }
  uint8_t *buffers[2];
  uint32_t size;
  uint32_t i;
  FILE *fporig = fopen(argv[1], "rb");
  FILE *fpmod = fopen(argv[2], "rb");
  fseek(fporig, 0, SEEK_END);
  fseek(fpmod, 0, SEEK_END);
  size = ftell(fporig);
  if(size != ftell(fpmod)) {
    fprintf(stderr, "Sizes don't match!\n");
    return EXIT_FAILURE;
  }
  fseek(fporig, 0, SEEK_SET);
  fseek(fpmod, 0, SEEK_SET);
  buffers[0] = (uint8_t*)malloc(size);
  if(buffers[0] == NULL) {
    perror("Unable to alloc buffer 0");
    return EXIT_FAILURE;
  }
  buffers[1] = (uint8_t*)malloc(size);
  if(buffers[1] == NULL) {
    perror("Unable to alloc buffer 1");
    return EXIT_FAILURE;
  }
  fread(buffers[0], size, 1, fporig);
  fread(buffers[1], size, 1, fpmod);
  fclose(fporig);
  fclose(fpmod);

  int patch_id = 0;
  bool patch_running = false;
  int patch_data_x;
  for(i = 0; i < size; i++) {
    if(buffers[0][i] != buffers[1][i]) {
      if(!patch_running) {
        patch_running = true;
        patch_data_x = 0;
        printf("  uint32_t patch%d_addr = 0x%08X;\n", patch_id, i);
        printf("  uint8_t patch%d_data[] = {\n", patch_id);
      }
      if((patch_data_x % 0x10) == 0)
        printf("    ");
      printf("0x%02X, ", buffers[1][i]);
      patch_data_x++;
      if((patch_data_x % 0x10) == 0)
        printf("\n");
    }else{
      if(patch_running) {
        patch_running = false;
        if((patch_data_x % 0x10) != 0)
          printf("\n");
        printf("  };\n");
        printf("  APPLY_PATCH(%d);\n\n", patch_id);
        patch_id++;
      }
    }
  }
  if(patch_running) {
    patch_running = false;
    if((patch_data_x % 0x10) != 0)
      printf("\n");
    printf("  };\n");
    printf("  APPLY_PATCH(%d);\n\n", patch_id);
    patch_id++;
  }

  return EXIT_SUCCESS;
}












