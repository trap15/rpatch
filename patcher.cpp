#include <stdio.h>
#include <stdlib.h>

#include "patcher.h"
#include "types.h"

uint8_t* load_buffer(const char *filename, uint32_t& length)
{
	FILE* fp = fopen(filename, "rb");
	if(!fp)
		return NULL;
	fseek(fp, 0, SEEK_END);
	length = ftell(fp);
	rewind(fp);
	uint8_t* buf = new uint8_t[length];
	int bytes_read = fread(buf, 1, length, fp);
	(void)bytes_read;
	fclose(fp);

	return buf;
}

int save_buffer(const char* filename, uint32_t length, uint8_t* buffer)
{
	FILE *fp = fopen(filename, "wb");
	if(!fp)
		return -1;
	fwrite(buffer, 1, length, fp);
	fclose(fp);
	return 0;
}

uint8_t *game_rom = NULL;
void game_patch_set(uint8_t* src)
{
	game_rom = src;
}

void game_patch_bytes(uint32_t addr, uint8_t *buf, uint32_t len)
{
	for(uint32_t i=0; i < len; i++) {
		game_rom[addr+i] = buf[i];
	}
}


