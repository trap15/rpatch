#ifndef PATCHER_H_
#define PATCHER_H_

#include "types.h"

uint8_t* load_buffer(const char *filename, uint32_t& length);
int save_buffer(const char *filename, uint32_t length, uint8_t* buffer);
void game_patch_set(uint8_t* src);

void game_patch_bytes(uint32_t addr, uint8_t *buf, uint32_t len);

extern uint8_t *game_rom;

struct game_info_t {
	int count;
	struct rom_data {
		const char *name;
		uint32_t size;
		uint32_t crc;
	} data[256];
};

#define APPLY_PATCH(id) game_patch_bytes(patch##id##_addr, patch##id##_data, sizeof(patch##id##_data))

#endif

